//
//  FruitGameSetting.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 18/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitGameSetting.h"

#define PREFIX	@"Fruit"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"


@implementation GameSetting (Fruit)

// Override
+ (NSString*)getClassPrefix {
	return PREFIX;
}

// Override
+ (NSString*)getNIBPrefix {
	return PREFIX;
}

+(NSString*) getiTunesID{
    return @"543121912";
}

+(NSString*) getFlurryID{
    return @"MZYNFN9ZQGHGYSRVQT8R";
}

+(NSString*) getAdMobID{
    return @"";
}

+(NSString*) getMoPubID{
    return @"";
}

+(NSString*) getMunerisID{
    return @"f69600ef4e3843f8acaee8e2ab2ca835";
}

+(BOOL)getRetinaDisplay{
    return YES;
}

+ (BOOL)isIPhone5Supported {
	return NO;
}

+ (BOOL)isUniversalSupported {
	return NO;
}

+ (NSString*) getPrivacyPolicyLink{
    return @"http://webprancer.com/privacy-policy-ios/";
}


@end

#pragma clang diagnostic pop
