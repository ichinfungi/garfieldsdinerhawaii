//
//  FruitGameSetting.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 18/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitGameSetting.h"
#import "JRSwizzle.h"

#define PREFIX	@"Fruit"

@implementation GameSetting (Fruit)

+ (void)load
{
	// method swizzling
	[self jr_swizzleClassMethod:@selector(getClassPrefix) withClassMethod:@selector(getFruitClassPrefix) error:nil];
	[self jr_swizzleClassMethod:@selector(getNIBPrefix) withClassMethod:@selector(getFruitNIBPrefix) error:nil];
	[self jr_swizzleClassMethod:@selector(getiTunesID) withClassMethod:@selector(getFruitiTunesID) error:nil];
	[self jr_swizzleClassMethod:@selector(getRetinaDisplay) withClassMethod:@selector(getFruitRetinaDisplay) error:nil];
	[self jr_swizzleClassMethod:@selector(isUniversalSupported) withClassMethod:@selector(isFruitUniversalSupported) error:nil];
}

// Override
+ (NSString*)getFruitClassPrefix {
	return PREFIX;
}

// Override
+ (NSString*)getFruitNIBPrefix {
	return PREFIX;
}

+(NSString*) getFruitiTunesID{
    return @"543125794";
}

+(BOOL)getFruitRetinaDisplay{
    return IsiPhone;	//only iphone support retina Display
}

+ (BOOL)isFruitUniversalSupported {
	return YES;
}

@end

