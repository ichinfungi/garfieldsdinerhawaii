//
//  InAppPurchaseSetting.m
//  DCGameTemplate
//
//  Created by Ron Leung on 26/08/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitInAppPurchaseSetting.h"
#import "Utilities.h"

/*

 #define PACKAGE1_PRODUCT_ID		(IsiPhone? @"com.dreamcortex.prettyPetTycoon.petPointsPackage1" : @"com.dreamcortex.prettyPetTycoonHD.petPointsPackage1")
 #define PACKAGE2_PRODUCT_ID		(IsiPhone? @"com.dreamcortex.prettyPetTycoon.petPointsPackage2" : @"com.dreamcortex.prettyPetTycoonHD.petPointsPackage2")
 #define PACKAGE3_PRODUCT_ID		(IsiPhone? @"com.dreamcortex.prettyPetTycoon.petPointsPackage3" : @"com.dreamcortex.prettyPetTycoonHD.petPointsPackage3")
 #define PACKAGE4_PRODUCT_ID		(IsiPhone? @"com.dreamcortex.prettyPetTycoon.petPointsPackage4" : @"com.dreamcortex.prettyPetTycoonHD.petPointsPackage4")

 */

@implementation InAppPurchaseSetting (Fruit)

+(NSString*)getPackage1_ID{
    return @"com.webprancer.itunes.garfieldsdinerhawaiiHD.package1";
}
+(NSString*)getPackage2_ID{
    return @"com.webprancer.itunes.garfieldsdinerhawaiiHD.package2";
}
+(NSString*)getPackage3_ID{
    return @"com.webprancer.itunes.garfieldsdinerhawaiiHD.package3";
}
+(NSString*)getPackage4_ID{
    return @"com.webprancer.itunes.garfieldsdinerhawaiiHD.package4";
}
+(NSString*)getPackage5_ID{
    return @"com.webprancer.itunes.garfieldsdinerhawaiiHD.package5";
}

+(int)getPackage1_Point{
    return 100;
}
+(int)getPackage2_Point{
    return 210;
}
+(int)getPackage3_Point{
    return 460;
}
+(int)getPackage4_Point{
    return 1250;
}
+(int)getPackage5_Point{
    return 2750;
}

@end