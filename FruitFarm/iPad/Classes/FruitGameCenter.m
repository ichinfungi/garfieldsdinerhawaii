
#import "FruitGameCenter.h"
#import "Utilities.h"

@implementation GameCenter (Fruit)

+(NSString*) getAchievementID:(AchievementID)achID{
    switch (achID) {
        case ach_firstpp:
            return @"hawaiiHD_ach_firstpp";
            break;
        case ach_50days:
            return @"hawaiiHD_ach_50days";
        
        case ach_100days:
            return @"hawaiiHD_ach_100days";

        case ach_200days:
            return @"hawaiiHD_ach_200days";

        case ach_happiness100:
            return @"hawaiiHD_ach_happiness100";

        case ach_happiness500:
            return @"hawaiiHD_ach_happiness500";

        case ach_happiness999:
            return @"hawaiiHD_ach_happiness999";

        case ach_customers2500:
            return @"hawaiiHD_ach_customers2500";

        case ach_customers5000:
            return @"hawaiiHD_ach_customers5000";

        case ach_customers10000:
            return @"hawaiiHD_ach_customers10000";

        case ach_shoplv5:
            return @"hawaiiHD_ach_shoplv5";

        case ach_shoplv10:
            return @"hawaiiHD_ach_shoplv10";

        case ach_allequipment:
            return @"hawaiiHD_ach_allequipment";

        case ach_maxupgrade:
            return @"hawaiiHD_ach_maxupgrade";

        case ach_allstaff:
            return @"hawaiiHD_ach_allstaff";

        case ach_spvisit:
            return @"hawaiiHD_ach_spvisit";

        case ach_dayearn0:
            return @"hawaiiHD_ach_dayearn0";

        case ach_dayearn5000:
            return @"hawaiiHD_ach_dayearn5000";

        case ach_dayearn10000:
            return @"hawaiiHD_ach_dayearn10000";

        case ach_dayearn15000:
            return @"hawaiiHD_ach_dayearn15000";

        case ach_dayhappiness10:
            return @"hawaiiHD_ach_dayhappiness10";

        case ach_dayhappiness15:
            return @"hawaiiHD_ach_dayhappiness15";

        case ach_daysad10:
            return @"hawaiiHD_ach_daysad10";

        case ach_daysad15:
            return @"hawaiiHD_ach_daysad15";
            
        case ach_perfectday:
            return @"hawaiiHD_ach_perfectday";
            
        default:
            break;
    }    
    return @"";
}

+(NSString*) getLeaderBoardID:(LeaderBoardID)ldBID{
    switch (ldBID) {
        case ilb_shopdays:
            return @"com.webprancer.itunes.garfiledsdinerhawaiiHD.lb_shopdays";

        case ilb_overallmoney:
            return  @"com.webprancer.itunes.garfiledsdinerhawaiiHD.overallmoney";

        case ilb_happiness:
            return  @"com.webprancer.itunes.garfiledsdinerhawaiiHD.lb_happiness";

        case ilb_quest_completed:
            return @"com.webprancer.itunes.garfiledsdinerhawaiiHD.lb_questcompleted";
            
        default:
            break;
    }
    return @"";
}
@end