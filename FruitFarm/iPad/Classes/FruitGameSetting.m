//
//  FruitGameSetting.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 18/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitGameSetting.h"

#define PREFIX	@"Fruit"

@implementation GameSetting (Fruit)

// Override
+ (NSString*)getClassPrefix {
	return PREFIX;
}

// Override
+ (NSString*)getNIBPrefix {
	return PREFIX;
}

+(NSString*) getiTunesID{
    return @"543425244";
}

+(NSString*) getFlurryID{
    return @"7N9XGDSTKS9H9JJXDBJK";
}

+(NSString*) getAdMobID{
    return @"a14ffce1104db19";
}

+(NSString*) getMoPubID{
    return @"agltb3B1Yi1pbmNyDQsSBFNpdGUY4drKFAw";
}

+(NSString*) getMunerisID{
    return @"6c87d59575fc4c838bad7b5e014b3825";
}
@end
