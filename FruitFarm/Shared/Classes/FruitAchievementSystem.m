//
//  FruitAchievementSystem.m
//  DCGameTemplate
//
//  Created by royl on 10/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitAchievementSystem.h"
#import "GameSetting.h"
#import "JRSwizzle.h"

#define USE_DCPROFILE_FOR_STORAGE	1

#if(USE_DCPROFILE_FOR_STORAGE)
// Use DCProfileManager
#import "DCProfileManager.h"
#import "NSDictionaryExtend.h"

#define ACHIEVEMENTS_DICT_NAME	@"Achievements"
#define ACHIEVEMENTS_DICT	[[[DCProfileManager sharedManager] getSystemProfile].dict objectForKey:ACHIEVEMENTS_DICT_NAME]

#else
// Use DataManager
#import "DataManager.h"
#endif

@implementation AchievementSystem (Fruit)

#pragma mark -
#pragma mark Class Methods

+ (void)load
{
	// method swizzling
	[self jr_swizzleMethod:@selector(getAchievementMessageImage) withMethod:@selector(getAchievementMessageImage_f) error:nil];
	[self jr_swizzleMethod:@selector(getAchievementMessageFrame) withMethod:@selector(getAchievementMessageFrame_f) error:nil];
	[self jr_swizzleMethod:@selector(customizeAchievementMessageLabel:) withMethod:@selector(customizeAchievementMessageLabel_f:) error:nil];
}

-(UIImage*)getAchievementMessageImage_f{
    return [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Challeng_banner.png")];
}

-(CGRect)getAchievementMessageFrame_f{
    if([Utilities isiPad]){
        return CGRectMake(0, 0, 1024, 54);
    }
    else{
        return CGRectMake(0, 0, 480, 27);
    }
}

-(void)customizeAchievementMessageLabel_f:(UILabel *)label{
    label.textAlignment = NSTextAlignmentCenter;
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 10;
    if([Utilities isiPad]){
        label.font = [UIFont fontWithName:@"Arial-BoldMT" size:28];
    }
    else{
        label.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    }
    label.textColor = [UIColor colorWithRed:87.0f/255.0f green:66.0f/255.0f blue:36.0f/255.0f alpha:1.0f];
    label.backgroundColor = [UIColor clearColor];
}

@end
