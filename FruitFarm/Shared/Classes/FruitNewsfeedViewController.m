//
//  FruitNewsfeedViewController.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 07/04/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitNewsfeedViewController.h"

#define NEWSFEED_URL	@"http://www.prettypetsalon.com/pptfeed.php"
#define FB_URL			@"http://www.prettypetsalon.com/pptfeed.php?fblink=1"

@implementation FruitNewsfeedViewController

- (NSString*)newsfeedURL {
	return NEWSFEED_URL;
}

- (NSString*)fbURL {
	return FB_URL;
}

@end
