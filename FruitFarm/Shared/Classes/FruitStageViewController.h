//
//  FruitStageViewController.h
//  DCGameTemplate
//
//  Created by Dream Cortex on 18/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PrettyStageViewController.h"
#import "ImageLabel.h"

typedef enum {
	STAGEVIEW_BTN_MAP = STAGEVIEW_BTN_COUNT,
	STAGEVIEW_BTN_EXTEND_COUNT
} STAGEVIEW_BTN_TYPE_EXTEND;

typedef enum {
	CHANGE_STYLE_UP = 0,
	CHANGE_STYLE_DOWN
} CHANGE_STYLE;

@interface FruitStageViewController : PrettyStageViewController {
	
	UIView *iDimFilter;
	
	// Status Bar
	//ImageLabel *starImageLabel;
	//UIView *iExpView;
	//UIImageView *iLevelStarImageView;
	//UIImageView *iExpStarImageView;
	UILabel *iChangeMoneyLabel;
	UILabel *iChangeScoreLabel;
	UIImageView *iReportLevelUpImageView;
	UILabel *iReportTodayMoneyLabel;
	UILabel *iReportTodayScoreLabel;
    UILabel *iReportHappyCustomers;
    UILabel *iReportSadCustomers;
    UILabel *iReportReqMoney;
    UILabel *iReportReqScore;
    UILabel *iTodayReportLabel;
    UILabel *iNextLvlReqLabel;
	UIImageView *iDialogueBoxImageView;
	UIView *iDialogueDimView;
	UIImageView *iWheelNoticeImageView;
    UIButton *iReportViewButton;
    UIButton *iStaffBoostButton;
    UIView *iUnlockCardView;
    UIImageView *iUnlockCardViewBG;
    UIView *iUnlockCardViewChar;
    UIImageView *iDialogueTip;
	
	UIImageView *iLevelUpSunShineImg;
	UIImageView *iLevelUpGarfieldIcon;
	NSTimer *levelUpTimer;
	
}

@property (nonatomic, retain) IBOutlet UIView *iDimFilter;

#pragma mark -
#pragma mark Status Bar
//@property (nonatomic, retain) IBOutlet UIView *iExpView;
//@property (nonatomic, retain) IBOutlet UIImageView *iLevelStarImageView;
//@property (nonatomic, retain) IBOutlet UIImageView *iExpStarImageView;
@property (nonatomic, retain) IBOutlet UILabel *iChangeMoneyLabel;
@property (nonatomic, retain) IBOutlet UILabel *iChangeScoreLabel;

- (void)updateDimFilter:(float)time animated:(BOOL)animated;
- (void)setExpPercent:(float)percent animated:(BOOL)animated;
- (void)showChangeScore:(NSString*)text style:(CHANGE_STYLE)style;
- (void)showChangeMoney:(NSString*)text style:(CHANGE_STYLE)style;

#pragma mark -
#pragma mark Prepare
@property (nonatomic, retain) IBOutlet UIButton *iMapButtom;
@property (nonatomic, retain) IBOutlet UIImageView *iWheelNoticeImageView;
- (IBAction)wheelOnClick:(id)sender;

#pragma mark -
#pragma mark Level Up View
@property (nonatomic, retain) IBOutlet UIImageView *iLevelUpSunShineImg;
@property (nonatomic, retain) IBOutlet UIImageView *iLevelUpGarfieldIcon;
- (void)sunShineAnimation;
- (void)levelupUpdate:(NSTimer*)timer; 
- (void)garfieldIconAnimation;


#pragma mark -
#pragma mark Report
@property (nonatomic, retain) IBOutlet UIImageView *iReportLevelUpImageView;
@property (nonatomic, retain) IBOutlet UILabel *iReportTodayMoneyLabel;
@property (nonatomic, retain) IBOutlet UILabel *iReportTodayScoreLabel;
@property (nonatomic ,retain) IBOutlet UILabel *iReportHappyCustomers;
@property (nonatomic ,retain) IBOutlet UILabel *iReportSadCustomers;
@property (nonatomic ,retain) IBOutlet UILabel *iReportReqMoney;
@property (nonatomic ,retain) IBOutlet UILabel *iReportReqScore;
@property (nonatomic ,retain) IBOutlet UIButton *iReportViewButton;
@property (nonatomic ,retain) IBOutlet UILabel *iTodayReportLabel;
@property (nonatomic ,retain) IBOutlet UILabel *iNextLvlReqLabel;

- (void)setReportViewTodayMoney:(int)todayMoney todayScore:(float)todayScore happyCustomers:(int)happyCustomers sadCustomers:(int)sadCustomers nextMoney:(int)nextMoney nextScore:(float)nextScore;

- (IBAction) iReportViewButtonOnClick;

- (void) resumeFromReportView;


#pragma mark -
#pragma mark Dialogue
@property (nonatomic, retain) IBOutlet UIImageView *iDialogueBoxImageView;
@property (nonatomic, retain) IBOutlet UIView *iDialogueDimView;
@property (nonatomic, retain) IBOutlet UIImageView *iDialogueTip;

#pragma mark - 
#pragma mark Banner
//- (void)showBanner;
- (void)hideBanner;

#pragma mark -
#pragma consumable item
@property (nonatomic, retain) IBOutlet UIButton *iStaffBoostButton;
@property (nonatomic, retain) IBOutlet UIImageView *iStaffBoostIconImage;
@property (nonatomic, retain) IBOutlet UIImageView *iStaffBoostEffectImage;
@property (nonatomic, retain) IBOutlet UIButton *iWildCardButton;
@property (nonatomic, retain) IBOutlet UIImageView *iWildCardIconImage;
@property (nonatomic, retain) IBOutlet UIImageView *iWildCardEffectImage;
@property (nonatomic, retain) IBOutlet UIButton *iSpawnBoostButton;
@property (nonatomic, retain) IBOutlet UIImageView *iSpawnBoostIconImage;
@property (nonatomic, retain) IBOutlet UIImageView *iSpawnBoostEffectImage;
-(IBAction)buyStaffBoostOnClick:(id)sender;
-(IBAction)buySpawnBoostOnClick:(id)sender;
-(IBAction)buyWildCardOnClick:(id)sender;

#pragma mark -
#pragma Upgrade catagory
@property (nonatomic, retain) IBOutlet UIButton *iCatagoryButtonFacilityUpgrade;
@property (nonatomic, retain) IBOutlet UIButton *iCatagoryButtonStaffUpgrade;
@property (nonatomic, retain) IBOutlet UIButton *iCatagoryButtonDailyBonus;
@property (nonatomic, retain) IBOutlet UIButton *iCatagoryButtonCoins;
@property (nonatomic, retain) IBOutlet UIButton *iCatagoryButtonCard;
-(IBAction)showFacilityUpgradeOnClick;
-(IBAction)showStaffUpgradeOnClick;
-(IBAction)showDailyRewardOnClick;
-(IBAction)showCardViewOnClick;
-(void)setCatagoryButtonsEnable:(BOOL)enable;

#pragma mark -
#pragma Card System
@property (nonatomic, retain) IBOutlet UIView *iUnlockCardView;
@property (nonatomic, retain) IBOutlet UIImageView *iUnlockCardViewBG;
@property (nonatomic, retain) IBOutlet UIView *iUnlockCardViewChar;
-(void)showUnlockCardAnim;
-(void)hideUnlockCardAnim;
-(void)releaseUnlockCardAnim;

#pragma mark -
#pragma debug
-(void)gotoDebugStage;

#pragma mark -
#pragma Speed Up
@property (nonatomic, retain) IBOutlet UIView *iGamePlayView;
@property (nonatomic, retain) IBOutlet UIView *iGamePlaySpeedView;
@property (retain, nonatomic) IBOutlet UIButton *iGameplaySpeedUpButton;
@property (retain, nonatomic) IBOutlet UIButton *iGameplaySpeedDownButton;
@property (retain, nonatomic) IBOutlet UILabel *iGameplaySpeedLabel;
@property (retain, nonatomic) IBOutlet UILabel *iGameplayProfitLabel;

- (void)showGameplayView;
- (void)hideGameplayView;
- (void)releaseGameplayView;

//- (void)showSpeedView;
- (void)updateSpeedInterface;
- (IBAction)gameplaySpeedOnClick:(id)sender;




@end
