//
//  FruitPrettyFacilityUser.m
//  DCGameTemplate
//
//  Created by Calvin on 22/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyFacilityUser.h"
#import "PrettyManager.h"
#import "PrettyFacility.h"
#import "NSDictionaryExtend.h"
#import "GameSetting.h"
#import "AchievementSystem.h"
#import "FruitGameSetting.h"
#import "GameUnit.h"

#define USER_WAITING_OUTDOOR_TIME 5

@implementation FruitPrettyFacilityUser

@synthesize originalScale = _originalScale;
@synthesize robotDragging = _robotDragging;

- (id) init:(int)pDNAID position:(CGPoint)pPosition{
    self = [super init:pDNAID position:pPosition];
    if(self != nil){
        //disable dragging when car waiting out door
        [self dragEnable:NO];
        if([Utilities isiPad]){
            self.walkPPS = self.walkPPS * 2;
        }
    }
	//Jonathan QuickFix: Touch problems with Garfield
	//Garfield has very slender characters, pad the touch rect a bit
//	if (self.dnaID == 2 || self.dnaID == 7 || self.dnaID == 8)
//	{
//		[self.sprite padTouchRectLeft:20 Top:0 Right:20 Bottom:10];
//	}
//	else
//	{
//		[self.sprite padTouchRectLeft:10 Top:0 Right:10 Bottom:0];
//	}
	
	[self.sprite padTouchRectLeft:0 Top:0 Right:0 Bottom:[GameUnit pixelLengthFromUnitLength:2]];
	self.originalScale = self.sprite.scale;
    return self;
}

- (BOOL)canEnterFacility:(PrettyFacility*)pFacility
{
    //overide 
    /*if(_isWildCard){
        if (pFacility == nil || pFacility == self.facility || self.facilityUserState == FACILITY_USER_TIMEOUT) {
            return NO;
        }
        
        return (pFacility.dnaID == 3 || pFacility.dnaID == 4 || pFacility.dnaID == 5 || pFacility.dnaID == 6 || pFacility.dnaID == 7);
    }
    else{*/
        return [super canEnterFacility:pFacility];
    //}
}

- (void)enterFacility:(PrettyFacility*)pFacility stageTime:(float)stageTime
{
    //enable dragging when car enter any facility
    [self dragEnable:YES];
	[super enterFacility:pFacility stageTime:stageTime];
}

- (BOOL)startWaitingForNextFacility:(float)stageTime
{
    //make user wait outdoor for longer time
	int nextFacilityDNAID = [self getNextFacilityDNAID];
	if(nextFacilityDNAID == 1 && self.facility == nil){
        stateTimer = stageTime + USER_WAITING_OUTDOOR_TIME;
		[self setFacilityUserState:FACILITY_USER_WAIT_FOR_NEXT_FACILITY];
		[self displayBubble:[[[PrettyManager sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d/userBubbleHint", nextFacilityDNAID]] style:GAMEOBJECT_BUBBLESTYLE_LOOPSCALE];
		return YES;
    }
    else{
        BOOL result = [super startWaitingForNextFacility:stageTime];
        /*if(_isWildCard){
            if(nextFacilityDNAID != 1 && nextFacilityDNAID != 2){
                [self hideBubble];
                [self displayBubble:@"bub_11_a.png" style:GAMEOBJECT_BUBBLESTYLE_LOOPSCALE];
            }
        }*/
        return result;
    }
}

- (BOOL) startWaitingForNextFacilityHurry:(float)stageTime{
    BOOL result = [super startWaitingForNextFacilityHurry:stageTime];
    /*int nextFacilityDNAID = [self getNextFacilityDNAID];
    if(_isWildCard){
        if(nextFacilityDNAID != 1 && nextFacilityDNAID != 2){
            [self hideBubble];
            [self displayBubble:@"bub_11_b.png" style:GAMEOBJECT_BUBBLESTYLE_LOOPSCALE_HIGHSPEED];
        }
    }*/
    return result;
}

- (void)startServiceAtFacility:(PrettyFacility*)pFacility
{
    //if(pFacility.dnaID == 2 && [pFacility.facilityUserQueueArray count] >= 5){
    //    [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_cashier percent:100];
    //}
    [super startServiceAtFacility:pFacility];
}

/*- (int)getNextFacilityDNAID
{
    if(self.numOfFacilityUsed < [self.facilityCycle count]){
        if((self.facilityUserState == FACILITY_USER_WAIT_FOR_NEXT_FACILITY || self.facilityUserState == FACILITY_USER_WAIT_FOR_NEXT_FACILITY_HURRY) && [[self.facilityCycle objectAtIndex:self.numOfFacilityUsed]intValue] == self.facility.dnaID){
            self.numOfFacilityUsed = [self.facilityCycle count] -1;
            return 2;
        }
        return [[self.facilityCycle objectAtIndex:self.numOfFacilityUsed]intValue];
    }
    return -1;
}*/

- (void)finishedServiceAtFacility:(PrettyFacility*)pFacility{
    [super finishedServiceAtFacility:pFacility];

    //_numOfFacilityUsed++;
}

- (void)robotDragStart
{
	_robotDragging = YES;
}

- (void)robotDragEnd
{
	_robotDragging = NO;
}

- (void)dragStart:(CGPoint)touchOffset
{
	if (self.robotDragging) {
		return;
	}
	[super dragStart:touchOffset];
    [self setAction:@"STAND" withDirection:@"DOWN"];
	[self.sprite setScale:(self.originalScale*1.2f)];

}

- (void)dragEnd
{
	if (self.robotDragging) {
		return;
	}
	[super dragEnd];
	[self.sprite setScale:self.originalScale];
}
- (void)timeOut
{
    [super timeOut];
    [_sprite setUserInteractionEnabled:NO];
    [_bubbleSprite setUserInteractionEnabled:NO];
}

//override to adjust bubble offset during parking
- (void)displayBubble:(NSString*)bubbleFile style:(GAMEOBJECT_BUBBLESTYLE)style
{
    //[super displayBubble:bubbleFile style:style];
    [self setBubbleWithFile:bubbleFile];
    [self setBubbleStyle:style];
    NSDictionary *dnaDict = [[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", _dnaID]];
    if([Utilities isiPad]){
		if ([dnaDict getData:@"offsets/bubbleX"] == nil) {
            if(self.facility != nil && self.facility.dnaID == 1){
                [self setBubbleOffsetInPixel:CGPointMake(([[[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/offsetX"] floatValue]), ([[[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/offsetY"] floatValue] * 768 / 320 ) - 40)];
            }
            else{
                [self setBubbleOffsetInPixel:CGPointMake(([[[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/offsetX"] floatValue]), ([[[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/offsetY"] floatValue] * 768 / 320 )-20)];
            }
        }
        else {
            if(self.facility != nil && self.facility.dnaID == 1){
                [self setBubbleOffsetInPixel:CGPointMake(([[dnaDict getData:@"offsets/bubbleX"] floatValue]), ([[dnaDict getData:@"offsets/bubbleY"] floatValue] * 768 / 320) - 40)];
            }
            else{
                [self setBubbleOffsetInPixel:CGPointMake(([[dnaDict getData:@"offsets/bubbleX"] floatValue]), ([[dnaDict getData:@"offsets/bubbleY"] floatValue] * 768 / 320)-20)];
            }
		}
    }
    else{
        if ([dnaDict getData:@"offsets/bubbleX"] == nil) {
            if(self.facility != nil && self.facility.dnaID == 1){
                [self setBubbleOffsetInPixel:CGPointMake(([[[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/offsetX"] floatValue]), ([[[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/offsetY"] floatValue]) - 10)];
            }
            else{
                [self setBubbleOffsetInPixel:CGPointMake(([[[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/offsetX"] floatValue]), ([[[[PrettyManagerClass sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/offsetY"] floatValue]))];
            }
        }
        else {
            if(self.facility != nil && self.facility.dnaID == 1){
                [self setBubbleOffsetInPixel:CGPointMake(([[dnaDict getData:@"offsets/bubbleX"] floatValue]), ([[dnaDict getData:@"offsets/bubbleY"] floatValue]) - 10)];
            }
            else{
                [self setBubbleOffsetInPixel:CGPointMake(([[dnaDict getData:@"offsets/bubbleX"] floatValue]), ([[dnaDict getData:@"offsets/bubbleY"] floatValue]))];
            }
		}
    }
    
    NSDictionary *pDict = [[PrettyManager sharedManager] getDNADict:FACILITY_USER_DNA_FILE];
    
    float specifyOffset = 0;
    if(self.facility != nil && self.facility.dnaID == 1){
        specifyOffset = -1.0;
    }
    
    if(([dnaDict getData:@"offsets/unitBubbleX"] != nil) && ([dnaDict getData:@"offsets/unitBubbleY"] != nil)){
        float offsetX = [[dnaDict getData:@"offsets/unitBubbleX"] floatValue];
        float offsetY = [[dnaDict getData:@"offsets/unitBubbleY"] floatValue]+specifyOffset;
        
        [self setBubbleOffsetInPixel:CGPointMake([GameUnit pixelLengthFromUnitLength:offsetX], [GameUnit pixelLengthFromUnitLength:offsetY])];
    }
    else if(([pDict getData:@"Bubbles/unitOffsetX"] != nil) && ([pDict getData:@"Bubbles/unitOffsetY"] != nil)){
        //[self setBubbleOffsetInPixel:CGPointMake([[dnaDict getData:@"Bubbles/unitOffsetX"] floatValue], [[dnaDict getData:@"Bubbles/unitOffsetY"] floatValue])];
        float offsetX = [[pDict getData:@"Bubbles/unitOffsetX"]floatValue];
        float offsetY = [[pDict getData:@"Bubbles/unitOffsetY"]floatValue]+specifyOffset;
        
        [self setBubbleOffsetInPixel:CGPointMake([GameUnit pixelLengthFromUnitLength:offsetX], [GameUnit pixelLengthFromUnitLength:offsetY])];
    }
    
    [self showBubble];
}
     
-(void)showEffect{
    [_EffectSprite setVisible:NO];
}

-(void)showBubble {
	[super showBubble];
//	//Functionality copied to Pretty Level
//	if (!_bIsDragging)
//	{
//		//Reset touch priority on show bubble
//		CGPoint pos = self.sprite.position;
//		[self.sprite resetTouchPriority:(pos.x + pos.y*1024) swallowsTouches:YES];
//		
//		//Bubble sprite should already be offset by 2
//		[self.bubbleSprite resetTouchPriority:(pos.x + pos.y*1024) swallowsTouches:YES];
//	}
}

-(void)hideBubble {
	[super hideBubble];
	if (!_bIsDragging)
	{
		//Reset touch priority on show bubble
		CGPoint pos = self.sprite.position;
		[self.sprite resetTouchPriority:(pos.x + pos.y*1024) swallowsTouches:NO];
	}
}


@end
