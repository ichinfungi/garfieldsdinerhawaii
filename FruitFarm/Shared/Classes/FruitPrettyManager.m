//
//  FruitPrettyManager.m
//  DCGameTemplate
//
//  Created by Chan Calvin on 19/10/2011.
//  Copyright 2011 Dream Cortex. All rights reserved.
//

#import "FruitPrettyManager.h"
#import "Localization.h"
#import "DCProfileManager.h"
#import "NSDictionaryExtend.h"

@implementation FruitPrettyManager

+ (NSString*)getProfileVersion{
    return @"1.0.0";
}

+ (NSString*)getGamePointSingularName{
    return LOCALIZATION_TEXT(@"Garfield Point");
}

+ (NSString*)getGamePointPluralName{
    return LOCALIZATION_TEXT(@"Garfield Points");
}

#pragma mark -
#pragma mark bonus percentage
+ (float)getPerfectDayBonusPercentage
{
	return 0;
}

+ (float)getPerfectFacilityUserBonusPercentage
{
	return 0;
}

+ (int)getEndingWorkingHour{
    return 21;
}

#pragma mark -
#pragma mark Rating

+ (NSMutableDictionary*)getManagerDict {
	DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
	if (profile == nil) {
		//NSLog(@"PonyManager: Current profile not found!");
		return nil;
	}
	
	NSMutableDictionary *dict = [profile.dict objectForKey:@"manager"];
	if (dict == nil) {
		// Create dictionary
		dict = [[[NSMutableDictionary alloc] init] autorelease];
		[profile.dict setObject:dict forKey:@"manager"];
		[[DCProfileManager sharedManager] saveAllProfiles];
	}
	return dict;
}

+ (int)getFirstRatingGameDay {
	NSMutableDictionary *managerDict = [self getManagerDict];
	if (managerDict==nil) {
		return 0;
	}
	return [[managerDict objectForKey:@"firstRating" defaultValue:[NSNumber numberWithInt:0]] intValue];
}

+ (void)setFirstRatingGameDay:(int)day {
	NSMutableDictionary *managerDict = [self getManagerDict];
	if (managerDict!=nil) {
		[managerDict setObject:[NSNumber numberWithInt:day] forKey:@"firstRating"];
	}
}

@end