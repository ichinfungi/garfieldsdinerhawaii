//
//  FruitMenuViewController.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 08/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitMenuViewController.h"
#import "AnimViewManager.h"
#import "FruitGameSetting.h"
#import "DCProfileManager.h"
#import "GamePointManager.h"
#import "AchievementSystem.h"
#import "FruitGameSetting.h"
#import "DataManager.h"
#import "NewsfeedViewController.h"
#import "LuckyWheelViewController.h"

#import "PrettyManager.h"
#import "NSDictionaryExtend.h"

#import "AnimViewAction.h"
#import "Localization.h"
#import "GameUnit.h"
#import "FruitDailyRewardsManager.h"

#define BGM_ENABLE_KEY	@"BGM_ENABLE_KEY"
#define SFX_ENABLE_KEY	@"SFX_ENABLE_KEY"

#define UPDATE_POP_UP_KEY @"V1.0.0_UPDATE_POP_UP_SHOWN"
#define INITIAL_POINT_KEY @"InitialPointKey"
#define FREE_GAME_TIPS_SHOWN_KEY @"FREE_GAME_TIPS_SHOWN_KEY"
#define CREATE_PROFILE_FIRST_TIME_KEY @"CREATE_PROFILE_FIRST_TIME_KEY"
#define INIT_GAME_POINT_KEY @"INIT_GAME_POINT_KEY"
#define UPDATE_NOTICE_VERSION @"1.2"
#define UPDATE_NOTICE_KEY @"UPDATE_NOTICE_KEY"

typedef enum {
	CAR = 1,
	USER_L = 2,
    USER_R
	
} ANIM_TYPE;

@implementation FruitMenuViewController
@synthesize iEnterNameTitleLabel;
@synthesize iHelpView;
@synthesize iHelpCloseButton;
@synthesize iHelpLeftButton;
@synthesize iHelpRightButton;
@synthesize iHelpPage;
@synthesize iHelpDesc1;
@synthesize iHelpDesc2;
@synthesize iSettingTitle;
@synthesize iUpdatePopUpView;
@synthesize iUpdatePopUpImage;
@synthesize iUpdatePopUpDesc;
@synthesize iUpdatePopUpDesc2;
@synthesize iUpdatePopUpCloseButton;
@synthesize iNewGameButton;
@synthesize iAnimationContainer;
@synthesize iMenuHousePic;
@synthesize iMenuButtonView;
@synthesize iCardCollectionButton;
@synthesize iDebugLabel;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    /*[iStartGameButton setExclusiveTouch:YES];
    [iResumeButton setExclusiveTouch:YES];
    [iGetPetPointsButton setExclusiveTouch:YES];
    [iMoreAppsButton setExclusiveTouch:YES];
    [iAchievementsButton setExclusiveTouch:YES];
    [iSettingView setExclusiveTouch:YES];
    [iProfileButton setExclusiveTouch:YES];
    [iHelpView setExclusiveTouch:YES];
    [iHelpButton setExclusiveTouch:YES];
    [iCardCollectionButton setExclusiveTouch:YES];*/
    showingHelp = NO;
    iAnimationViewArray = [[NSMutableArray alloc] init];
    
    [self addMenuAnimationViewWithType:(![Utilities isiPad])?USER_L:USER_R];
    [self addMenuAnimationViewWithType:(![Utilities isiPad])?USER_L:USER_R];
    [self addMenuAnimationViewWithType:(![Utilities isiPad])?USER_R:USER_L];
    [self addMenuAnimationViewWithType:(![Utilities isiPad])?USER_R:USER_L];
//    [self addMenuAnimationViewWithType:CAR];
//    [self addMenuAnimationViewWithType:CAR];
    
    DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
    
    if(sysProfile.dict != nil && [sysProfile.dict getData:CREATE_PROFILE_FIRST_TIME_KEY]==nil){
        NSString *userName = [[UIDevice currentDevice] name];
        
        userName = [userName stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"‚Äôs %@", [[UIDevice currentDevice] model]] withString:@""];
        DCProfile *profile = [[DCProfileManager sharedManager] createProfileWithReuseIndex];
        profile.name = userName;
        
        [sysProfile.dict setObject:[NSNumber numberWithBool:YES] forKey:CREATE_PROFILE_FIRST_TIME_KEY];
        
        [[DCProfileManager sharedManager] saveAllProfiles];
        [[DCProfileManager sharedManager] setCurrentProfileIndex:[profile.index intValue]];
    }
    
    if([sysProfile.dict getData:INIT_GAME_POINT_KEY] == nil){
        [sysProfile.dict setObject:[NSNumber numberWithBool:YES] forKey:INIT_GAME_POINT_KEY];
        [[DCProfileManager sharedManager] saveAllProfiles];
        [self.rootViewController addPoints:100];
    }
    
    [self hideBanner];
    
	[iDebugLabel setHidden:!DEBUG_SKIP];
	
    [super viewDidLoad];
}

-(void)viewWillUnload{
    for(int i = 0; i < [iAnimationViewArray count]; i ++){
        UIImageView *tAnimView = [iAnimationViewArray objectAtIndex:i];
        [tAnimView stopAnimating];
        [AnimViewManager stopAllActionForView:tAnimView];
    }
    [iAnimationViewArray removeAllObjects];
    [iAnimationViewArray release];
    iAnimationViewArray = nil;
}

-(void)addMenuAnimationViewWithType:(int)type{
//    NSString *retinaDisplaySuffix = @"";
//    if([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2 && [GameSetting getRetinaDisplay]){
//        retinaDisplaySuffix = @"-hd";
//    }
    UIImageView *iUser;
    if(type == USER_L || type == USER_R){
        if([Utilities isiPad]){
            iUser = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 300)]autorelease];
        }
        else{
            iUser = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)]autorelease];
        }
    }
    else if(type == CAR){
        iUser = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 136, 97)]autorelease];
    }
    iUser.tag = type;
    NSMutableArray *iArray = [NSMutableArray arrayWithCapacity:4];
    int rand = (arc4random()%10) + 1;
    for(int i = 0 ; i < 4; i++){
		NSString *spriteSheetName = [NSString stringWithFormat:@"cus%.2d.plist",rand];
		NSString *spriteFileName = [NSString stringWithFormat:@"cus%.2d.png",rand];
        if(type == USER_L){
            [iArray addObject:[self getImageFromSpriteSheet:spriteSheetName spriteFile:spriteFileName frameName:[NSString stringWithFormat:@"cus%.2d_walk_c_%.4d.png",rand,i] flipX:NO]];
        }
        else if(type == USER_R){
            [iArray addObject:[self getImageFromSpriteSheet:spriteSheetName spriteFile:spriteFileName frameName:[NSString stringWithFormat:@"cus%.2d_walk_c_%.4d.png",rand,i] flipX:YES]];
        }
        else if(type == CAR){
            [iArray addObject:[UIImage imageNamed:[NSString stringWithFormat:appendUniversalDeviceSuffixToFileName(@"car%d.png"),i+1]]];
        }
    }
    [iUser setAnimationImages:iArray];
    [iUser setAnimationDuration:0.5f];
    [iUser setAnimationRepeatCount:0];
    [iAnimationContainer addSubview:iUser];
    [iUser startAnimating];
    [iAnimationViewArray addObject:iUser];
    [self setAnimationPosition:iUser];
}

-(UIImage*)getImageFromSpriteSheet:(NSString*)spriteSheetFile spriteFile:(NSString*)spriteFileName frameName:(NSString*)frameName flipX:(BOOL)flipX{
	spriteSheetFile = appendUniversalDeviceSuffixToFileName(spriteSheetFile);
	if([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2 && [GameSetting getRetinaDisplay]){
		[NSString stringWithFormat:@"%@%@.%@", [[NSString stringWithFormat:@"%@", spriteSheetFile] stringByDeletingPathExtension], @"-hd", [[NSString stringWithFormat:@"%@", spriteSheetFile] pathExtension]];
	}
    NSDictionary *spriteSheetDict = [[(PrettyManager*)[PrettyManagerClass sharedManager]getDNADict:spriteSheetFile]getData:[NSString stringWithFormat:@"frames/%@",frameName]];
    float x = [[spriteSheetDict getData:@"x"]floatValue];
    float y = [[spriteSheetDict getData:@"y"]floatValue];
    float width = [[spriteSheetDict getData:@"width"]floatValue];
    float heigth = [[spriteSheetDict getData:@"height"]floatValue];
    float originalWidth = [[spriteSheetDict getData:@"originalWidth"]floatValue];
    float originalHeight = [[spriteSheetDict getData:@"originalHeight"]floatValue];
    
    CGSize newImageSize = CGSizeMake(originalWidth , originalHeight);
    CGRect newImageRect = CGRectMake(x, y, width, heigth);
    UIImage *spriteSheet = [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(spriteFileName)];
    UIImage *spriteImage = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([spriteSheet CGImage], newImageRect)];
    float drawAtX = (originalWidth / 2) - (width / 2);
    float drawAtY = (originalHeight / 2) - (heigth / 2);
    
    UIGraphicsBeginImageContext(newImageSize);
    [spriteImage drawAtPoint:CGPointMake(drawAtX, drawAtY)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if(flipX){
        UIGraphicsBeginImageContext(newImageSize);
        UIImageView *tImageView = [[[UIImageView alloc] initWithImage:newImage]autorelease];
        CGContextRef context = UIGraphicsGetCurrentContext();
        if(flipX){
            CGContextConcatCTM(context, CGAffineTransformMake(-1, 0, 0, 1, newImageSize.width, 0));
        }
        [tImageView.layer renderInContext:context];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return newImage;
}

-(void)setAnimationPosition:(UIImageView*)sender{
    CGPoint endPoint = CGPointMake(0, 0);
    CGPoint startPoint = CGPointMake(0, 0);
    CGSize frameSize = CGSizeMake(0, 0);
    float speed = 0;
    float delay = 0;
    if(sender.tag == USER_L){
        if([Utilities isiPad]){
            endPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:52],[GameUnit pixelLengthFromUnitLength:40]);
            startPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:-10],[GameUnit pixelLengthFromUnitLength:20]);
        }
        else{
            endPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:52],[GameUnit pixelLengthFromUnitLength:33]);
            startPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:-10],[GameUnit pixelLengthFromUnitLength:13]);
        }
        speed = 7.0 + ((arc4random()%40)/10.0f);
        delay = arc4random() % 5;
        if([Utilities isiPad]){
            frameSize = CGSizeMake(300, 300);
            speed = 3.0 + ((arc4random()%80)/10.0f);
        }
        else{
            frameSize = CGSizeMake(150, 150);
        }
        [sender setFrame:CGRectMake(startPoint.x, startPoint.y, frameSize.width, frameSize.height)];

    }
    else if (sender.tag == USER_R){
        if([Utilities isiPad]){
            startPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:52],[GameUnit pixelLengthFromUnitLength:33]);
            endPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:-4],[GameUnit pixelLengthFromUnitLength:22]);
        }
        else{
            startPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:52],[GameUnit pixelLengthFromUnitLength:32]);
            endPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:-4],[GameUnit pixelLengthFromUnitLength:21]);
        }
        speed = 7.0 + ((arc4random()%40)/10.0f);
        delay = arc4random() % 5;
        if([Utilities isiPad]){
            frameSize = CGSizeMake(300, 300);
            speed = 3.0 + ((arc4random()%80)/10.0f);
        }
        else{
            frameSize = CGSizeMake(150, 150);
        }
        [sender setFrame:CGRectMake(startPoint.x, startPoint.y, frameSize.width, frameSize.height)];
    }
    else if (sender.tag == CAR){
        endPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:40], [GameUnit pixelLengthFromUnitLength:40]);
        startPoint = CGPointMake([GameUnit pixelLengthFromUnitLength:-15], [GameUnit pixelLengthFromUnitLength:20]);
        [sender setFrame:CGRectMake(startPoint.x, startPoint.y, 136, 97)];
        speed = (2.0 + (arc4random() % 4))/2.0;
        delay = arc4random()%5;
        
        if([Utilities isiPad]){
            [sender setFrame:CGRectMake(startPoint.x, startPoint.y, 305, 200)];
            speed = speed * 2;
        }
    }
    
    [AnimViewManager addAnimView:sender actions:[avDelay actionWithDuration:delay],[avMove actionWithDuration:speed center:CGPointMake(endPoint.x, endPoint.y)],[avCallback setCallbackTarget:self selector:@selector(setAnimationPosition:)],nil];
}

- (void)viewFirstLoad {
	[super viewFirstLoad];
	
    DCSysProfile* SysProfile = [[DCProfileManager sharedManager] getSystemProfile];
    NSDictionary* updatePopUpDict = nil;
    if(SysProfile != nil){
        updatePopUpDict = [SysProfile.dict getData:@"updatePopUpDict"];
        if(updatePopUpDict == nil){
            [self showUpdatePopUpView];
        }
        else if([updatePopUpDict getData:UPDATE_POP_UP_KEY] == nil){
            [self showUpdatePopUpView];
        }
    }
    
    if (self.rootViewController != nil) {
        if(iUpdatePopUpView == nil){
            //[self.rootViewController showTapjoyFeaturedApp];
        }
        //[self.rootViewController showBannerWithViewController:self.rootViewController position:BANNER_BOTTOM animated:NO];
        //[self showNewsFeed];
    }
	
	bBgmEnable = [DataManager getBoolForKey:BGM_ENABLE_KEY setDefault:YES];
	bSfxEnable = [DataManager getBoolForKey:SFX_ENABLE_KEY setDefault:YES];
	
	[[OALSimpleAudio sharedInstance] setBgMuted:!bBgmEnable];
	[[OALSimpleAudio sharedInstance] setEffectsMuted:!bSfxEnable];
	
	[[OALSimpleAudio sharedInstance] playBg:@"menu.mp3" loop:YES];
    [self setDailyRewardsButtonEnabled:NO];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRewardsIcon) name:DailyRewardsManagerUpdatedNotification object:nil];
}

- (void)viewReload {
	[super viewReload];
	
	if (self.showTitle) {
		[self showTitleView];
	}
}

- (void)showTitleView{
    [super showTitleView];
    [iMenuButtonView setHidden:YES];
    DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
    [sysProfile.dict setObject:[NSNumber numberWithBool:NO] forKey:SHOULD_SHOW_TAKEOVER_KEY];
    [[DCProfileManager sharedManager] saveAllProfiles];
}

- (void)hideTitleView{
    [super hideTitleView];
    [iMenuButtonView setHidden:NO];
	NSString *bundleVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
	
    DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
    if(sysProfile.dict != nil && [sysProfile.dict getData:FREE_GAME_TIPS_SHOWN_KEY]==nil){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:LOCALIZATION_TEXT(@"FREE_GAME_TIPS") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        [sysProfile.dict setObject:[NSNumber numberWithBool:YES] forKey:FREE_GAME_TIPS_SHOWN_KEY];
		[sysProfile.dict setObject:UPDATE_NOTICE_VERSION forKey:UPDATE_NOTICE_KEY];
        [[DCProfileManager sharedManager] saveAllProfiles];
    } else if (sysProfile.dict != nil && [bundleVer isEqualToString:UPDATE_NOTICE_VERSION] )
	{
		BOOL newNotice = NO;
		//If we have shown an update notice before, check that it isn't the same key
		if ([sysProfile.dict getData:UPDATE_NOTICE_KEY] != nil)
		{

			//Safe conversion
			if ([[sysProfile.dict getData:UPDATE_NOTICE_KEY] isKindOfClass:[NSString class]])
			{
				if (![[sysProfile.dict getData:UPDATE_NOTICE_KEY] isEqualToString:UPDATE_NOTICE_VERSION]) newNotice = YES;
			}
			else if ([[sysProfile.dict getData:UPDATE_NOTICE_KEY] isKindOfClass:[NSNumber class]])
			{
				if (![[[sysProfile.dict getData:UPDATE_NOTICE_KEY] stringValue] isEqualToString:UPDATE_NOTICE_VERSION]) newNotice = YES;
			}
		}
		else 
		{
			newNotice = YES;
		}
		
		if (newNotice)
		{
			//If we're here, this isn't a new install (free game tips would've fired)
			//Also we have to check that the user has at least played the game, (day > 1)
			DCProfile* profile = [[DCProfileManager sharedManager] getCurrentProfile];
			if (profile)
			{
				NSMutableDictionary *stageDNADict = [profile.dict getData:[NSString stringWithFormat:@"stage/%d", 1]];
				int day = [[stageDNADict objectForKey:@"day" defaultValue:@"0"] intValue];
				if (day > 0)
				{
					UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:LOCALIZATION_TEXT(@"UPDATE_NOTICE") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alertView show];
					[alertView release];
					
					//Giving tokens here, valid for version 1.2 ONLY.
					if ([bundleVer isEqualToString:@"1.2"])
					{
						DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
						if (sysProfile != nil) {
							int newCredit = [[sysProfile.dict objectForKey:@"CREDIT_KEY"] intValue] + 30;
							[sysProfile.dict setObject:[NSNumber numberWithInt:newCredit] forKey:@"CREDIT_KEY"];
						}
					}
				}
			}
		}
		[sysProfile.dict setObject:UPDATE_NOTICE_VERSION forKey:UPDATE_NOTICE_KEY];
		[[DCProfileManager sharedManager] saveAllProfiles];
	}
    [self hideBanner];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
	[[NewsfeedViewController sharedController] removeFromCurrentView];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setIStartGameButton:nil];
    [self setIAchievementsButton:nil];
    [self setISettingButton:nil];
    [self setIGetPetPointsButton:nil];
    [self setIMoreAppsButton:nil];
    [self setIProfileButton:nil];
    [self setIHelpView:nil];
    [self setIHelpRightButton:nil];
    [self setIHelpLeftButton:nil];
    [self setIHelpCloseButton:nil];
    [self setIHelpButton:nil];
    [self setIAnimationContainer:nil];
    [self setICardCollectionButton:nil];
	[self setIDebugLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[[NewsfeedViewController sharedController] removeFromView:self.view];
    [iEnterNameTitleLabel release];
    [iStartGameButton release];
    [iAchievementsButton release];
    [iSettingButton release];
    [iGetPetPointsButton release];
    [iMoreAppsButton release];
    [iProfileButton release];
    [iHelpView release];
    [iHelpCloseButton release];
    [iHelpLeftButton release];
    [iHelpRightButton release];
    [iHelpView release];
    [iAnimationContainer release];
	[iDebugLabel release];
	[super dealloc];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
    [self.rootViewController hideBannerAnimated:NO];
	if (!self.showTitle && !showingHelp) {
		//[self.rootViewController showBannerWithViewController:self.rootViewController position:BANNER_BOTTOM animated:NO];
	}
    
    DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
    int level = 1;
	if (profile.dict != nil && [profile.dict getData:@"stage/1/level"] != nil) {
        level = [[profile.dict getData:@"stage/1/level"] intValue];
    }
    
	NSString *houseFileName = [NSString stringWithFormat:@"gd_title_house%d.png",level];
	
    [iMenuHousePic setImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(houseFileName)]];
    
    [self hideBanner];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    for(int i = 0 ; i < [iAnimationViewArray count]; i++){
        UIImageView *iAnim = [iAnimationViewArray objectAtIndex:i];
        [iAnimationContainer addSubview:iAnim];
        [self setAnimationPosition:iAnim];
        [iAnim startAnimating];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    for(int i = 0 ; i < [iAnimationViewArray count]; i++){
        UIImageView *iAnim = [iAnimationViewArray objectAtIndex:i];
        [AnimViewManager stopAllActionForView:iAnim];
    }
}

- (void)updateRewardsIcon{
    [self setDailyRewardsButtonEnabled:[[DailyRewardsManager sharedManager] canCollectDailyRewards]];
}

- (void)setDailyRewardsButtonEnabled:(BOOL)enable{
    if(enable){
        [self.iDailyRewardsButton setImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"gd_title_bonus1.png")] forState:UIControlStateNormal];
        if (self.presentedViewController == NULL) {
            [self performSelectorOnMainThread:@selector(dailyRewardsOnClick:) withObject:nil waitUntilDone:NO];
        }
        
        //[self dailyRewardsOnClick:nil];
    }
    else{
        [self.iDailyRewardsButton setImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"gd_title_bonus1a.png")] forState:UIControlStateNormal];
    }
}

- (void)updateGamePointLabel {
	[super updateGamePointLabel];
	
	// For achievements
	if ([GamePointManager gamePoint] > 0) {
		[[AchievementSystem sharedManager] updateAchievementWithID:car_ach_firstpp percent:COMPLETE_PERCENT];
	}
}

- (void)overAllGamePointDidChange {
	[super overAllGamePointDidChange];
	
	// For leadboard
	//[[GameCenter sharedManager] reportScore:[GamePointManager overAllGamePoint] forCategory:lb_overAllPetpoints];
}

// Override
- (void)updateCurrentProfileLabel {
	DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
	if (profile != nil) {
		self.iCurProfile.text = profile.name;
		if ([profile.dict objectForKey:@"currentStageDNAID"] != nil) {
            iResumeButton.enabled = YES;
            iNewGameButton.enabled = NO;
            iResumeButton.hidden = NO;
            iNewGameButton.hidden = YES;
		}
		else {
			iResumeButton.enabled = NO;
            iNewGameButton.enabled = YES;
            iResumeButton.hidden = YES;
            iNewGameButton.hidden = NO;
		}
	}
	else {
        self.iCurProfile.text = LOCALIZATION_TEXT(@"Create a profile");
        iResumeButton.enabled = NO;
        iNewGameButton.enabled = YES;
        iResumeButton.hidden = YES;
        iNewGameButton.hidden = NO;
	}
}

- (void)startGame {
    DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
	if (profile != nil) {
		[profile.dict setObject:[NSNumber numberWithInt:1]  forKey:@"currentStageDNAID"];
        [[DCProfileManager sharedManager] saveAllProfiles];
    }
	if (self.rootViewController != nil) {
		[self.rootViewController setNextProgramState:PROGRAM_LOADING_STATE];
	}
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	if (textField == iEnterNameTextField) {
		if (PROFILE_NAME_LENGTH <= 0) {
			// Unlimit
			return YES;
		}
		
		NSInteger insertDelta = string.length - range.length;
		
		if (insertDelta > 0) {
			if (textField.text.length + insertDelta > PROFILE_NAME_LENGTH) {
				// Over limit
				return NO;
			}
		}
	}
	
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == iEnterNameTextField) {
		if ([textField.text length] > 0) {
			[self enterNameDidFinish];
			[textField endEditing:YES];
		}
	}
	
	return YES;
}

-(void)showHelpView {
    if(iHelpView == nil){
        [[NSBundle mainBundle] loadNibNamed:AutoNIB(@"HelpView") owner:self options:nil];
		[self.view addSubview:iHelpView];
		iHelpView.center = CGPointMake(self.view.frame.size.width*0.5f, self.view.frame.size.height*0.5f);
        [iHelpCloseButton setExclusiveTouch:YES];
        [iHelpLeftButton setExclusiveTouch:YES];
        [iHelpRightButton setExclusiveTouch:YES];
    }
    curHelpPage = 1;
    [self updateHelpPage];
    [self.rootViewController hideBannerAnimated:YES];
    
    [AnimViewManager addAnimView:iHelpView actions:
	 [avScale setScale:0],
	 [avScale actionWithDuration:0.2f scale:1.2f],
	 [avScale actionWithDuration:0.1f scale:0.9f],
	 [avScale actionWithDuration:0.1f scale:1.0f],
	 nil];
    showingHelp = YES;
}

-(void)hideHelpView {
    if (iHelpView != nil) {
		[AnimViewManager addAnimView:iHelpView actions:
		 [avInteract setInteract:NO],
		 [avScale actionWithDuration:0.1f scale:1.2f],
		 [avScale actionWithDuration:0.2f scale:0],
		 [avCallback setCallbackTarget:self selector:@selector(releaseHelpView)],
		 nil];
	}
    showingHelp = NO;
    //[self.rootViewController showBannerWithViewController:self.rootViewController position:BANNER_BOTTOM animated:YES];
}

-(void)releaseHelpView{
    if (iHelpView != nil) {
		[iHelpView removeFromSuperview];
		[iHelpView release];
		iHelpView = nil;
	}
}

-(void)updateHelpPage{
    if(curHelpPage == 1){
        iHelpLeftButton.enabled = NO;
    }
    else{
        iHelpLeftButton.enabled = YES;
    }
    
    if(curHelpPage == 9){
        iHelpRightButton.enabled = NO;
    }
    else{
        iHelpRightButton.enabled = YES;
    }
    iHelpDesc1.hidden = YES;
    iHelpDesc2.hidden = YES;
    
    NSDictionary *iDict = [[[PrettyManager sharedManager] getDNADict:DIALOGUE_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/HelpPage/%d",curHelpPage]];
    if([iDict count] >= 1){
        int x = [[iDict getData:@"1/x"]floatValue];
        int y = [[iDict getData:@"1/y"]floatValue];
        int w = [[iDict getData:@"1/width"]floatValue];
        int h = [[iDict getData:@"1/height"]floatValue];
        [iHelpDesc1 setFont:[UIFont fontWithName:@"Verdana" size:15]];
        iHelpDesc1.frame = CGRectMake(x, y, w, h);
        NSString *Desc = [NSString stringWithFormat:@"%@",[iDict getData:@"1/desc"]];
        iHelpDesc1.text = Desc;
        iHelpDesc1.hidden = NO;
    }
    if([iDict count] >= 2){
        int x = [[iDict getData:@"2/x"]floatValue];
        int y = [[iDict getData:@"2/y"]floatValue];
        int w = [[iDict getData:@"2/width"]floatValue];
        int h = [[iDict getData:@"2/height"]floatValue];
        [iHelpDesc2 setFont:[UIFont fontWithName:@"Verdana" size:15]];
        iHelpDesc2.frame = CGRectMake(x, y, w, h);
        iHelpDesc2.text = [iDict getData:@"2/desc"];
        iHelpDesc2.hidden = NO;
    }
    
    [iHelpPage setImage:[UIImage imageNamed:[NSString stringWithFormat:appendUniversalDeviceSuffixToFileName(@"Help_Step%d.jpg"),curHelpPage]]];
}

-(IBAction)HelpLeftOnClick:(id)sender{
    if(curHelpPage > 1){
        [self playClickSound];
        curHelpPage--;
    }
    [self updateHelpPage];
}

-(IBAction)HelpRightOnClikc:(id)sender{
    if(curHelpPage < 9){
        [self playClickSound];
        curHelpPage++;
    }
    [self updateHelpPage];
}

-(IBAction)HelpCloseOnClick:(id)sender{
    [self playClickSound];
    [self hideHelpView];
}

-(IBAction)HelpButtonOnClick:(id)sender{
    [self playClickSound];
    [self showHelpView];
}

-(void)showUpdatePopUpView{
    /*if(iUpdatePopUpView == nil){
        [[NSBundle mainBundle] loadNibNamed:AutoNIB(@"UpdatePopUpView") owner:self options:nil];
		[self.view addSubview:iUpdatePopUpView];
		iHelpView.center = CGPointMake(self.view.frame.size.width*0.5f, self.view.frame.size.height*0.5f);
        [iUpdatePopUpCloseButton setExclusiveTouch:YES];
        NSDictionary *iDict = [[[PrettyManager sharedManager] getDNADict:DIALOGUE_DNA_FILE]getData:@"DNADict/UpdatePopUp"];
        [iUpdatePopUpDesc setText:[iDict getData:@"desc2"]];
        [iUpdatePopUpDesc2 setText:[iDict getData:@"desc1"]];
    }
    
    [AnimViewManager addAnimView:iUpdatePopUpView actions:
	 [avScale setScale:0],
	 [avScale actionWithDuration:0.2f scale:1.2f],
	 [avScale actionWithDuration:0.1f scale:0.9f],
	 [avScale actionWithDuration:0.1f scale:1.0f],
	 nil];*/
    
    DCSysProfile* SysProfile = [[DCProfileManager sharedManager] getSystemProfile];
    NSMutableDictionary* updatePopUpDict = nil;
    updatePopUpDict = [SysProfile.dict getData:@"updatePopUpDict"];
    if( updatePopUpDict == nil ){
        updatePopUpDict = [[[NSMutableDictionary alloc] init] autorelease];
    }
    [updatePopUpDict setObject:[NSNumber numberWithBool:YES] forKey:UPDATE_POP_UP_KEY];
    [SysProfile.dict setObject:updatePopUpDict forKey:@"updatePopUpDict"];
    [[DCProfileManager sharedManager]saveAllProfiles];
}

-(void)hideUpdatePopUpView{
    if (iUpdatePopUpView != nil) {
		[AnimViewManager addAnimView:iUpdatePopUpView actions:
		 [avInteract setInteract:NO],
		 [avScale actionWithDuration:0.1f scale:1.2f],
		 [avScale actionWithDuration:0.2f scale:0],
		 [avCallback setCallbackTarget:self selector:@selector(releaseUpdatePopUpView)],
		 nil];
	}
}

-(void)releaseUpdatePopUpView{
    if (iUpdatePopUpView != nil) {
		[iUpdatePopUpView removeFromSuperview];
		[iUpdatePopUpView release];
		iUpdatePopUpView = nil;
	}
}

-(IBAction)UpdatePopUpCloseButtonOnClick:(id)sender{
    [self hideUpdatePopUpView];
}

-(void)showSettingView{
    [super showSettingView];
    [self showBanner];
}

-(void)hideSettingView{
    [super hideSettingView];
    [self hideBanner];
}

@end
