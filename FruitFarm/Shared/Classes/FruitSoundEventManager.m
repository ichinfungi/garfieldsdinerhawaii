//
//  FruitSoundEventManager.m
//  DCGameTemplate
//
//  Created by royl on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitSoundEventManager.h"

@implementation FruitSoundEventManager

- (void)handlePrettySoundEventMusicPlayerStart:(NSObject*)data{
    [[OALSimpleAudio sharedInstance] playBg:@"musicbox.mp3" loop:YES];
}

- (void)handlePrettySoundEventQuestStart:(NSObject*)data
{
	//no-op, override to play a sound
}

- (void)handlePrettySoundEventQuestSuccess:(NSObject*)data
{
	//no-op, override to play a sound
}

- (void)handlePrettySoundEventQuestFail:(NSObject*)data
{
	//no-op, override to play a sound
}

- (void)handlePrettySoundEventQuestReportSuccess:(NSObject*)data
{
	[[OALSimpleAudio sharedInstance] playEffect:@"QuestCompleted.wav"];
}

- (void)handlePrettySoundEventQuestReportFail:(NSObject*)data
{
	[[OALSimpleAudio sharedInstance] playEffect:@"QuestFailed.wav"];
}

-(void)handlePrettySoundEventShowCardView:(NSObject*)data
{
    //override to play a sound
}

-(void)handlePrettySoundEventHideCardView:(NSObject*)data
{
    [[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
}

-(void)handlePrettySoundEventCardOnClick:(NSObject*)data
{
    [[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
}

-(void)handlePrettySoundEventCardFlip:(NSObject*)data
{
    [[OALSimpleAudio sharedInstance] playEffect:@"Card_Flip.wav"];
}
-(void)handlePrettySoundEventCardBrowsing:(NSObject*)data
{
    [[OALSimpleAudio sharedInstance] playEffect:@"Card_browsing.wav"];
}
-(void)handlePrettySoundEventCardClose:(NSObject*)data
{
    [[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
}

- (void)handlePrettySoundEventShowPauseView:(NSObject*)data
{
	[[OALSimpleAudio sharedInstance] playEffect:@"ui_popup.wav"];
}

- (void)handlePrettySoundEventShowPurchaseView:(NSObject*)data
{
	[[OALSimpleAudio sharedInstance] playEffect:@"ui_popup.wav"];
}

- (void)handlePrettySoundEventCardUnlockNewCard:(NSObject*)data{
    [[OALSimpleAudio sharedInstance] playEffect:@"card_unlocked.wav"]; 
}

- (void)handlePrettySoundEventDailyRewardsGetReward:(NSObject*)data;
{
	[[OALSimpleAudio sharedInstance] playEffect:@"moneyUp.wav"];
}

- (void)handlePrettySoundEventLevelUp:(NSObject*)data{
    
}

@end
