//
//  FruitRootViewController.m
//  DCGameTemplate
//
//  Created by royl on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitRootViewController.h"
#import "Utilities.h"
#import "FruitMenuViewController.h"
#import "DCProfileManager.h"
#import "NSDictionaryExtend.h"
#import "InAppPurchaseViewController.h"
#import "LuckyWheelViewController.h"
#import "ProfileViewController.h"
#import "SoundEventManager.h"

@implementation RootViewController (Fruit)

@end
