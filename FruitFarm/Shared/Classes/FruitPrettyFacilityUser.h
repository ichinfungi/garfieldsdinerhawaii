//
//  FruitPrettyFacilityUser.h
//  DCGameTemplate
//
//  Created by Calvin on 22/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrettyFacilityUser.h"
#import "DCSprite.h"

@interface FruitPrettyFacilityUser : PrettyFacilityUser {
	float _originalScale;
    BOOL _robotDragging;
}

@property(nonatomic,assign) float originalScale;
@property(nonatomic,assign) BOOL robotDragging;

- (void)robotDragStart;
- (void)robotDragEnd;

@end
