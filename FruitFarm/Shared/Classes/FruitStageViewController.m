//
//  FruitStageViewController.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 18/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitStageViewController.h"
#import "AnimViewManager.h"
#import "PrettyManager.h"
#import "GamePointManager.h"
#import "AchievementSystem.h"
#import "FruitGameSetting.h"
#import "NSDictionaryExtend.h"
#import "Localization.h"
#import "NewsfeedViewController.h"
#import "FruitLuckyWheelViewController.h"
#import "PrettyConsumableItemController.h"
#import "PrettySoundEventManager.h"
#import "FruitPrettyStage.h"
#import "PrettyCardCollectionViewController.h"
#import "DCProfileManager.h"
#import "GameUnit.h"
#import "FruitPrettyConsumableItemController.h"
#import "FruitPrettyManager.h"

#define STAR_FONT_OFFSET	-8

#define TEXT_COLOR_GREEN	[UIColor colorWithRed:17.0f/255.0f green:116.0f/255.0f blue:0 alpha:1]
#define TEXT_COLOR_RED		[UIColor colorWithRed:119.0f/255.0f green:0 blue:0 alpha:1]

@implementation FruitStageViewController
@synthesize iDialogueBoxImageView;
@synthesize iDialogueDimView;
@synthesize iDialogueTip;
@synthesize iReportLevelUpImageView;
@synthesize iReportTodayMoneyLabel;
@synthesize iReportTodayScoreLabel;
@synthesize iReportHappyCustomers;
@synthesize iReportSadCustomers;
@synthesize iReportReqMoney;
@synthesize iReportReqScore;
@synthesize iDimFilter;
//@synthesize iExpView;
//@synthesize iLevelStarImageView;
//@synthesize iExpStarImageView;
@synthesize iChangeMoneyLabel;
@synthesize iChangeScoreLabel;
@synthesize iMapButtom;
@synthesize iWheelNoticeImageView;
@synthesize iReportViewButton;
@synthesize iTodayReportLabel;
@synthesize iNextLvlReqLabel;
@synthesize iStaffBoostButton;
@synthesize iStaffBoostEffectImage;
@synthesize iStaffBoostIconImage;
@synthesize iWildCardEffectImage;
@synthesize iWildCardButton;
@synthesize iWildCardIconImage;
@synthesize iSpawnBoostButton;
@synthesize iSpawnBoostEffectImage;
@synthesize iSpawnBoostIconImage;
@synthesize iCatagoryButtonCard;
@synthesize iCatagoryButtonCoins;
@synthesize iCatagoryButtonDailyBonus;
@synthesize iCatagoryButtonStaffUpgrade;
@synthesize iCatagoryButtonFacilityUpgrade;
@synthesize iUnlockCardView;
@synthesize iUnlockCardViewBG;
@synthesize iUnlockCardViewChar;
@synthesize iGamePlayView;
@synthesize iGamePlaySpeedView;
@synthesize iGameplaySpeedUpButton;
@synthesize iGameplaySpeedDownButton;
@synthesize iGameplaySpeedLabel;
@synthesize iGameplayProfitLabel;
@synthesize iLevelUpSunShineImg;
@synthesize iLevelUpGarfieldIcon;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization.
		levelUpTimer = nil;
	}
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	// Init UI before super viewDidLoad
	[super viewDidLoad];
	
	iChangeMoneyLabel.hidden = YES;
	iChangeScoreLabel.hidden = YES;
    
    [DCGraphicEngine setMultipleTouchEnable:NO];
	
	iDimFilter.hidden = YES;
}

- (void)viewFirstLoad {
	
	[super viewFirstLoad];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dailyBonusDidReceive) name:LuckyWheelDailyBonusDidReceiveNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dailyBonusDidEarn) name:LuckyWheelDailyBonusDidEarnNotification object:nil];
}

- (void)viewReload {
	
    [AnimViewManager stopAllActionForView:iChangeMoneyLabel];
    [iChangeMoneyLabel setAlpha:0];
    [AnimViewManager stopAllActionForView:iChangeScoreLabel];
    [iChangeScoreLabel setAlpha:0];
    
	[super viewReload];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	if (self.iReportView != nil || self.iPurchaseView != nil) {
		/* REMOVE INGAME BANNER */
//		[self.rootViewController showBannerWithViewController:self.rootViewController position:BANNER_BOTTOM animated:NO];
	}
}

-(void)viewWillUnload{
    ((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iStaffBoostBtn = nil;
    ((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iWildCardBtn = nil;
    ((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iSpawnBoostBtn = nil;
    [super viewWillUnload];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return UIInterfaceOrientationIsLandscape(interfaceOrientation);
 }
 */


- (void)applicationWillResignActive{
    [super applicationWillResignActive];
    [AnimViewManager stopAllActionForView:iChangeScoreLabel];
    [iChangeScoreLabel setAlpha:0];
    [AnimViewManager stopAllActionForView:iChangeMoneyLabel];
    [iChangeMoneyLabel setAlpha:0];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
	[self setIDimFilter:nil];
    //[self setIExpView:nil];
	//[self setILevelStarImageView:nil];
	//[self setIExpStarImageView:nil];
	[self setIMapButtom:nil];
    [self setIReportLevelUpImageView:nil];
    [self setIReportTodayMoneyLabel:nil];
    [self setIReportTodayScoreLabel:nil];
    [self setIDialogueBoxImageView:nil];
	[self setIDialogueBoxView:nil];
	[self setIDialogueDimView:nil];
    [self setIDialogueTip:nil];
    [self setIChangeMoneyLabel:nil];
    [self setIChangeScoreLabel:nil];
    [self setIWheelNoticeImageView:nil];
    [self setIReportReqMoney:nil];
    [self setIReportReqScore:nil];
    [self setIReportHappyCustomers:nil];
    [self setIReportSadCustomers:nil];
    [self setIReportTodayMoneyLabel:nil];
    [self setIReportTodayScoreLabel:nil];
    [self setIReportViewButton:nil];
    [self setIStaffBoostButton:nil];
    [self setIStaffBoostEffectImage:nil];
    [self setIStaffBoostIconImage:nil];
    [self setISpawnBoostButton:nil];
    [self setISpawnBoostEffectImage:nil];
    [self setISpawnBoostIconImage:nil];
    [self setIWildCardButton:nil];
    [self setIWildCardEffectImage:nil];
    [self setIWildCardIconImage:nil];
    [self setICatagoryButtonCard:nil];
    [self setICatagoryButtonCoins:nil];
    [self setICatagoryButtonFacilityUpgrade:nil];
    [self setICatagoryButtonDailyBonus:nil];
    [self setICatagoryButtonStaffUpgrade:nil];
	[self setILevelUpSunShineImg:nil];
	[self setILevelUpGarfieldIcon:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[iDimFilter release];
	[iMapButtom release];
    [iReportLevelUpImageView release];
    [iReportTodayMoneyLabel release];
    [iReportTodayScoreLabel release];
    [iDialogueBoxImageView release];
	[iDialogueDimView release];
    [iDialogueTip release];
    [iChangeMoneyLabel release];
    [iChangeScoreLabel release];
    [iWheelNoticeImageView release];
    [iReportReqMoney release];
    [iReportReqScore release];
    [iReportHappyCustomers release];
    [iReportSadCustomers release];
    [iReportViewButton release];
    [iStaffBoostButton release];
    [iStaffBoostEffectImage release];
    [iStaffBoostIconImage release];
    [iSpawnBoostButton release];
    [iSpawnBoostEffectImage release];
    [iSpawnBoostIconImage release];
    [iWildCardButton release];
    [iWildCardEffectImage release];
    [iWildCardIconImage release];
    [iCatagoryButtonFacilityUpgrade release];
    [iCatagoryButtonCoins release];
    [iCatagoryButtonCard release];
    [iCatagoryButtonStaffUpgrade release];
    [iCatagoryButtonDailyBonus release];
	[iLevelUpSunShineImg release];
	[iLevelUpGarfieldIcon release];
    [super dealloc];
}

#pragma mark -
#pragma mark Class Methods
//- (CGPoint)getInterfaceCenter {
//	return CGPointMake(self.iInterfaceView.frame.size.width*0.5f, self.iInterfaceView.frame.size.height*0.5f+13.0f);
//}

- (void)updateGamePointLabel {
	[super updateGamePointLabel];
	
	// For achievements
	if ([GamePointManager gamePoint] > 0) {
		[[AchievementSystem sharedManager] updateAchievementWithID:car_ach_firstpp percent:COMPLETE_PERCENT];
	}
}

- (void)overAllGamePointDidChange {
	[super overAllGamePointDidChange];
	
	// For leadboard
	//[[GameCenter sharedManager] reportScore:[GamePointManager overAllGamePoint] forCategory:lb_overAllPetpoints];
}

#pragma mark -
#pragma mark Status Bar
- (void)setTime:(float)time {
	[super setTime:time];
	
	if( time >= [self.stage getDayLengthValue])
	{
		iTimeLabel.text = @"--:--";
		return;
	}
	
	// Disable dim filter
	//[self updateDimFilter:time animated:YES];
}

- (void)setLevel:(int)level {
    [self.iLevelLabel setText:[NSString stringWithFormat:@"%d", level]];
	//[starImageLabel setText:[NSString stringWithFormat:@"%d", level]];
}

- (void)updateDimFilter:(float)time animated:(BOOL)animated {
	float fTargetAlpha = 0;
	
	float fNightStartDuration = (float)(NIGHT_START - WORKINGHOUR_START) / (float)(WORKINGHOUR_END - WORKINGHOUR_START);
	float fNightEndDuration = (float)(NIGHT_END - WORKINGHOUR_START) / (float)(WORKINGHOUR_END - WORKINGHOUR_START);
	
	float fDuration = time / [self.stage getDayLengthValue];
	
	if (fDuration > fNightStartDuration && fDuration < fNightEndDuration) {
		fTargetAlpha = NIGHT_ALPHA * (fDuration - fNightStartDuration) / (fNightEndDuration - fNightStartDuration);
	}
	else if (fDuration >= fNightEndDuration) {
		fTargetAlpha = NIGHT_ALPHA;
	}
	
	// Update night filter
	if (animated) {
		float fOldAlpha = iDimFilter.alpha;
		
		float fDeltaAlpha = 0.01f;
		
		if (fabs(fTargetAlpha - fOldAlpha) > fDeltaAlpha) {
			if (fTargetAlpha > fOldAlpha) {
				fTargetAlpha = fOldAlpha + fDeltaAlpha;
			}
			else if (fTargetAlpha < fOldAlpha) {
				fTargetAlpha = fOldAlpha - fDeltaAlpha;
			}
		}
	}
	
	iDimFilter.alpha = fTargetAlpha;
}

- (void)setExpPercent:(float)percent animated:(BOOL)animated {
	/*float degree = fmin(fmax(0.0f, percent), 1.0f) * -180.0f;
	
	if (animated) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
		[UIView setAnimationDuration:0.25f];
	}
	
	iExpView.transform = CGAffineTransformMakeRotation(RADIANS(degree));
	
	if (animated) {
		[UIView commitAnimations];
		
		[AnimViewManager addAnimView:iExpStarImageView actions:
		 [avRotate setDegree:0],
		 [avRotate actionWithDuration:0.1f degree:90],
		 [avRotate actionWithDuration:0.1f degree:180],
		 [avRotate actionWithDuration:0.1f degree:270],
		 [[avRotate actionWithDuration:0.15f degree:360] setCurve:UIViewAnimationCurveEaseOut],
		 nil];
	}*/
}

- (void)setScore:(float)score {
    self.iScoreLabel.text = [NSString stringWithFormat:@"%.1f",score];
}

- (void)showChangeScore:(NSString*)text style:(CHANGE_STYLE)style {
	iChangeScoreLabel.hidden = NO;
	iChangeScoreLabel.alpha = 1;
	
	iChangeScoreLabel.text = text;
	iChangeScoreLabel.textColor = (style==CHANGE_STYLE_UP)?TEXT_COLOR_GREEN:TEXT_COLOR_RED;
	
	CGPoint startPoint = self.iScoreLabel.center;
	CGPoint endPoint = startPoint;
	if (style==CHANGE_STYLE_UP) {
		startPoint.y += 5.0f;
		endPoint.y -= 5.0f;
	}
	else {
		startPoint.y -= 5.0f;
		endPoint.y += 5.0f;
	}
	
	iChangeScoreLabel.center = startPoint;
	
	[AnimViewManager stopAllActionForView:iChangeScoreLabel];
	[AnimViewManager addAnimView:iChangeScoreLabel actions:
	 [avMove actionWithDuration:2.0f center:endPoint],
	 [avFade actionWithDuration:0.5f alpha:0],
	 nil];
}

- (void)showChangeMoney:(NSString*)text style:(CHANGE_STYLE)style {
	iChangeMoneyLabel.hidden = NO;
	iChangeMoneyLabel.alpha = 1;
	
	iChangeMoneyLabel.text = text;
	iChangeMoneyLabel.textColor = (style==CHANGE_STYLE_UP)?TEXT_COLOR_GREEN:TEXT_COLOR_RED;
	
	CGPoint startPoint = self.iMoneyLabel.center;
	CGPoint endPoint = startPoint;
	if (style==CHANGE_STYLE_UP) {
		startPoint.y += 5.0f;
		endPoint.y -= 5.0f;
	}
	else {
		startPoint.y -= 5.0f;
		endPoint.y += 5.0f;
	}
	
	iChangeMoneyLabel.center = startPoint;
	
	[AnimViewManager stopAllActionForView:iChangeMoneyLabel];
	[AnimViewManager addAnimView:iChangeMoneyLabel actions:
	 [avMove actionWithDuration:2.0f center:endPoint],
	 [avFade actionWithDuration:0.5f alpha:0],
	 nil];
}

#pragma mark -
#pragma mark Pause View
- (void)showPauseViewWithAnimation:(BOOL)animated {
	[[PrettySoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_SHOW_PAUSE_VIEW];
	//[super showPauseViewWithAnimation:animated];
    if([[_stage.questContoller getActiveQuests] count] > 0){
        QuestItem *q = [[_stage.questContoller getActiveQuests] objectAtIndex:0];
        if(iQuestReportViewController == nil){
            iQuestReportViewController = [[AutoClass(@"PrettyQuestReport") alloc] initWithQuest:q questController:_stage.questContoller stageViewController:self nibName:AutoNIB(@"QuestReportViewController") bundle:nil];
        }
        iQuestReportView = iQuestReportViewController.view;
        [iQuestReportView setCenter:screenCenter];
        [iInterfaceView addSubview:iQuestReportView];
		iQuestReportView.bounds = iInterfaceView.bounds;
		iQuestReportView.center = [self getInterfaceCenter];
        [_stage pauseGame];
    }
    
	[[NewsfeedViewController sharedController] addToView:self.iInterfaceView];
}

- (void)hidePauseView {
    [[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
	[super hidePauseView];
	[[NewsfeedViewController sharedController] removeFromView:self.iInterfaceView];
}

- (void) setPauseButtonEnable:(BOOL)enable
{
    self.iPauseButton.hidden = !enable;
    [super setPauseButtonEnable:enable];
}

-(void)pauseOnClick:(id)sender{
    if(_stage.isPausingGame){
        return;
    }
    else{
        [_stage pauseGameWithAnimation:YES];
    }
}

#pragma mark -
#pragma mark Purchase
- (void)showPurchaseView {
	[[PrettySoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_SHOW_PURCHASE_VIEW];
	[super showPurchaseView];
	self.iPurchaseView.center = [super getInterfaceCenter];

	/* REMOVE INGAME BANNER */
//	[self showBanner];
}

- (void)hidePurchaseView {
    [[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
	[super hidePurchaseView];
	
	[self hideBanner];
}

-(void)setPurchaseViewTitle:(NSString *)title imageName:(NSString *)imageName name:(NSString *)name desc:(NSString *)desc money:(int)money gamePoint:(int)gamePoint{
    [super setPurchaseViewTitle:title imageName:imageName name:name desc:desc money:money gamePoint:gamePoint];
    //iPurchaseImageView.contentMode = UIViewContentModeCenter;
}

#pragma mark -
#pragma mark Prepare
- (void)showPrepareView {
	[super showPrepareView];
	self.iPrepareView.center = [super getInterfaceCenter];
	iWheelNoticeImageView.hidden = YES;
	[LuckyWheelViewController checkDailyBonus];
}

- (void)setPrepareViewButton:(int)type target:(id)target selector:(SEL)selector {
	switch (type) {
		case STAGEVIEW_BTN_MAP:
			if (iMapButtom != nil) {
				[iMapButtom addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
			}
			break;
		default:
			[super setPrepareViewButton:type target:target selector:selector];
			break;
	}
}

- (IBAction)wheelOnClick:(id)sender {
	[[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
	
	if (self.rootViewController != nil) {
		if (self.rootViewController.presentedViewController == nil) {
			iWheelNoticeImageView.hidden = YES;
			FruitLuckyWheelViewController *viewController = [[[FruitLuckyWheelViewController alloc] initWithNibName:AutoNIB(@"LuckyWheelViewController") bundle:nil] autorelease];
			viewController.rootViewController = self.rootViewController;
			viewController.stage = self.stage;
			[self.rootViewController presentViewController:viewController animated:YES completion:nil];
		}
	}
}

- (void)dailyBonusDidReceive {
	if (self.iPrepareView != nil) {
		if (iWheelNoticeImageView != nil) {
			iWheelNoticeImageView.hidden = NO;
		}
	}
}

- (void)dailyBonusDidEarn {
	if (self.iPrepareView != nil) {
		if (iWheelNoticeImageView != nil) {
			iWheelNoticeImageView.hidden = YES;
		}
	}
}

#pragma mark -
#pragma mark Level Up View

- (void)releaseLevelUpView {
	if (levelUpTimer != nil) {
		[levelUpTimer invalidate];
		levelUpTimer = nil;
	}
	[super releaseLevelUpView];
}

- (void)bgAnimation {
	if (iLevelUpNewLvImg!=nil) {
		[self stopAllAction:iLevelUpNewLvImg];
		[AnimViewManager addAnimView:iLevelUpNewLvImg actions:
		 [avHidden setHidden:NO],
		 [avFade setAlpha:0],
		 [avFade actionWithDuration:0.8f alpha:1],
		 [avCallback setCallbackTarget:self selector:@selector(sunShineAnimation)],
		 nil];
	}
}

- (void)sunShineAnimation {
	if (iLevelUpSunShineImg!=nil) {
		
		if (_stage.level==2) {
			iLevelUpSunShineImg.center = (![Utilities isiPad])?CGPointMake(95, 122):CGPointMake(200, 220);
		} else {
			iLevelUpSunShineImg.center = (![Utilities isiPad])?CGPointMake(170, 122):CGPointMake(360, 220);
		}
		
		[self stopAllAction:iLevelUpSunShineImg];
		[AnimViewManager addAnimView:iLevelUpSunShineImg actions:
		 [avHidden setHidden:NO],
		 [avDelay actionWithDuration:0.2f],
		 [avCallback setCallbackTarget:self selector:@selector(garfieldIconAnimation)],
		 nil];
		
		if (levelUpTimer == nil) {
			levelUpTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f/60.0f target:self selector:@selector(levelupUpdate:) userInfo:nil repeats:YES];
		}
	}
}

- (void)levelupUpdate:(NSTimer*)timer {
	if (iLevelUpSunShineImg!=nil) {
		NSTimeInterval deltaTime = [timer timeInterval];
		float fRotate = deltaTime * 100.0f;
		iLevelUpSunShineImg.transform = CGAffineTransformRotate(iLevelUpSunShineImg.transform, RADIANS(fRotate));
	}
}

- (void)garfieldIconAnimation {
	if (iLevelUpGarfieldIcon!=nil) {
		[[PrettySoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_SHOW_PURCHASE_VIEW];
		
		[self stopAllAction:iLevelUpGarfieldIcon];
		[AnimViewManager addAnimView:iLevelUpGarfieldIcon actions:
		 [avScale setScale:0],
		 [avHidden setHidden:NO],
		 [[avScale actionWithDuration:0.2f scale:1.2] setCurve: UIViewAnimationCurveEaseOut],
		 [[avScale actionWithDuration:0.1f scale:1.0] setCurve: UIViewAnimationCurveEaseOut],
		 nil];
	}
}

#pragma mark -
#pragma mark Report

- (void)playLvUpSound{
    [[OALSimpleAudio sharedInstance] playEffect:@"levelUp.wav"];
}

- (void)showReportViewWithLevelUp:(BOOL)levelUp {
    if(self.stage.stageState == STAGE_RESULT){
        [[SoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_SHOW_REPORT_VIEW data:[NSNumber numberWithBool:levelUp]];
    }
    else{
        [[SoundEventManager sharedManager] handleSoundEvent:SOUND_EVENT_BUTTON_CLICK];
    }
	
	if (iReportView == nil) {
		[[NSBundle mainBundle] loadNibNamed:AutoNIB(@"ReportView") owner:self options:nil];
		[iInterfaceView addSubview:iReportView];
		iReportView.bounds = iInterfaceView.bounds;
		iReportView.center = [self getInterfaceCenter];
	}
	
	if (!STAGEVIEW_ANIMATION_ENABLE) {
		return;
	}
	
	[self setEAGLViewTouchDisable];
	[self scaleAppear:iReportView];
	self.iReportView.center = [super getInterfaceCenter];
	
	iReportLevelUpImageView.hidden = YES;
	
	if (!STAGEVIEW_ANIMATION_ENABLE) {
		return;
	}
	
	bWillShowRating = NO;
	
	if (levelUp) {
		[AnimViewManager addAnimView:iReportLevelUpImageView actions:
		 [avDelay actionWithDuration:0.6f],
         [avCallback setCallbackTarget:self selector:@selector(playLvUpSound)],
		 [avHidden setHidden:NO],
		 [avFade setAlpha:1],
		 [avScale setScale:2.0f],
         [avScale actionWithDuration:0.2f scale:1.0f]
		 ,nil];
	}
	
	// Show rating
	if (self.stage != nil) {
		if ((self.stage.dnaID == 1 && self.stage.level >= 4) ||
			self.stage.dnaID >= 2) {
			if ([FruitPrettyManager getFirstRatingGameDay]==0) {
				[FruitPrettyManager setFirstRatingGameDay:_stage.day];
			}
			NSLog(@"stage.day: %d; firstRatingDay: %d", _stage.day, [FruitPrettyManager getFirstRatingGameDay]);
			
			if ([FruitPrettyManager getFirstRatingGameDay]==_stage.day || (_stage.day-[FruitPrettyManager getFirstRatingGameDay])%5==0) {
				bWillShowRating = YES;
			}
		}
	}

	/* REMOVE INGAME BANNER */
//	[self.rootViewController showBannerWithViewController:self position:BANNER_BOTTOM animated:YES];
}

- (IBAction)iReportViewButtonOnClick{
    [self.stage pauseGame];
    [self showReportViewWithLevelUp:NO];
    BOOL noMoreLevel = (([self.stage getLevelUpMoney]) == 0 && ([self.stage getLevelUpScore] == 0));
    [self setReportViewTodayMoney:self.stage.todayMoney todayScore:self.stage.todayScore happyCustomers:(self.stage.todayPerfectFacilityUser+self.stage.todayNormalFacilityUser) sadCustomers:self.stage.todayWastedFacilityUser nextMoney:noMoreLevel?-1:[self.stage getLevelUpMoney] nextScore:noMoreLevel?-1:[self.stage getLevelUpScore]];
    [self.iReportOKButton addTarget:self action:@selector(resumeFromReportView) forControlEvents:UIControlEventTouchUpInside];
}

- (void)resumeFromReportView{
    [self.stage resumeGame];
    [self hideReportView];
}

- (void)setReportViewTodayMoney:(int)todayMoney todayScore:(float)todayScore happyCustomers:(int)happyCustomers sadCustomers:(int)sadCustomers nextMoney:(int)nextMoney nextScore:(float)nextScore
{
    //NSString *userLang=[[NSUserDefaults standardUserDefaults] stringForKey:@"UserLangPreference"];
    
    if(nextMoney >=0 && nextScore >=0){
        iReportReqMoney.text = [NSString stringWithFormat:@"%d", nextMoney];
        iReportReqScore.text = [NSString stringWithFormat:@"%.1f", nextScore]; 
    }
    else{
        iReportReqMoney.text = LOCALIZATION_TEXT(@"Coming soon");
        iReportReqScore.text = LOCALIZATION_TEXT(@"Coming soon");
    }
    iReportHappyCustomers.text = [NSString stringWithFormat:@"%d", happyCustomers];
    iReportSadCustomers.text = [NSString stringWithFormat:@"%d",sadCustomers];
    iReportTodayMoneyLabel.text = [NSString stringWithFormat:@"%d",todayMoney];
    iReportTodayScoreLabel.text = [NSString stringWithFormat:@"%.1f",todayScore];
    // NSLog(@"TODAY REPORT-->%@",iTodayReportLabel.text);

    iTodayReportLabel.text = LOCALIZATION_TEXT(@"Today's Report"); 
    iNextLvlReqLabel.text = LOCALIZATION_TEXT(@"Next Level Requirements");
}

- (void)hideReportView {
	[super hideReportView];
	[self hideBanner];
	
	if (bWillShowRating) {
		if (self.rootViewController != nil) {
			[self.rootViewController showRating];
		}
		bWillShowRating = NO;
	}
}

- (void)releaseReportView {
	[AnimViewManager stopAllActionForView:iReportLevelUpImageView];
	[super releaseReportView];
}

#pragma mark -
#pragma mark TransferPoint
- (IBAction)transferCancelOnClick:(id)sender {
	[self playClickSound];
    [super transferCancelOnClick:sender];
}

- (void)playTransferAnimation {
	[[OALSimpleAudio sharedInstance] playEffect:@"moneyUp.wav"];
	
	if (!STAGEVIEW_ANIMATION_ENABLE) {
		return;
	}
	

	CGPoint startPoint;
	CGPoint midPoint;
	CGPoint endPoint;
    if([Utilities isiPad]){
        startPoint = CGPointMake(182, 266);
        midPoint = CGPointMake(476, 267);
        endPoint = CGPointMake(599, 268);
    }
    else{
        startPoint = CGPointMake(105, 115);
        midPoint = CGPointMake(190, 115);
        endPoint = CGPointMake(280, 115);
    }
	
	for (int i=0; i<1; i++) {
		UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"wheel_pt.png")]] autorelease];
		imageView.center = startPoint;
		imageView.hidden = YES;
		[self.iTransferPointView addSubview:imageView];
		
		[AnimViewManager addAnimView:imageView actions:
		 [avDelay actionWithDuration:(float)i*0.2f],
		 [avHidden setHidden:NO],
		 [avScale setScale:0],
		 [avScale actionWithDuration:0.2f scale:1],
		 [[avMove actionWithDuration:0.25f center:midPoint] setCurve:UIViewAnimationCurveEaseIn],
		 [avCallback setCallbackTarget:self selector:@selector(changeTransferCoinImage:)],
		 [[avMove actionWithDuration:0.25f center:endPoint] setCurve:UIViewAnimationCurveEaseOut],
		 [avScale actionWithDuration:0.2f scale:0],
		 [avCallback setCallbackTarget:imageView selector:@selector(removeFromSuperview)],
		 nil];
	}
}

- (void)changeTransferCoinImage:(UIImageView*)imageView {
	imageView.image = [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"gd_report_money.png")];
}

#pragma mark -
#pragma mark Dialogue

- (void)showDialogueView {
	[super showDialogueView];
	
    //[_stage pauseGame];
    
	if (!STAGEVIEW_ANIMATION_ENABLE) {
		self.iDialogueDimView.alpha = 0.5;
		return;
	}
	
	// Dim bg
	[AnimViewManager stopAllActionForView:self.iDialogueDimView];
	[AnimViewManager addAnimView:self.iDialogueDimView actions:
	 [avFade setAlpha:0],
	 [avFade actionWithDuration:0.25 alpha:0.5],
	 nil];
    
    [[OALSimpleAudio sharedInstance] playBg:@"cutscene.mp3" loop:YES];
}

- (void)setDialogueDirection:(DIALOGUE_DIR)dir {
	curDialogueDir = dir;
	
	// Reset transform
	iDialogueCharImageView.transform = CGAffineTransformMakeScale(1, 1);
	iDialogueBoxView.transform = CGAffineTransformMakeScale(1, 1);
	
	CGRect rect;
	
	if (iDialogueCharImageView.image != nil) {
		rect.size = iDialogueCharImageView.image.size;
	}
	else {
		rect.size = CGSizeZero;
	}
	
	if (curDialogueDir == DIALOGUE_LEFT) {
		// Lefe
        rect.origin.x = 0;
		rect.origin.y = iDialogueView.frame.size.height-rect.size.height;
		iDialogueCharImageView.frame = rect;
		
		//iDialogueCharImageView.transform = CGAffineTransformMakeScale(-1, 1);
		
		rect = iDialogueBoxView.frame;
        rect.origin.x = iDialogueView.frame.size.width-rect.size.width;
		rect.origin.y = iDialogueView.frame.size.height-rect.size.height;
		iDialogueBoxView.frame = rect;
	}
	else {
		// Right
        rect.origin.x = iDialogueView.frame.size.width-rect.size.width;
        rect.origin.y = iDialogueView.frame.size.height-rect.size.height;
		iDialogueCharImageView.frame = rect;
        iDialogueCharImageView.transform = CGAffineTransformMakeScale(-1, 1);
		
		rect = iDialogueBoxView.frame;
        rect.origin.x = 0;
		rect.origin.y = iDialogueView.frame.size.height-rect.size.height;
        
        iDialogueBoxImageView.transform = CGAffineTransformMakeScale(-1, 1);
		iDialogueBoxView.frame = rect;
	}
}

- (void)hideDialogueView {
	if (!STAGEVIEW_ANIMATION_ENABLE) {
        //[_stage resumeGame];
		self.iDialogueDimView.alpha = 0;
		[super hideDialogueView];
		return;
	}
	
	if (self.iDialogueView != nil) {
		float fTime = 0.25f;
		
		// Dim bg
		[AnimViewManager stopAllActionForView:iDialogueDimView];
		[AnimViewManager addAnimView:iDialogueDimView actions:
		 [avFade actionWithDuration:fTime alpha:0],
		 nil];
		
		// Delay for release
		[AnimViewManager addAnimView:self.iDialogueView actions:
		 [avDelay actionWithDuration:fTime],
		 nil];
	}
	//[_stage resumeGame];
	[super hideDialogueView];
    //[_stage pauseGame];
    if(_stage.stageState == STAGE_STARTED){
        [[PrettySoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_STAGE_START];
    }
    else{
        [[PrettySoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_STAGE_PREPARE];
    }
}

- (void)releaseDialogueView {
	[AnimViewManager stopAllActionForView:iDialogueDimView];
	[super releaseDialogueView];
}

- (void)showNextDialogue {
    [super showNextDialogue];
    if([dialogueArray count] <= 0 ){
        return;
    }
    NSDictionary *iDict = [dialogueArray objectAtIndex:0];
    if([iDict objectForKey:@"perfromSelector"] != nil){
        NSString *sel = [iDict objectForKey:@"perfromSelector" defaultValue:nil];
        SEL selector = NSSelectorFromString(sel);
        [self performSelector:selector withObject:nil];
    }
    if([iDict objectForKey:@"tipPointer"] != nil){
        float x = [GameUnit pixelLengthFromUnitLength:[[iDict getData:@"tipPointer/x"]floatValue]];
        float y = [GameUnit pixelLengthFromUnitLength:[[iDict getData:@"tipPointer/y"]floatValue]];
        [iDialogueTip setFrame:CGRectMake(x, y, iDialogueTip.frame.size.width, iDialogueTip.frame.size.height)];
        [iDialogueTip setHidden:NO];
    }
    else{
        [iDialogueTip setHidden:YES];
    }
    if([iDict objectForKey:@"skipBtnPosition"] != nil){
        float x = [GameUnit pixelLengthFromUnitLength:[[iDict getData:@"skipBtnPosition/x"]floatValue]];
        float y = [GameUnit pixelLengthFromUnitLength:[[iDict getData:@"skipBtnPosition/y"]floatValue]];
        [iDialogueSkipButton setFrame:CGRectMake(x, y, iDialogueSkipButton.frame.size.width, iDialogueSkipButton.frame.size.height)];
    }
    
    [iDialogueBoxView setHidden:([iDict objectForKey:@"text"] == nil)];
}

#pragma mark - 
#pragma mark Banner
/*- (void)showBanner {
	[self.rootViewController showBannerWithViewController:self.rootViewController position:BANNER_BOTTOM animated:YES];
}*/

- (void)hideBanner {
	[self.rootViewController hideBannerAnimated:YES];
}

#pragma mark -
#pragma Consumable Item
-(IBAction)buyStaffBoostOnClick:(id)sender{
    [((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iStaffBoostBtn buttonOnClick:sender];
}

-(IBAction)buySpawnBoostOnClick:(id)sender{
    [((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iSpawnBoostBtn buttonOnClick:sender];
}

-(IBAction)buyWildCardOnClick:(id)sender{
    [((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iWildCardBtn buttonOnClick:sender];
}

#pragma mark -
#pragma Upgrade catagory
-(IBAction)showFacilityUpgradeOnClick{
    [((FruitPrettyStage*)_stage) showAllFacilityUpgradeBubble];
}

-(IBAction)showStaffUpgradeOnClick{
    [((FruitPrettyStage*)_stage) showAllStaffUpgradeBubble];
}

-(IBAction)showCardViewOnClick{
    PrettyCardCollectionViewController *iCardCollectionViewController = [[[AutoClass(@"PrettyCardCollectionViewController") alloc] initWithNibName:AutoNIB(@"PrettyCardCollectionViewController") bundle:nil] autorelease];
    iCardCollectionViewController.rootViewController = self.rootViewController;
	[self.rootViewController presentViewController:iCardCollectionViewController animated:YES completion:nil];
}

-(IBAction)showDailyRewardOnClick{
    [self dailyRewardsOnClick:nil];
}

-(void)setCatagoryButtonsEnable:(BOOL)enable{
    [iCatagoryButtonCard setHidden:!enable];
    [iCatagoryButtonCoins setHidden:!enable];
    [iCatagoryButtonFacilityUpgrade setHidden:!enable];
    [iCatagoryButtonStaffUpgrade setHidden:!enable];
    [iCatagoryButtonDailyBonus setHidden:!enable];
}

#pragma mark -
#pragma Card System
-(void)showUnlockCardAnim{
    [super showUnlockCardAnim];
    if(iUnlockCardView == nil){
        [[NSBundle mainBundle] loadNibNamed:AutoNIB(@"GotNewCardAnimation") owner:self options:nil];
        [iInterfaceView addSubview:iUnlockCardView];
		iUnlockCardView.bounds = iInterfaceView.bounds;
        iUnlockCardView.center = [self getInterfaceCenter];
    }
    if(!STAGEVIEW_ANIMATION_ENABLE){
        return;
    }
    [self setEAGLViewTouchDisable];
    
	CGPoint bgCenter = iUnlockCardViewBG.center;
	CGPoint charCenter = iUnlockCardViewChar.center;
    
	[iUnlockCardViewBG setCenter:CGPointMake(-[self getInterfaceCenter].x, bgCenter.y)];
	[iUnlockCardViewChar setCenter:CGPointMake(iInterfaceView.frame.size.width+[self getInterfaceCenter].x, charCenter.y)];
	[AnimViewManager addAnimView:iUnlockCardViewBG action:[avMove actionWithDuration:0.2f center:bgCenter]];
	[AnimViewManager addAnimView:iUnlockCardViewChar actions:[avMove actionWithDuration:0.2f center:charCenter],[avDelay actionWithDuration:2.0f],[avCallback setCallbackTarget:self selector:@selector(hideUnlockCardAnim)],nil];
}

-(void)hideUnlockCardAnim{
    if(!STAGEVIEW_ANIMATION_ENABLE){
        [self releaseUnlockCardAnim];
        return;
    }
    if(iUnlockCardView != nil){
		CGPoint bgCenter = iUnlockCardViewBG.center;
		CGPoint charCenter = iUnlockCardViewChar.center;
		
        [AnimViewManager addAnimView:iUnlockCardViewBG action:[avMove actionWithDuration:0.2f center:CGPointMake(-[self getInterfaceCenter].x, bgCenter.y)]];
		[AnimViewManager addAnimView:iUnlockCardViewChar actions:[avMove actionWithDuration:0.2f center:CGPointMake(iInterfaceView.frame.size.width+[self getInterfaceCenter].x, charCenter.y)],[avCallback setCallbackTarget:self selector:@selector(releaseUnlockCardAnim)],nil];
    }
    
}

-(void)releaseUnlockCardAnim{
    if(iUnlockCardView != nil){
        [self setEAGLViewTouchEnable];
        [AnimViewManager stopAllActionForView:iUnlockCardViewBG];
        [AnimViewManager stopAllActionForView:iUnlockCardViewChar];
        [iUnlockCardView removeFromSuperview];
        [iUnlockCardViewBG release];
        [iUnlockCardViewChar release];
        [iUnlockCardView release];
        iUnlockCardViewBG = nil;
        iUnlockCardViewChar = nil;
        iUnlockCardView = nil;
    }
}

#pragma mark -
#pragma mark Debug

-(void)addDebugButtonForPrepareView{
    [super addDebugButtonForPrepareView];
    /*UIButton *button = nil;
    UIView* parentView = self.iPrepareView;
    button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(parentView.frame.size.width-50, 55, 50, 20);
    [button setTitle:@"Debug stage" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoDebugStage) forControlEvents:UIControlEventTouchUpInside];
    [parentView addSubview:button];*/
}

-(void)gotoDebugStage{
    DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
	if (profile != nil) {
        [profile.dict setObject:@"2" forKey:@"currentStageDNAID"];
		
        [_stage saveStageAllDataToProfile];
        [self dismissAndBackToProgramState:PROGRAM_LOADING_STATE];
    }
}

#pragma mark - 
#pragma mark Speed Up

-(void)showGameplayView{
    [super showGameplayView];
    /*if(self.iGamePlayView == nil){
        [[NSBundle mainBundle] loadNibNamed:AutoNIB(@"GamePlayView") owner:self options:nil];
        [self.iInterfaceView addSubview:iGamePlayView];
    }
    
    if(!STAGEVIEW_ANIMATION_ENABLE){
        return;
    }
    
    //[self showSpeedView];*/
    [self updateSpeedInterface];
}

-(void)hideGameplayView{
    if (self.iGameplayView != nil) {
		[self fadeDisappear:self.iGameplayView];
		[AnimViewManager addAnimView:self.iGameplayView action:[avCallback setCallbackTarget:self selector:@selector(releaseGameplayView)]];
	}
}

- (void)releaseGameplayView {
	if (self.iGameplayView != nil) {
		[AnimViewManager stopAllActionForView:self.iGamePlaySpeedView];
		[AnimViewManager stopAllActionForView:self.iGameplayView];
		[self.iGameplayView removeFromSuperview];
		self.iGameplayView = nil;
		self.iGamePlaySpeedView = nil;
	}
}

-(void)updateSpeedInterface{
    self.iGameplaySpeedUpButton.enabled = (self.stage.gameSpeed < 4);
	self.iGameplaySpeedDownButton.enabled = (self.stage.gameSpeed > 1);
	
	self.iGameplaySpeedLabel.text = [NSString stringWithFormat:@"%.fx", self.stage.gameSpeed];
    self.iGameplayProfitLabel.text = [NSString stringWithFormat:@"+%.f%%", ([self.stage getSpeedUpBonusRatio]-1.0f)*100.0f];
}

-(IBAction)gameplaySpeedOnClick:(id)sender{
    [[SoundEventManager sharedManager] handleSoundEvent:SOUND_EVENT_BUTTON_CLICK];
    if(sender == self.iGameplaySpeedUpButton){
        self.stage.gameSpeed = fmin(self.stage.gameSpeed + 1 , 4);
    }
    if(sender == self.iGameplaySpeedDownButton){
        self.stage.gameSpeed = fmax(self.stage.gameSpeed - 1, 1);
    }
    [self updateSpeedInterface];
}



@end
