//
//  FruitLuckyWheelViewController.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 20/04/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitLuckyWheelViewController.h"
#import "DCProfileManager.h"
#import "GamePointManager.h"
#import "NSDictionaryExtend.h"

#import "Utilities.h"
#import "FruitPrettyStage.h"
#import "AnimViewManager.h"
#import "GameUnit.h"
#import "LuckyWheelViewController.h"
#import "Localization.h"
#import "PrettyManager.h"
#import "JRSwizzle.h"

#define CREDIT_KEY	@"CREDIT_KEY"

@implementation LuckyWheelViewController (Extend)
#pragma mark - Daily Bonus Data

+ (void)load
{
	// method swizzling
	[self jr_swizzleClassMethod:@selector(creditPerBonus) withClassMethod:@selector(creditPerBonus_f) error:nil];
	[self jr_swizzleClassMethod:@selector(initialBonus) withClassMethod:@selector(initialBonus_f) error:nil];
	[self jr_swizzleClassMethod:@selector(playGetBonusSound) withClassMethod:@selector(playGetBonusSound_f) error:nil];
	[self jr_swizzleClassMethod:@selector(getRequireInternetConnectionText) withClassMethod:@selector(getRequireInternetConnectionText_f) error:nil];
	[self jr_swizzleClassMethod:@selector(getEarnedDailyCreditsText:) withClassMethod:@selector(getEarnedDailyCreditsText_f:) error:nil];
	[self jr_swizzleClassMethod:@selector(getReceivedAlertText:) withClassMethod:@selector(getReceivedAlertText_f:) error:nil];
}

+ (int)creditPerBonus_f {
	return 2;
}

+ (int)initialBonus_f {
	return 5;
}

#pragma mark - Daily Bonus
+ (void)playGetBonusSound_f {
	[[OALSimpleAudio sharedInstance] playEffect:@"moneyUp.wav"];
}

+ (NSString*)getRequireInternetConnectionText_f{
    return LOCALIZATION_TEXT(@"WHEEL_INTERNET_REQUIRE_TEXT");
}

+ (NSString*)getEarnedDailyCreditsText_f:(int)credit{
    int maxCredit = [self maxDayOfBonus] * [self creditPerBonus];
    
    return [NSString stringWithFormat:LOCALIZATION_TEXT(@"WHEEL_CREDIT_EARNED_TEXT"),credit, [self creditName:credit],
            [self creditPerBonus], [self creditName:[self creditPerBonus]],
            maxCredit, [self creditName:maxCredit]];
}

+(NSString*) getReceivedAlertText_f:(NSNumber*)day{
    return [NSString stringWithFormat:LOCALIZATION_TEXT(@"WHEEL_RECEIVED_ALERT_TEXT"), [PrettyManagerClass getGamePointPluralName], [day intValue]*[self creditPerBonus], [self maxDayOfBonus]*[self creditPerBonus]];
}

@end



@implementation FruitLuckyWheelViewController

@synthesize rootViewController;
@synthesize stage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		rootViewController = nil;
		stage = nil;
    }
    return self;
}

- (void)dealloc
{
	rootViewController = nil;
	stage = nil;
	[super dealloc];
}

- (void)viewFirstLoad {
	[super viewFirstLoad];
	
	// Change music
	[[OALSimpleAudio sharedInstance] playBg:@"menu.mp3" loop:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleGamePointDidChange) name:GamePointDidChangeNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[self onGamePointChanged];
	
	if (IsiPad) {
		[self showBanner];
	}
}

- (void)dismiss {
	[super dismiss];
	[self dismissViewControllerAnimated:YES completion:nil];
	
	if (rootViewController != nil) {
		//[rootViewController cancelTapjoyOffers];
	}
	
	// Restore Music
	[[OALSimpleAudio sharedInstance] playBg:@"prepare.mp3" loop:YES];
}

- (void)packageDidBuy:(WHEEL_CREDIT_PACKAGE)package {
	[super packageDidBuy:package];
	
	// Tapjoy pay per action: Convert Pet point to lottery token
    /*if([Utilities isiPad]){
        [TapjoyConnect actionComplete:@"a064a2d7-c571-40c6-821f-1295a5d123e4"];
    }
    else{
        [TapjoyConnect actionComplete:@"4c6997b2-dfe4-43e6-bd61-bcaf19b810cb"];
    }*/
}

- (IBAction)buyPointOnClick:(id)sender {
	[self playClickSound];
	
	if (rootViewController != nil) {
		[rootViewController showInAppPurchaseWithViewController:self];
	}
}

- (IBAction)earnFreePointOnClick:(id)sender {
	[self playClickSound];
	
	[[MunerisAppEvents report:@"offerwall" withViewController:self] execute];
}

- (void)handleGamePointDidChange {
	[self onGamePointChanged];
}

#pragma mark - Data Control
- (int)getMoney {
	if (stage != nil) {
		return stage.money;
	}
	else {
		return 0;
	}
}

- (void)addMoney:(int)money {
	if (stage != nil) {
		[(FruitPrettyStage*)stage addMoneyWithoutAnimate:money];
		[stage saveStageGameDataToProfile];
	}
}

- (int)getGamePoint {
	return [[GamePointManager sharedManager] gamePoint];
}

- (void)addGamePoint:(int)point {
	[[GamePointManager sharedManager] addGamePoint:point];
}

- (void)spendGamePoint:(int)point {
	[self addGamePoint:-point];
}

- (int)getCredit {
	DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
	if (sysProfile != nil) {
		return [[sysProfile.dict objectForKey:CREDIT_KEY defaultValue:[NSNumber numberWithInt:0]] intValue];
	}
	else {
		return 0;
	}
}

- (void)addCredit:(int)credit {
	DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
	if (sysProfile != nil) {
		int newCredit = [self getCredit] + credit;
		[sysProfile.dict setObject:[NSNumber numberWithInt:newCredit] forKey:CREDIT_KEY];
	}
}

- (void)spendCredit:(int)credit {
	[self addCredit:-credit];
}

- (int)getPackageGamePoint:(WHEEL_CREDIT_PACKAGE)package {
	switch (package) {
		case WHEEL_CREDIT_PACKAGE1:
			return 3;
		case WHEEL_CREDIT_PACKAGE2:
			return 20;
		case WHEEL_CREDIT_PACKAGE3:
			return 50;
		default:
			return 0;
	}
}

- (int)getPackageCredit:(WHEEL_CREDIT_PACKAGE)package {
	switch (package) {
		case WHEEL_CREDIT_PACKAGE1:
			return 1;
		case WHEEL_CREDIT_PACKAGE2:
			return 7;
		case WHEEL_CREDIT_PACKAGE3:
			return 20;
		default:
			return 0;
	}
}

- (void)updateCreditLabel {
	iCreditLabel.text = [NSString stringWithFormat:@"x %d", [self getCredit]];
}

- (void)updateSpinButton {
	iInvisibleAddCreditButton.enabled = NO;
	
	switch (_curState) {
		case WHEEL_FREE:
			//[iSpinButton setTitle:@"Spin!" forState:UIControlStateNormal];
            [iSpinButton setBackgroundImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Wheel_spin_play.png")] forState:UIControlStateNormal];
			iSpinButton.enabled = ([self getCredit] > 0);
			iInvisibleAddCreditButton.enabled = YES;
			break;
		case WHEEL_SPINNING:
			if (_wheelMode == WHEEL_STARTANDSTOP) {
                [iSpinButton setBackgroundImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Wheel_spin_stop.png")] forState:UIControlStateNormal];
				//[iSpinButton setTitle:@"Stop!" forState:UIControlStateNormal];
				iSpinButton.enabled = YES;
			}
			break;
		default:
			iSpinButton.enabled = NO;
			break;
	}
}

- (void)addPrizeText:(NSString*)text color:(WHEEL_COLOR)wheelColor {
	// View
	UIView *prizeView = [[[UIView alloc] initWithFrame:CGRectMake(204, 70+wheelColor*37, 252, 37)] autorelease];
	prizeView.backgroundColor = [UIColor clearColor];
	prizeView.clipsToBounds = NO;
	[iWheelInterfaceView insertSubview:prizeView belowSubview:iAnimationContainerView];
	
	// Bg
	UIImageView *bgImageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"prize_List.png")] highlightedImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"wheel_result_highlight.png")]] autorelease];
	[prizeView addSubview:bgImageView];
	bgImageView.center = CGPointMake(prizeView.frame.size.width*0.5f, prizeView.frame.size.height*0.5f);
	[_prizeBgDict setObject:bgImageView forKey:[NSString stringWithFormat:@"%d", wheelColor]];
	
	// Ball
	UIImageView *ballImageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:appendUniversalDeviceSuffixToFileName(@"prize_%d.png"), wheelColor+1]]] autorelease];
	[prizeView addSubview:ballImageView];
	ballImageView.center = CGPointMake(bgImageView.center.x-[GameUnit pixelLengthFromUnitLength:9.6], bgImageView.center.y);
	
	// Label
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 176, 37)] autorelease];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor brownColor];
	label.textAlignment = NSTextAlignmentCenter;
	label.font = [UIFont fontWithName:@"Verdana" size:18];
	label.text = text;
	[prizeView addSubview:label];
	label.center = CGPointMake(bgImageView.center.x+18.0f, bgImageView.center.y+2.0f);
    
    if([Utilities isiPad]){
        [prizeView setFrame:CGRectMake(556, 248+wheelColor*70, 538, 89)];
        [label setFrame:CGRectMake(100, 0, 352, 74)];
        [label setFont:[UIFont fontWithName:@"Verdana" size:36]];
        [label setCenter:CGPointMake(bgImageView.center.x + 36.0f, bgImageView.center.y+4.0f)];
        [ballImageView setCenter:CGPointMake(bgImageView.center.x-[GameUnit pixelLengthFromUnitLength:9.2], bgImageView.center.y)];
    }
}

- (void)playAwardMoneyAnimation:(WHEEL_COLOR)color {
	if (!_bIsAppear) {
		[self updateMoneyLabel];
		[self playGetAwardSound];
		return;
	}
	
	UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"wheel_current_money.png")]] autorelease];
	imageView.contentMode = UIViewContentModeScaleToFill;
    CGPoint destPoint = CGPointMake(214, 0);
    imageView.center = CGPointMake(225, 49+37*color);

    
	[iAnimationContainerView addSubview:imageView];
	float duration = CGPointDistance(imageView.center, destPoint)*0.002f;
	
    if([Utilities isiPad]){
        imageView.center = CGPointMake(455, 265+70*color);
        destPoint = CGPointMake(432, 142);
        duration = duration * 2;
    }
    
	[AnimViewManager addAnimView:imageView actions:
	 [avScale setScale:0],
	 [avCallback setCallbackTarget:self selector:@selector(playSpawnAwardSound)],
	 [avScale actionWithDuration:0.4f scale:1.2f],
	 [avScale actionWithDuration:0.2f scale:0.9f],
	 [avScale actionWithDuration:0.2f scale:1.0f],
	 [[avMove actionWithDuration:duration center:destPoint] setCurve:UIViewAnimationCurveEaseInOut],
	 [avScale actionWithDuration:0.1f scale:1.1f],
	 [avScale actionWithDuration:0.3f scale:0],
	 [avCallback setCallbackTarget:self selector:@selector(updateMoneyLabel)],
	 [avCallback setCallbackTarget:self selector:@selector(playGetAwardSound)],
	 [avCallback setCallbackTarget:imageView selector:@selector(removeFromSuperview)],
	 nil];
}

- (void)playAwardGamePointAnimation:(WHEEL_COLOR)color {
	if (!_bIsAppear) {
		[self updateGamePointLabel];
		[self playGetAwardSound];
		return;
	}
	
	UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"wheel_pt.png")]] autorelease];
	imageView.contentMode = UIViewContentModeScaleToFill;
	imageView.center = CGPointMake(225, 49+37*color);
	[iAnimationContainerView addSubview:imageView];
	
	CGPoint destPoint = CGPointMake(341, 0);

	float duration = CGPointDistance(imageView.center, destPoint)*0.002f;
    if([Utilities isiPad]){
        imageView.center = CGPointMake(455, 265+70*color);
        destPoint = CGPointMake(694, 144);
        duration = duration * 2;
    }
	[AnimViewManager addAnimView:imageView actions:
	 [avScale setScale:0],
	 [avCallback setCallbackTarget:self selector:@selector(playSpawnAwardSound)],
	 [avScale actionWithDuration:0.4f scale:1.2f],
	 [avScale actionWithDuration:0.2f scale:0.9f],
	 [avScale actionWithDuration:0.2f scale:1.0f],
	 [[avMove actionWithDuration:duration center:destPoint] setCurve:UIViewAnimationCurveEaseInOut],
	 [avScale actionWithDuration:0.1f scale:1.1f],
	 [avScale actionWithDuration:0.3f scale:0],
	 [avCallback setCallbackTarget:self selector:@selector(updateGamePointLabel)],
	 [avCallback setCallbackTarget:self selector:@selector(playGetAwardSound)],
	 [avCallback setCallbackTarget:imageView selector:@selector(removeFromSuperview)],
	 nil];
}

- (void)playBuyCreditAnimation {
	[self updateGamePointLabel];
	
	if (!_bIsAppear) {
		[self updateCreditLabel];
		return;
	}
	
	UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"wheel_wheel.png")]] autorelease];
	imageView.contentMode = UIViewContentModeScaleToFill;
	imageView.frame = CGRectMake(53, 125, 29, 29);
    if([Utilities isiPad]){
        imageView.frame = CGRectMake(88, 400, 61, 60);
    }
	[iAnimationContainerView addSubview:imageView];
	
	[AnimViewManager addAnimView:imageView actions:
	 [[avMove actionWithDuration:0.2f center:CGPointMake(imageView.center.x, imageView.center.y+[GameUnit pixelLengthFromUnitLength:2.5])] setCurve:UIViewAnimationCurveEaseIn],
	 [avCallback setCallbackTarget:self selector:@selector(updateCreditLabel)],
	 [avCallback setCallbackTarget:imageView selector:@selector(removeFromSuperview)],
	 nil];
	
	imageView.alpha = 0;
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.15f];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	imageView.alpha = 1;
	[UIView commitAnimations];
}

- (void)playSpendCreditAnimation {
    [self updateCreditLabel];
	
	if (!_bIsAppear) {
		return;
	}
	
	UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Credits_Icon.png")]] autorelease];
	imageView.contentMode = UIViewContentModeScaleToFill;
	imageView.frame = CGRectMake(53, 190, 29, 29);
    if([Utilities isiPad]){
        imageView.frame = CGRectMake(88, 444, 61, 60);
    }
	[iAnimationContainerView addSubview:imageView];
	
	[AnimViewManager addAnimView:imageView actions:
	 [[avMove actionWithDuration:0.4f center:CGPointMake(imageView.center.x, imageView.center.y+50)] setCurve:UIViewAnimationCurveEaseIn],
	 [avCallback setCallbackTarget:imageView selector:@selector(removeFromSuperview)],
	 nil];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelay:0.2f];
	[UIView setAnimationDuration:0.15f];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	imageView.alpha = 0;
	[UIView commitAnimations];
}
    
#pragma mark - Sound
- (void)playClickSound {
	[[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
}

- (void)playSpinSound {
	[[OALSimpleAudio sharedInstance] playEffect:@"moneyUp.wav"];
}

- (void)playSpawnAwardSound {
	[[OALSimpleAudio sharedInstance] playEffect:@"clickEffect.wav"];
}

- (void)playGetAwardSound {
	[[OALSimpleAudio sharedInstance] playEffect:@"getAward.wav"];
}

- (void)playBuyCreditSound {
	[[OALSimpleAudio sharedInstance] playEffect:@"moneyUp.wav"];
}

@end

