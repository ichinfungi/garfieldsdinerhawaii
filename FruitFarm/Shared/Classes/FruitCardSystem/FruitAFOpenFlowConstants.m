//
//  FruitAFOpenFlowConstants.m
//  DCGameTemplate
//
//  Created by royl on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitAFOpenFlowConstants.h"

@implementation FruitAFOpenFlowConstants

+ (float)getCoverSpacing{
    if([Utilities isiPad]){
        return 240;
    }
    return 120;
}
+ (float)getCenterCoverOffset{
    return 0;
}
+ (float)getSideCoverAngle{
    return 0.1;
}
+ (float)getSideCoverZPosition{
    return -60;
}
+ (float)getReflectionOffset{
    return 10000;
}
+ (float)getOffsetX{
    return 0;
}
+ (float)getOffsetY{
    return 40;
}

@end
