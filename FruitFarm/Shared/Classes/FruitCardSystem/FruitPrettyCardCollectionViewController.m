//
//  FruitPrettyCardCollectionViewController.m
//  DCGameTemplate
//
//  Created by royl on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyCardCollectionViewController.h"

@implementation FruitPrettyCardCollectionViewController

/* REMOVE INGAME BANNER */
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if (self.stage!=nil) {
		[self hideBanner];
	}
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self showBanner];
}

-(void)closeOnClick{
    [super closeOnClick];
    [self hideBanner];
}

@end
