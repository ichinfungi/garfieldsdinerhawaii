//
//  FruitLuckyWheelViewController.h
//  DCGameTemplate
//
//  Created by Dream Cortex on 20/04/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LuckyWheelViewController.h"
#import "RootViewController.h"
#import "PrettyStage.h"

@interface LuckyWheelViewController (Extend)

@end

@interface FruitLuckyWheelViewController : LuckyWheelViewController {
    
}

@property (nonatomic, assign) RootViewController *rootViewController;
@property (nonatomic, assign) PrettyStage *stage;

@end
