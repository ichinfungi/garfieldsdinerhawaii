//
//  FruitPrettyStaff.m
//  DCGameTemplate
//
//  Created by Chan Calvin on 03/04/2011.
//  Copyright 2011 Infunity. All rights reserved.
//

#import "FruitPrettyStaff.h"
#import "NSDictionaryExtend.h"
#import "PrettyManager.h"
#import "Utilities.h"
#import "GameCharacter.h"
#import "PrettyFacility.h"


@implementation FruitPrettyStaff

// override for infinity level upgrade
- (id)init:(int)pDNAID ID:(int)pID level:(int)pLevel position:(CGPoint)pPosition
{
	self = [super init:pDNAID ID:pID level:pLevel position:pPosition];
	if (self != nil )
	{		
		NSDictionary *dnaDict = [[[PrettyManager sharedManager] getDNADict:STAFF_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", _dnaID]];
		
        if ([Utilities isiPad]){ 
            if ([dnaDict getData:[NSString stringWithFormat:@"level/%d", _level]] != nil) {
                self.walkPPS = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/walkPPS", _level]] floatValue] * 1024.0f/480.0f;
                self.serviceSpeed = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/serviceSpeed", _level]] floatValue];
            }
        }
        else{
            if ([dnaDict getData:[NSString stringWithFormat:@"level/%d", _level]] != nil) {
                self.walkPPS = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/walkPPS", _level]] floatValue];
                self.serviceSpeed = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/serviceSpeed", _level]] floatValue];
            }
        }
		
		if ([dnaDict getData:[NSString stringWithFormat:@"level/%d", _level+1]] != nil) {
			self.upgradeMoney = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/upgrade/money", (_level+1)]] floatValue];
			self.upgradeGamePoint = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/upgrade/gamePoint", (_level+1)]] floatValue];
			self.upgradeDesc = [dnaDict getData:[NSString stringWithFormat:@"level/%d/upgrade/desc", (_level+1)]];
		}
        else{
            //don't show upgrade bubble if no more level
            self.upgradeMoney = -1;
            self.upgradeGamePoint = -1;
        }
        
	}
	return self;
}


@end
