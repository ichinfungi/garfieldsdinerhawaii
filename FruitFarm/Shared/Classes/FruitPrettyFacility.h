//
//  FruitPrettyFacility.h
//  DCGameTemplate
//
//  Created by Calvin on 22/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrettyFacility.h"

@interface FruitPrettyFacility : PrettyFacility {
    
}

@property (nonatomic, assign) int waitingUser;

@end
