//
//  FruitInAppPurchaseSetting.h
//  DCGameTemplate
//
//  Created by Dream Cortex on 25/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
/*
#define PACKAGE1_POINT	100
#define PACKAGE2_POINT	210
#define PACKAGE3_POINT	460
#define PACKAGE4_POINT	1250

#ifdef KOREAN_TARGET
    #define PACKAGE1_PRODUCT_ID		@"com.animoca.MyCarSalon.carPointsPackage1_KR"
    #define PACKAGE2_PRODUCT_ID		@"com.animoca.MyCarSalon.carPointsPackage2_KR"
    #define PACKAGE3_PRODUCT_ID		@"com.animoca.MyCarSalon.carPointsPackage3KR"
    #define PACKAGE4_PRODUCT_ID		@"com.animoca.MyCarSalon.carPointsPackage4_KR"
#else
    #ifdef JAPANESE_TARGET
        #define PACKAGE1_PRODUCT_ID		@"com.tg.mycarsalonjp.100coins"
        #define PACKAGE2_PRODUCT_ID		@"com.tg.mycarsalonjp.215coins"
        #define PACKAGE3_PRODUCT_ID		@"com.tg.mycarsalonjp.460coins"
        #define PACKAGE4_PRODUCT_ID		@"com.tg.mycarsalonjp.1250coins"
    #else 
        #define PACKAGE1_PRODUCT_ID		@"com.animoca.MyCarSalon.carPointsPackage1"
        #define PACKAGE2_PRODUCT_ID		@"com.animoca.MyCarSalon.carPointsPackage2"
        #define PACKAGE3_PRODUCT_ID		@"com.animoca.MyCarSalon.carPointsPackage3"
        #define PACKAGE4_PRODUCT_ID		@"com.animoca.MyCarSalon.carPointsPackage4"
    #endif
#endif
*/
#import "InAppPurchaseSetting.h"
#import <Foundation/Foundation.h>


#define PACKAGE1_PRODUCT_ID [InAppPurchaseSetting getPackage1_ID]
#define PACKAGE2_PRODUCT_ID [InAppPurchaseSetting getPackage2_ID]
#define PACKAGE3_PRODUCT_ID [InAppPurchaseSetting getPackage3_ID]
#define PACKAGE4_PRODUCT_ID [InAppPurchaseSetting getPackage4_ID]
#define PACKAGE5_PRODUCT_ID [InAppPurchaseSetting getPackage5_ID]

#define PACKAGE1_POINT [InAppPurchaseSetting getPackage1_Point]
#define PACKAGE2_POINT [InAppPurchaseSetting getPackage2_Point]
#define PACKAGE3_POINT [InAppPurchaseSetting getPackage3_Point]
#define PACKAGE4_POINT [InAppPurchaseSetting getPackage4_Point]
#define PACKAGE5_POINT [InAppPurchaseSetting getPackage5_Point]


@interface InAppPurchaseSetting (Fruit)

@end
