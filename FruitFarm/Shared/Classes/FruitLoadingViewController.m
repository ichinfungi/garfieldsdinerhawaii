//
//  FruitLoadingViewController.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 25/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitLoadingViewController.h"
#import "PrettyManager.h"
#import "NSDictionaryExtend.h"
#import "Utilities.h"

@implementation FruitLoadingViewController
@synthesize iLogoImageView;

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// Reset
	[DCTextureManager releaseManager];
	
	for (int i=1; i<=10; i++) {
		[[DCTextureManager sharedManager] addFileToPreloadTextureQueue:[NSString stringWithFormat:@"cus%.2d.png", i]];
		[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:[NSString stringWithFormat:@"cus%.2d.plist", i]];
	}
    
    [[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"stf_arlene.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"stf_arlene.plist"];
    [[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"stf_doc_boy.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"stf_doc_boy.plist"];
    [[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"stf_garfield.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"stf_garfield.plist"];
    [[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"stf_jon.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"stf_jon.plist"];
    [[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"stf_liz.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"stf_liz.plist"];
    [[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"stf_nermal.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"stf_nermal.plist"];
    [[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"stf_odie.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"stf_odie.plist"];
    [[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"stf_robot.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"stf_robot.plist"];
	
	[[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"vfx006.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"vfx006.plist"];
	
	[[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"Emotion_Angry.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"Emotion_Angry.plist"];
	
	[[DCTextureManager sharedManager] addFileToPreloadTextureQueue:@"Emotion_Happy.png"];
	[[DCTextureManager sharedManager] addFileToPreloadFrameQueue:@"Emotion_Happy.plist"];
	/*
	int curStage = [[PrettyManager sharedManager] getCurrentStage];
	int curLevel = [[PrettyManager sharedManager] getCurrentLevel];
	NSString* bgName = [NSDictionary getData:[NSString stringWithFormat:@"DNADict/%d/level/%d/bg", curStage, curLevel] withContentsOfFile:[Utilities getPathForResource:@"StageDNA.plist"]];
	
	if (bgName != nil) {
		[[DCTextureManager sharedManager] addFileToPreloadTextureQueue:bgName];
		NSLog(@"preload bg %@", bgName);
	}
	*/
}

- (void)viewDidUnload {
	[self setILogoImageView:nil];
	[super viewDidUnload];
}

- (void)dealloc {
	[iLogoImageView release];
	[super dealloc];
}

- (void)updateBar:(float)percent {
	[super updateBar:percent];
	
	iLogoImageView.center = CGPointMake(iLoadingBarView.frame.origin.x + iLoadingBarView.frame.size.width, iLogoImageView.center.y);
}
@end
