//
//  FruitPrettyConsumableItemController.h
//  DCGameTemplate
//
//  Created by royl on 11/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrettyConsumableItemController.h"
#import "FruitStaffBoostBtn.h"
#import "FruitWildCardFacilityBtn.h"
#import "FruitSpawnBoostBtn.h"

@interface FruitPrettyConsumableItemController : PrettyConsumableItemController{
    FruitStaffBoostBtn *iStaffBoostBtn;
    FruitWildCardFacilityBtn *iWildCardBtn;
    FruitSpawnBoostBtn *iSpawnBoostBtn;
}

@property (nonatomic,retain) FruitStaffBoostBtn *iStaffBoostBtn;
@property (nonatomic,retain) FruitWildCardFacilityBtn *iWildCardBtn;
@property (nonatomic,retain) FruitSpawnBoostBtn *iSpawnBoostBtn;

-(void)addButtons;

@end
