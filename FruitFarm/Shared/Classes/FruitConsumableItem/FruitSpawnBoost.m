//
//  FruitSpawnBoost.m
//  DCGameTemplate
//
//  Created by royl on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitSpawnBoost.h"
#import "DCGraphicEngine.h"

@implementation FruitSpawnBoost

-(id)initWithStage:(PrettyStage *)pStage button:(PrettyConsumableItemButton *)btn{
    self = [super initWithStage:pStage button:btn];
    if(self != nil){
        _spawnTimeFactor = 0.5f;
    }
    return self;
}


-(void)playAnimation{
    CGPoint bgPosition = CGPointMake(240.0f, 100.0f);
    CGPoint charStartPosition = CGPointMake(screenWidth, 140.0f);
    CGPoint charEndPosition = CGPointMake(140.0f, 140.0f);
    CGPoint titleStartPosition = CGPointMake(0.0f, 100.0f);
    CGPoint titleEndPosition = CGPointMake(340.0f, 100.0f);
    
    if([Utilities isiPad]){
        bgPosition = CGPointMake(512.0f, 240.0f);
        charStartPosition = CGPointMake(screenWidth, 360.0f);
        charEndPosition = CGPointMake(300, 360);
        titleStartPosition = CGPointMake(0.0, 240.0f);
        titleEndPosition = CGPointMake(730.0f, 240.0f);
    }
    
    animationBG = [[DCAnimatedSprite alloc] initWithSpriteSheetFile:@"skill03_bg.plist" animationFile:@"skill03_bg_anim.plist"];
    [animationBG setPosition:bgPosition];
    [[DCGraphicEngine layer] addChild:animationBG z:10000001];
    [animationBG playAnimation:@"SERVE DOWN" repeatCount:0 fps:8];
    animationChar = [[DCSprite alloc] initWithFile:@"skill03_cha.png"];
    [animationChar setPosition:charStartPosition];
    [[DCGraphicEngine layer] addChild:animationChar z:10000002];
    [animationChar runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.2f position:charEndPosition],[CCDelayTime actionWithDuration:0.8f],[CCCallFunc actionWithTarget:self selector:@selector(endAnimation)],nil]];
    animationTitle = [[DCSprite alloc] initWithFile:@"skill03_title.png"];
    [animationTitle setPosition:titleStartPosition];
    [[DCGraphicEngine layer] addChild:animationTitle z:10000003];
    [animationTitle runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.2f position:titleEndPosition],[CCDelayTime actionWithDuration:0.8f], nil]];
}

-(void)endAnimation{
    [super endAnimation];
    if(animationTitle != nil){
        [animationTitle stopAllActions];
        [[DCGraphicEngine layer] removeChild:animationTitle cleanup:YES];
        [animationTitle release];
        animationTitle = nil;
    }
}

@end
