//
//  SpawnBoostBtn.h
//  DCGameTemplate
//
//  Created by royl on 11/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrettyStage.h"
#import "PrettyConsumableItemController.h"
#import "PrettyConsumableItemButton.h"
#import "SpawnBoostBtn.h"

@interface FruitSpawnBoostBtn : SpawnBoostBtn {
    UIImageView *mIconImage;
    UIImageView *mEffectImage;
    BOOL shouldCallAnimationUpdate;
}
-(void)blinkAnimation:(NSString*)animationId percentage:(float)percentage;
-(void)blinkAnimationDidFinished;
@end