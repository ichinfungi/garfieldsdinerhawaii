//
//  WildCardFacilityBtn.m
//  DCGameTemplate
//
//  Created by royl on 11/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitWildCardFacilityBtn.h"
#import "FruitStageViewController.h"
#import "GamePointManager.h"
#import "OperationItem.h"
#import "FruitWildCardFacility.h"
#import "NSDictionaryExtend.h"

@implementation FruitWildCardFacilityBtn

-(id)initWithButton:(UIButton *)btn stage:(PrettyStage *)pStage nameInDict:(NSString *)pNameInDict{
    self = [super initWithButton:btn stage:pStage nameInDict:pNameInDict];
    if(self != nil){
        mIconImage = ((FruitStageViewController*)self.stage.stageViewController).iWildCardIconImage;
        mEffectImage = ((FruitStageViewController*)self.stage.stageViewController).iWildCardEffectImage;
        shouldCallAnimationUpdate = YES;
    }
    return self;
}

-(BOOL)update{
    if (![super update]){
        return NO;
    }
    float percentage = [self.itemDelegate getRemainPercentage:self.stage.stageTime];
    if(shouldCallAnimationUpdate){
        [self blinkAnimation:@"WildCardBlinkingAnimation" percentage:percentage];
        
    }
    if([Utilities isiPad]){
        [mEffectImage setFrame:CGRectMake(805.0f,64.0f - (40.0f*percentage), 51.0,40.0f * percentage)];
    }
    else{
        [mEffectImage setFrame:CGRectMake(376.0f,31.0f - (19.0f*percentage), 25.0,19.0f * percentage)];
    }
    return YES;
}


-(void)setButtonEnable:(BOOL)enable{
    [super setButtonEnable:enable];
    shouldCallAnimationUpdate = YES;
    [mIconImage setAlpha:1.0f];
    if([Utilities isiPad]){
        [mEffectImage setFrame:CGRectMake(805.0f,24.0f , 51.0f ,40.0f)];
    }
    else{
        [mEffectImage setFrame:CGRectMake(376.0f,12.0f , 25.0f ,19.0f)];
    }
}

-(void)blinkAnimation:(NSString*)animationId percentage:(float)percentage{
    shouldCallAnimationUpdate = NO;
    [UIView beginAnimations:animationId context:mIconImage];
    [UIView setAnimationDuration:0.25f * percentage/4.0f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(blinkAnimationDidFinished)];
    if([mIconImage alpha] == 1.0f){
        [mIconImage setAlpha:0.0f];
    }
    else if([mIconImage alpha] == 0.0f){
        [mIconImage setAlpha:1.0f];
    }
    [UIView commitAnimations];
}

-(void)blinkAnimationDidFinished{
    shouldCallAnimationUpdate = YES;
}

-(PrettyConsumableItem*)createItemInstance{
    FruitWildCardFacility* item = [[[FruitWildCardFacility alloc]initWithStage:self.stage button:self]autorelease];
    return item;
}


@end
