//
//  WildCardFacilityBtn.h
//  DCGameTemplate
//
//  Created by royl on 11/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrettyStage.h"
#import "PrettyConsumableItemController.h"
#import "PrettyConsumableItemButton.h"
#import "WildCardFacilityBtn.h"

@interface FruitWildCardFacilityBtn : WildCardFacilityBtn {
    UIImageView *mIconImage;
    UIImageView *mEffectImage;
    BOOL shouldCallAnimationUpdate;
}
-(void)blinkAnimation:(NSString*)animationId percentage:(float)percentage;
-(void)blinkAnimationDidFinished;
@end
