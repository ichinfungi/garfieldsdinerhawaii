//
//  FruitStaffBoost.m
//  DCGameTemplate
//
//  Created by royl on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitStaffBoost.h"
#import "DCGraphicEngine.h"

@implementation FruitStaffBoost

-(void)playAnimation{
    CGPoint bgPosition = CGPointMake(240.0f, 100.0f);
    CGPoint charStartPosition = CGPointMake(0.0f, 140.0f);
    CGPoint charEndPosition = CGPointMake(330.0f, 140.0f);
    CGPoint titleStartPosition = CGPointMake(screenWidth, 100.0f);
    CGPoint titleEndPosition = CGPointMake(150.0f, 100.0f);
    
    if([Utilities isiPad]){
        bgPosition = CGPointMake(512.0f, 240.0f);
        charStartPosition = CGPointMake(0.0f, 360.0f);
        charEndPosition = CGPointMake(700, 360);
        titleStartPosition = CGPointMake(screenWidth, 240.0f);
        titleEndPosition = CGPointMake(360.0f, 240.0f);
    }
    
    animationBG = [[DCAnimatedSprite alloc] initWithSpriteSheetFile:@"skill01_bg.plist" animationFile:@"skill01_bg_anim.plist"];
    [animationBG setPosition:bgPosition];
    [[DCGraphicEngine layer] addChild:animationBG z:10000001];
    [animationBG playAnimation:@"SERVE DOWN" repeatCount:0 fps:8];
    animationChar = [[DCSprite alloc] initWithFile:@"skill01_cha.png"];
    [animationChar setPosition:charStartPosition];
    [[DCGraphicEngine layer] addChild:animationChar z:10000002];
    [animationChar runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.2f position:charEndPosition],[CCDelayTime actionWithDuration:0.8f],[CCCallFunc actionWithTarget:self selector:@selector(endAnimation)],nil]];
    animationTitle = [[DCSprite alloc] initWithFile:@"skill01_title.png"];
    [animationTitle setPosition:titleStartPosition];
    [[DCGraphicEngine layer] addChild:animationTitle z:10000003];
    [animationTitle runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.2f position:titleEndPosition],[CCDelayTime actionWithDuration:0.8f], nil]];
}

-(void)endAnimation{
    [super endAnimation];
    if(animationTitle != nil){
        [animationTitle stopAllActions];
        [[DCGraphicEngine layer] removeChild:animationTitle cleanup:YES];
        [animationTitle release];
        animationTitle = nil;
    }
}

-(void)endEffect{
    [super endEffect];
    if([Utilities isiPad]){
        for (int i = 0 ; i < [self.stage._StaffArray count]; i++) {
            PrettyStaff *tStaff = [self.stage._StaffArray objectAtIndex:i];
            tStaff.walkPPS = tStaff.walkPPS * 2;
        }
    }
}

@end
