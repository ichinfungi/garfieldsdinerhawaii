//
//  FruitPrettyConsumableItemController.m
//  DCGameTemplate
//
//  Created by royl on 11/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyConsumableItemController.h"
#import "FruitStageViewController.h"
#import "FruitPrettyStage.h"
#import "OperationItem.h"

@implementation FruitPrettyConsumableItemController
@synthesize iStaffBoostBtn;
@synthesize iSpawnBoostBtn;
@synthesize iWildCardBtn;

-(void)addButtons{
    FruitStageViewController *tStageViewController = (FruitStageViewController*)_stage.stageViewController;
    //iStaffBoostBtn = [[StaffBoostBtn alloc]initWithButton:tStageViewController.iStaffBoostButton ImageBtn:tStageViewController.iStaffBoostImageButton effect:tStageViewController.iStaffBoostEffectImage stage:_stage];
    if(iStaffBoostBtn == nil){
        iStaffBoostBtn = [[AutoClass(@"StaffBoostBtn") alloc]initWithButton:tStageViewController.iStaffBoostButton stage:_stage];
    }
    else{
        iStaffBoostBtn.button = tStageViewController.iStaffBoostButton;
    }
    
    if(iWildCardBtn == nil){
        iWildCardBtn = [[AutoClass(@"WildCardFacilityBtn") alloc] initWithButton:tStageViewController.iWildCardButton stage:_stage];
    }
    else{
        iWildCardBtn.button = tStageViewController.iWildCardButton;
    }
    
    if(iSpawnBoostBtn == nil){
        iSpawnBoostBtn = [[AutoClass(@"SpawnBoostBtn") alloc] initWithButton:tStageViewController.iSpawnBoostButton stage:_stage];
    }
    else{
        iSpawnBoostBtn.button = tStageViewController.iSpawnBoostButton;
    }
}

-(void)update:(float)stageTime{
    [super update:stageTime];
    [iStaffBoostBtn update];
    [iWildCardBtn update];
    [iSpawnBoostBtn update];
}

-(void)stopAndReomveAllEvent{
    [super stopAndReomveAllEvent];
}

-(void)dealloc{
    self.iSpawnBoostBtn = nil;
    self.iWildCardBtn = nil;
    self.iStaffBoostBtn = nil;
    [super dealloc];
}

@end
