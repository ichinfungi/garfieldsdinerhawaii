//
//  FruitLoadingViewController.h
//  DCGameTemplate
//
//  Created by Dream Cortex on 25/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LoadingViewController.h"


@interface FruitLoadingViewController : LoadingViewController {
    
	UIImageView *iLogoImageView;
}
@property (nonatomic, retain) IBOutlet UIImageView *iLogoImageView;

@end
