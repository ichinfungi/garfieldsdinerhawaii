//
//  FruitPrettyManager.h
//  DCGameTemplate
//
//  Created by Chan Calvin on 19/10/2011.
//  Copyright 2011 Dream Cortex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrettyManager.h"

@interface FruitPrettyManager : PrettyManager {
    
}

#pragma mark -
#pragma mark profile
+ (NSString*)getProfileVersion;

#pragma mark -
#pragma mark Game Point

+ (NSString*)getGamePointSingularName;
+ (NSString*)getGamePointPluralName;

+ (float)getPerfectDayBonusPercentage;
+ (float)getPerfectFacilityUserBonusPercentage;

#pragma mark -
#pragma mark Rating
+ (NSMutableDictionary*)getManagerDict;
+ (int)getFirstRatingGameDay;
+ (void)setFirstRatingGameDay:(int)day;

@end
