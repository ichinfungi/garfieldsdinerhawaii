//
//  FruitProfileViewController.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 22/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitProfileViewController.h"
#import "FruitGameSetting.h"


@implementation FruitProfileViewController

- (IBAction)deleteOnClick {
	[[OALSimpleAudio sharedInstance] playEffect:@"alert.wav"];
	[self showDeleteView];
}

- (IBAction)deleteConfirmOnClick:(id)sender {
	[[OALSimpleAudio sharedInstance] playEffect:@"confirm.wav"];
	[super deleteConfirmOnClick:sender];
}

- (int)profileNameLengthLimit {
	return PROFILE_NAME_LENGTH;
}

- (void)endRename {
	[super endRename];
}

@end
