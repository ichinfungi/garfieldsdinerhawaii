//
//  FruitMapViewController.m
//  DCGameTemplate
//
//  Created by Dream Cortex on 28/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitMapViewController.h"
#import "DCProfileManager.h"
#import "Utilities.h"
#import "NSDictionaryExtend.h"
#import "PrettyManager.h"
#import "PrettyStage.h"

@implementation FruitMapViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	
	[[OALSimpleAudio sharedInstance] playBg:@"menu.mp3" loop:YES];
	
	[self showBanner];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[self showBanner];
}

- (void)loadMap {
	UIButton *tButton;
	
	mapScrollView.contentSize = CGSizeMake(mapBG.image.size.width * 1, mapBG.image.size.height * 1);
	mapScrollView.bounces = NO;
	
	NSDictionary *mapDict = [[PrettyManager sharedManager] getDNADict:STAGE_DNA_FILE];
	
	NSDictionary *stagesDict = [mapDict objectForKey:@"DNADict"];
	for( id key in stagesDict ) {
		int dnaID = [key intValue];
		BOOL enable = [[PrettyManager sharedManager] isStageUnlocked:dnaID];
		
		// Image Button
		tButton = [UIButton buttonWithType:UIButtonTypeCustom];
		
		NSDictionary *stageDict = [stagesDict objectForKey:key];
		[tButton setImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Lock.png")] forState:UIControlStateDisabled];
		[tButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[[stageDict objectForKey:@"img"] stringByDeletingPathExtension] ofType:[[stageDict objectForKey:@"img"] pathExtension]]] forState:UIControlStateNormal];
		tButton.tag = dnaID;
		tButton.frame = CGRectMake(0, 0, tButton.currentBackgroundImage.size.width, tButton.currentBackgroundImage.size.height);
		CGRect tFrame = tButton.frame;
        if([Utilities isiPad]){
            tFrame.origin.x = roundf([[stageDict objectForKey:@"x"] intValue] * 1024.0f/480.0f);
            tFrame.origin.y = roundf([[stageDict objectForKey:@"y"] intValue] * 768.0f/320.0f);
        }
        else{
            tFrame.origin.x = [[stageDict objectForKey:@"x"] intValue];
            tFrame.origin.y = [[stageDict objectForKey:@"y"] intValue];
        }
		tButton.frame = tFrame;
		tButton.enabled = enable;
		
		[tButton addTarget:self action:@selector(stageOnClick:) forControlEvents:UIControlEventTouchUpInside] ;
		
		[mapScrollView addSubview:tButton];
		
		// Label Button
		CGPoint center = tButton.center;
		center.x = (int)center.x;
		center.y = (int)(center.y + tButton.frame.size.height*0.5f);
		
		NSString *stageName = [stageDict objectForKey:@"name"];
		
		if (stageName != nil) {
			tButton = [UIButton buttonWithType:UIButtonTypeCustom];
			[tButton setTitle:stageName forState:UIControlStateNormal];
            if([Utilities isiPad]){
                tButton.titleLabel.font = [UIFont fontWithName:@"Verdana" size:28];
                tButton.contentEdgeInsets = UIEdgeInsetsMake(9, 38, 0, 0);
            }
            else{
                tButton.titleLabel.font = [UIFont fontWithName:@"Verdana" size:14];
                tButton.contentEdgeInsets = UIEdgeInsetsMake(3, 14, 0, 0);
            }
			tButton.titleLabel.textAlignment = NSTextAlignmentCenter;
			[tButton setBackgroundImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Map_Btn.png")] forState:UIControlStateNormal];
			tButton.tag = dnaID;
			tButton.frame = CGRectMake(0, 0, tButton.currentBackgroundImage.size.width, tButton.currentBackgroundImage.size.height);
			tButton.center = center;
			tButton.enabled = enable;
			
			[tButton addTarget:self action:@selector(stageOnClick:) forControlEvents:UIControlEventTouchUpInside] ;
			
			[mapScrollView addSubview:tButton];
		}
	} 
}

- (IBAction)stageOnClick:(id)sender {
	[self playClickSound];
	
	DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
	if (profile != nil) {
		[profile.dict setObject:[NSNumber numberWithInteger:((UIButton*)sender).tag]  forKey:@"currentStageDNAID"];
        [[DCProfileManager sharedManager] saveAllProfiles];
    }
	
	
	[self showStageView];
}

- (void)showStageView {
	if (self.rootViewController != nil) {
		[self.rootViewController setNextProgramState:PROGRAM_LOADING_STATE];
	}
}

@end
