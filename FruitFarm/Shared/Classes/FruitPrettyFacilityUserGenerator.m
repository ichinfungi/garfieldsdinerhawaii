//
//  FruitFacilityUserGenerator.m
//  DCGameTemplate
//
//  Created by royl on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyFacilityUserGenerator.h"
#import "DCProfile.h"
#import "DCProfileManager.h"
#import "NSDictionaryExtend.h"
#import "FruitPrettyStage.h"


@implementation FruitPrettyFacilityUserGenerator

- (BOOL)canGenerateFacilityUser:(float)stageTime CurrentScore:(float)CurrentScore CurrentUserCount:(int)CurrentUserCount isBlocked:(BOOL)isBlocked
{
    if(isBlocked){
        return NO;
    }
    if(self.maxFacilityUserCount>0 && CurrentUserCount >= self.maxFacilityUserCount){
        return NO;
    }
    
    BOOL result = NO;
    float resultSpawnInterval = self.spawnInterval - ((arc4random() % 100)/self.affectByScore);
   
    if(resultSpawnInterval < self.minSpawnInterval){
        result = ((stageTime - self.spawnTimer) > self.minSpawnInterval);
    }
    else{
        result = ((stageTime - self.spawnTimer) > resultSpawnInterval);
    }
    
    if(result){
        NSLog(@"stageTime = %.1f, spawnTime = %.1f, resultSpawnInterval = %.1f",stageTime,self.spawnTimer, resultSpawnInterval);
    }
    
    return result;
}

- (int)generateFacilityUser:(float)stageTime{
    int vipShowCount = 0;
    DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
    NSMutableDictionary *VIPProfile = [profile.dict getData:@"vipData"];
    if(VIPProfile == nil){
        VIPProfile = [NSMutableDictionary dictionary];
    }
    if([VIPProfile getData:VIP_SHOWN_KEY] != nil){
        vipShowCount = [[VIPProfile getData:VIP_SHOWN_KEY]intValue];
    }
    NSLog(@"VIP Count: %d",vipShowCount);
    if(((arc4random() % 100) < 5) && vipShowCount < 1){
        vipShowCount++;
        [VIPProfile setObject:[NSNumber numberWithInt:vipShowCount] forKey:VIP_SHOWN_KEY];
        [profile.dict setObject:VIPProfile forKey:@"vipData"];
        [[DCProfileManager sharedManager] saveAllProfiles];
        return [self generateVIPFacilityUser:stageTime];
    }
    else{
        return [super generateFacilityUser:stageTime];
    }
}

@end
