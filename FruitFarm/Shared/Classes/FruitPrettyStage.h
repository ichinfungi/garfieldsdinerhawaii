//
//  FruitPrettyStage.h
//  DCGameTemplate
//
//  Created by Calvin on 24/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrettyStage.h"
#import "PrettyFacilityUserGenerator.h"

#define BG_CAR_BASE_TIME 2
#define BG_CAR_OFFSET_TIME 5
#define TotalCustomersKey @"TotalCustomersServedKey"
#define TotalMoneyKey @"TotalMoneyEarnedKey"
#define TotalPurchaseEquipmentKey @"TotalPurchaseEquimentKey"
#define VIP_SHOWN_KEY @"VIP_SHOWN_KEY"
#define GET_V1_4_0_RED_POCKET_KEY @"v1.4.0_chineseNewYearRedPocketKey"

@interface FruitPrettyStage : PrettyStage {

	NSMutableArray *_CustomerArray;
	NSMutableArray *_CustomerGeneratorArray;
    NSMutableArray *_BackgroundCarArray;
    NSMutableArray *_WaitingOutdoorArray;
    float backgroundCarSwanpTimer;
    PathMap *staff_map;
    BOOL UsedMusicPlayer;
    UIView *VIPAnimationView;
    BOOL shouldDoorOpen;
}

@property (nonatomic, retain) NSMutableArray *_CustomerArray;
@property (nonatomic, retain) NSMutableArray *_CustomerGeneratorArray;
@property (nonatomic, retain) NSMutableArray *_WaitingOutdoorArray;

- (int)getIDFromPositionForVersion_1_1_0:(CGPoint)pPosition atStage:(int)pStage;
- (int)addMoneyWithoutAnimate:(int)pMoney;
-(void)hideAllUpgradeBubble;
-(void)showAllFacilityUpgradeBubble;
-(void)showAllStaffUpgradeBubble;
@end
