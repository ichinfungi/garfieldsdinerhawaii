//
//  FruitPrettyFacility.m
//  DCGameTemplate
//
//  Created by Calvin on 22/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyFacility.h"
#import "PrettyManager.h"
#import "NSDictionaryExtend.h"
#import "CCActionEase.h"
#import "AchievementSystem.h"
#import "GameUnit.h"
#import "GameSetting.h"
#import "CCLabelTTF.h"

@implementation FruitPrettyFacility
@synthesize waitingUser;

// override for infinity level upgrade
- (id)init:(int)pDNAID ID:(int)pID level:(int)pLevel position:(CGPoint)pPosition
{
	self = [super init:pDNAID ID:pID level:pLevel position:pPosition];
	if (self != nil)
	{		
		NSDictionary *dnaDict = [[[PrettyManager sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", _dnaID]];
		
            if(self.ID == 16 || self.ID == 15 || self.ID == 20 || self.ID == 22){
                self.facilityUserPosOffset = CGPointMake([[dnaDict getData:@"offsets/facilityUserX"] floatValue] * -1, [[dnaDict getData:@"offsets/facilityUserY"] floatValue]);
            }
            else{
                self.facilityUserPosOffset = CGPointMake([[dnaDict getData:@"offsets/facilityUserX"] floatValue], [[dnaDict getData:@"offsets/facilityUserY"] floatValue]);
            }

        
        if([specialFacilityType isEqualToString:@"MusicPlayer"]){
            NSLog(@"Music player count %d",self.maxUserCount);
            CCLabelTTF *MusicPlayerCount = nil;
            if([Utilities isiPad]){
                MusicPlayerCount = [[[CCLabelTTF alloc] initWithString:[NSString stringWithFormat:@"%.2d",self.maxUserCount] fontName:@"Verdana" fontSize:20]autorelease];
                [MusicPlayerCount setPosition:CGPointMake(45, 75)];
            }
            else{
                MusicPlayerCount = [[[CCLabelTTF alloc] initWithString:[NSString stringWithFormat:@"%.2d",self.maxUserCount] fontName:@"Verdana" fontSize:10]autorelease];
                [MusicPlayerCount setPosition:CGPointMake(20, 36)];
            }
            [MusicPlayerCount setColor:ccWHITE];
            
            [self.sprite addChild:MusicPlayerCount z:1 tag:1];
        }
        
        if([specialFacilityType isEqualToString:@"Blocker"]){
            NSLog(@"Blocker count %d",self.maxUserCount);
            CCLabelTTF *blockerCount = [[[CCLabelTTF alloc] initWithString:[NSString stringWithFormat:@"%.2d", self.maxUserCount] fontName:@"Verdana" fontSize:10]autorelease];
            [blockerCount setPosition:CGPointMake(25, 27)];
            [blockerCount setColor:ccWHITE];
            [self.sprite addChild:blockerCount z:1 tag:1];
        }
		
		if ([dnaDict getData:[NSString stringWithFormat:@"level/%d", _level+1]] != nil) {
			self.upgradeMoney = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/upgrade/money", (_level+1)]] floatValue];
			self.upgradeGamePoint = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/upgrade/gamePoint", (_level+1)]] floatValue];
			self.upgradePic = [dnaDict getData:[NSString stringWithFormat:@"level/%d/upgradePic", (_level+1)]];
			self.upgradeDesc = [dnaDict getData:[NSString stringWithFormat:@"level/%d/upgrade/desc", (_level+1)]];			
		}
        else{
            //make it don't show upgrade bubble
            self.upgradeMoney = -1;
            self.upgradeGamePoint = -1;
        }
        
        if([Utilities isiPad]){
            self.facilityUserPosOffset = CGPointMake(facilityUserPosOffset.x * 1024.0/480.0, facilityUserPosOffset.y * 768.0/320.0);
            self.snapDistance = self.snapDistance * 2;
            self.staffPosOffset = CGPointMake(self.staffPosOffset.x * 1024.0/480.0, self.staffPosOffset.y * 768.0/320.0);
        }
    }
	return self;
}

// override for update Count Label on music player
- (void)showFacilityUpgrade
{
    [super showFacilityUpgrade];
    if ([self.specialFacilityType isEqualToString:@"MusicPlayer"] && [self.sprite getChildByTag:1]!=nil){
        CCLabelTTF *MusicPlayerCount = (CCLabelTTF*)[self.sprite getChildByTag:1];
        [MusicPlayerCount setString:[NSString stringWithFormat:@"%.2d",self.maxUserCount]];
    }
    if([self.specialFacilityType isEqualToString:@"Blocker"]){
        if([self.sprite getChildByTag:1] != nil){
            CCLabelTTF *blockerCount = (CCLabelTTF*)[self.sprite getChildByTag:1];
            [blockerCount setString:[NSString stringWithFormat:@"%.2d",self.maxUserCount]];
        }
    }
}

///*
// game loop
- (void)update:(float)stageTime
{
	[super update:stageTime];
	if([facilityUserArray count] >0){
		PrettyFacilityUser* user = [facilityUserArray objectAtIndex:0];
		if(user != nil){
			[self.sprite setUserInteractionEnabled:(!(user.facilityUserState == FACILITY_USER_WAIT_FOR_NEXT_FACILITY || user.facilityUserState == FACILITY_USER_WAIT_FOR_NEXT_FACILITY_HURRY))];
		}
	}
	// won't die at counter
    /*if (_dnaID == 2) {
        PrettyFacilityUser* facilityUser;
        for (int i=0;i<[facilityUserArray count];i++) {
            facilityUser = (PrettyFacilityUser*)[facilityUserArray objectAtIndex:i];
            facilityUser.stateTimer = stageTime;
        }
        
    }*/
}

// override to let music player not became available
- (void)checkAndSetAvailable:(float)stageTime
{
	if ([facilityUserArray count] == 0 && self.sprite.numberOfRunningActions == 0 && self.dnaID != 8) {

		facilityState = FACILITY_AVAILABLE;
	}
}

- (void)startServingFacilityUser:(float)pTime{
    
    if([facilityUserArray count] > 0){
        PrettyFacilityUser *facilityUser = [facilityUserArray objectAtIndex:0];
    
        if ([facilityUser isCharacter]) {
            NSDictionary *dnaDict = [[[PrettyManagerClass sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", _dnaID]];
            if ([dnaDict getData:@"characterBehaviour"] != nil) {
                if ([dnaDict getData:@"characterBehaviour/facilityUserServicingAction"] != nil && [dnaDict getData:@"characterBehaviour/facilityUserServicingDirection"] != nil) {
                    if(self.ID == 16 || self.ID == 15 || self.ID == 20 || self.ID == 22){
                        [facilityUser setAction:[dnaDict getData:@"characterBehaviour/facilityUserServicingAction"] withDirection:@"LEFT"];
                    }
                    else{
                        [facilityUser setAction:[dnaDict getData:@"characterBehaviour/facilityUserServicingAction"] withDirection:[dnaDict getData:@"characterBehaviour/facilityUserServicingDirection"]];
                    }
                }
            }
        }
    }
    [super startServingFacilityUser:pTime];
}

-(void)finishServingFacilityUser{
    PrettyFacilityUser *facilityUser = nil;
    
    if([facilityUserArray count] > 0){
        facilityUser = [facilityUserArray objectAtIndex:0];
    }
    
    if (facilityUser != nil && [facilityUser isCharacter]) {
		NSDictionary *dnaDict = [[[PrettyManagerClass sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", _dnaID]];
		if ([dnaDict getData:@"characterBehaviour"] != nil) {
			if ([dnaDict getData:@"characterBehaviour/facilityUserAction"] != nil && [dnaDict getData:@"characterBehaviour/facilityUserDirection"] != nil) {
                if(self.ID == 16 || self.ID == 15 || self.ID == 20 || self.ID == 22){
                    [facilityUser setAction:[dnaDict getData:@"characterBehaviour/facilityUserAction"] withDirection:@"LEFT"];
                }
                else{
                    [facilityUser setAction:[dnaDict getData:@"characterBehaviour/facilityUserAction"] withDirection:[dnaDict getData:@"characterBehaviour/facilityUserDirection"]];
                }
			}
		}
	}
    [super finishServingFacilityUser];
}

-(void)facilityUserEntered:(PrettyFacilityUser *)pPrettyFacilityUser stageTime:(float)stageTime{
    [super facilityUserEntered:pPrettyFacilityUser stageTime:stageTime];
    NSDictionary *dnaDict = [[[PrettyManagerClass sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", _dnaID]];
    if ([dnaDict getData:@"characterBehaviour"] != nil) {
        if ([dnaDict getData:@"characterBehaviour/facilityUserAction"] != nil && [dnaDict getData:@"characterBehaviour/facilityUserDirection"] != nil) {
            if(self.ID == 16 || self.ID == 15 || self.ID == 20 || self.ID == 22){
                [pPrettyFacilityUser setAction:[dnaDict getData:@"characterBehaviour/facilityUserAction"] withDirection:@"LEFT"];
            }
            else{
                [pPrettyFacilityUser setAction:[dnaDict getData:@"characterBehaviour/facilityUserAction"] withDirection:[dnaDict getData:@"characterBehaviour/facilityUserDirection"]];
            }
        }
    }
    self.waitingUser++;
}


@end
