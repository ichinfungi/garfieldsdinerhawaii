//
//  FruitAchievementTableViewController.m
//  DCGameTemplate
//
//  Created by royl on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitAchievementTableViewController.h"
#import "AchievementSystem.h"
#import "GameSetting.h"

@implementation FruitAchievementTableViewController

typedef enum {
	PIC_TAG = 1,
	TITLE_TAG,
	DESC_TAG,
	BG_TAG,
	PIC_TAG2,
	TITLE_TAG2,
	DESC_TAG2,
	BG_TAG2
	
} CELL_TAG;

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 86;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 377, 78)];
		bgImageView.center = CGPointMake(self.tableView.frame.size.width*0.5f, 43);
		bgImageView.tag = BG_TAG;
		[cell.contentView addSubview:bgImageView];
		[bgImageView release];
		
		UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 56, 60)];
		imageView.center = CGPointMake(imageView.frame.size.width*0.5f + 12, bgImageView.frame.size.height*0.5f-5);
		imageView.tag = PIC_TAG;
		[bgImageView addSubview:imageView];
		[imageView release];
        
		UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 290, 28)];
		titleLabel.tag = TITLE_TAG;
		titleLabel.adjustsFontSizeToFitWidth = YES;
		titleLabel.minimumScaleFactor = 10;
		titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
		titleLabel.textColor = [UIColor colorWithRed:87.0f/255.0f green:66.0f/255.0f blue:36.0f/255.0f alpha:1.0f];
		titleLabel.backgroundColor = [UIColor clearColor];
		[bgImageView addSubview:titleLabel];
		[titleLabel release];
		
		UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 28, 290, 36)];
		descLabel.tag = DESC_TAG;
		descLabel.adjustsFontSizeToFitWidth = YES;
		descLabel.minimumScaleFactor = 10;
		descLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
		descLabel.textColor = [UIColor colorWithRed:87.0f/255.0f green:66.0f/255.0f blue:36.0f/255.0f alpha:1.0f];
		descLabel.backgroundColor = [UIColor clearColor];
		descLabel.numberOfLines = 2;
		[bgImageView addSubview:descLabel];
		[descLabel release];
        
        if ([Utilities isiPad]) {
			//Add second column
			UIImageView *bgImageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 377, 78)];
			bgImageView2.center = CGPointMake(self.tableView.frame.size.width*0.5f, 43);
			bgImageView2.tag = BG_TAG2;
			[cell.contentView addSubview:bgImageView2];
			[bgImageView2 release];
			
			UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 56, 60)];
			imageView2.center = CGPointMake(imageView2.frame.size.width*0.5f + 12, bgImageView2.frame.size.height*0.5f-5);
			imageView2.tag = PIC_TAG2;
			[bgImageView2 addSubview:imageView2];
			[imageView2 release];
			
			UILabel *titleLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 290, 28)];
			titleLabel2.tag = TITLE_TAG2;
			titleLabel2.adjustsFontSizeToFitWidth = YES;
			titleLabel2.minimumScaleFactor = 10;
			titleLabel2.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
			titleLabel2.textColor = [UIColor colorWithRed:87.0f/255.0f green:66.0f/255.0f blue:36.0f/255.0f alpha:1.0f];
			titleLabel2.backgroundColor = [UIColor clearColor];
			[bgImageView2 addSubview:titleLabel2];
			[titleLabel2 release];
			
			UILabel *descLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(80, 28, 290, 36)];
			descLabel2.tag = DESC_TAG2;
			descLabel2.adjustsFontSizeToFitWidth = YES;
			descLabel2.minimumScaleFactor = 10;
			descLabel2.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
			descLabel2.textColor = [UIColor colorWithRed:87.0f/255.0f green:66.0f/255.0f blue:36.0f/255.0f alpha:1.0f];
			descLabel2.backgroundColor = [UIColor clearColor];
			descLabel2.numberOfLines = 2;
			[bgImageView2 addSubview:descLabel2];
			[descLabel2 release];
			
			//move both items to new positions for iPad
			bgImageView.center = CGPointMake(self.tableView.frame.size.width*0.25f, 40);
			bgImageView2.center = CGPointMake(self.tableView.frame.size.width*0.75f, 40);
		}
    }
    
    // Configure the cell...
	UIImageView *imageView = (UIImageView *)[cell viewWithTag:PIC_TAG];
	UILabel *titleLabel = (UILabel *)[cell viewWithTag:TITLE_TAG];
	UILabel *descLabel = (UILabel *)[cell viewWithTag:DESC_TAG];
	UIImageView *bgImage = (UIImageView *)[cell viewWithTag:BG_TAG];
	
	Achievement *achievement = [achievementsArray objectAtIndex:indexPath.row];
    Achievement *achievement2 = nil;
	if ([Utilities isiPad]) {
		achievement = [achievementsArray objectAtIndex:indexPath.row*2];
		if (indexPath.row*2+1 < [achievementsArray count]) {
			achievement2 = [achievementsArray objectAtIndex:indexPath.row*2+1];
		} else {
			achievement2 = nil;
		}
	} else {
		achievement2 = nil;
	}
    
	if (achievement != nil) {
        //[[AchievementSystem sharedManager] updateAchievementWithID:achievement.achId percent:COMPLETE_PERCENT];
		if (achievement.percent >= COMPLETE_PERCENT && achievement.picName != nil) {
			imageView.image = [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(achievement.picName)];
            [imageView setHidden:NO];
			bgImage.image = [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Achievements_Lock.png")];
			descLabel.textColor = [UIColor colorWithRed:(87.0f/255.0f) green:(66.0f/255.0f) blue:(36.0f/255.0f) alpha:255.0f];
		}
		else {
			//imageView.image = [UIImage imageNamed:@"Achievements_Locked.png"];
            [imageView setHidden:YES];
			bgImage.image = [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Achievements_Lock.png")];
            descLabel.textColor = [UIColor colorWithRed:(87.0f/255.0f) green:(66.0f/255.0f) blue:(36.0f/255.0f) alpha:255.0f];
		}
		
		titleLabel.text = achievement.title;
		descLabel.text = achievement.desc;
	}
	else {
		imageView.image = nil;
		titleLabel.text = @"";
		descLabel.text = @"";
	}
    
    UIImageView *imageView2 = (UIImageView *)[cell viewWithTag:PIC_TAG2];
	UILabel *titleLabel2 = (UILabel *)[cell viewWithTag:TITLE_TAG2];
	UILabel *descLabel2 = (UILabel *)[cell viewWithTag:DESC_TAG2];
	UIImageView *bgImage2 = (UIImageView *)[cell viewWithTag:BG_TAG2];
	if (achievement2 != nil) {
		//[[AchievementSystem sharedManager] updateAchievementWithID:achievement2.achId percent:COMPLETE_PERCENT];
		if (achievement2.percent >= COMPLETE_PERCENT && achievement2.picName != nil) {
			imageView2.image = [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(achievement2.picName)];
            [imageView2 setHidden:NO];
			bgImage2.image = [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Achievements_Lock.png")];
			descLabel2.textColor = [UIColor colorWithRed:(87.0f/255.0f) green:(66.0f/255.0f) blue:(36.0f/255.0f) alpha:255.0f];
		}
		else {
			//imageView2.image = [UIImage imageNamed:@"Achievements_Locked.png"];
            [imageView2 setHidden:YES];
			bgImage2.image = [UIImage imageNamed:appendUniversalDeviceSuffixToFileName(@"Achievements_Lock.png")];
			descLabel2.textColor = [UIColor colorWithRed:(87.0f/255.0f) green:(66.0f/255.0f) blue:(36.0f/255.0f) alpha:255.0f];
		} 
		titleLabel2.text = achievement2.title;
		descLabel2.text = achievement2.desc;
	}
	else {
		imageView2.image = nil;
		bgImage2.image = nil;
		titleLabel2.text = @"";
		descLabel2.text = @"";
		
	}
	
    return cell;
}

@end
