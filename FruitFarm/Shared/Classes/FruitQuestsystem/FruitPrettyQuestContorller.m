//
//  FruitPrettyQuestController.m
//  DCGameTemplate
//
//  Created by royl on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyQuestContorller.h"
#import "FruitGameCenter.h"
#import "DCProfileManager.h"
#import "NSDictionaryExtend.h"

@implementation FruitPrettyQuestContorller

-(void)logChallengeResult:(BOOL)complete quest:(QuestItem *)q{
    [super logChallengeResult:complete quest:q];
    DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
    NSDictionary *iDict = [sysProfile.dict getData:DATA_CHALLENGE_DICT_KEY];
    int questCompleted = [[iDict getData:DATA_CHALLENGE_COMPLETED] intValue];
    [[GameCenter sharedManager] reportScore:questCompleted forCategory:lb_questcompleted];
}

@end
