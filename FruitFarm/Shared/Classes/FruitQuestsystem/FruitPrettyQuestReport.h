//
//  FruitPrettyQuestReport.h
//  DCGameTemplate
//
//  Created by royl on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrettyQuestReport.h"

@interface FruitPrettyQuestReport : PrettyQuestReport{
    UIImageView *_req3Icon;
    UIImageView *_req4Icon;
    UIImageView *_req5Icon;
    UIImageView *_req6Icon;
    UILabel *_req3Label;
    UILabel *_req4Label;
    UILabel *_req5Label;
    UILabel *_req6Label;
    UIImageView *_reward3Container;
    UILabel *_reward3Quantity;
    UIImageView *_reward3Icon;
}
@property (nonatomic, retain) IBOutlet UIButton *iQuitGameButton;
@property (nonatomic, retain) IBOutlet UIView *questReq1Box;
@property (nonatomic, retain) IBOutlet UIView *questReq2Box;
@property (nonatomic, retain) IBOutlet UIView *questReq3Box;
@property (nonatomic, retain) IBOutlet UIView *questReq4Box;
@property (nonatomic, retain) IBOutlet UIView *questReq5Box;
@property (nonatomic, retain) IBOutlet UIView *questReq6Box;
@property (nonatomic, retain) IBOutlet UIImageView *resultMark;
@property (nonatomic, retain) IBOutlet UIImageView *req3Icon;
@property (nonatomic, retain) IBOutlet UIImageView *req4Icon;
@property (nonatomic, retain) IBOutlet UIImageView *req5Icon;
@property (nonatomic, retain) IBOutlet UIImageView *req6Icon;
@property (nonatomic, retain) IBOutlet UILabel *req3Label;
@property (nonatomic, retain) IBOutlet UILabel *req4Label;
@property (nonatomic, retain) IBOutlet UILabel *req5Label;
@property (nonatomic, retain) IBOutlet UILabel *req6Label;
@property (nonatomic, retain) IBOutlet UIImageView *reward3Container;
@property (nonatomic, retain) IBOutlet UILabel *reward3Quantity;
@property (nonatomic, retain) IBOutlet UIImageView *reward3Icon;
-(IBAction)quitGameButtonOnClick;
-(IBAction)dayEndOnClick;
@end
