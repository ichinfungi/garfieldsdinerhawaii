//
//  FruitPrettyQuestReport.m
//  DCGameTemplate
//
//  Created by royl on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyQuestReport.h"
#import "PrettyStage.h"
#import "PrettyStageViewController.h"
#import "QuestItem.h"
#import "PrettyCardCollectionManager.h"
#import "AnimViewManager.h"
#import "PrettySoundEventManager.h"
#import "Localization.h"
#import "GameUnit.h"
#import "DCProfileManager.h"
#import "NSDictionaryExtend.h"

@implementation FruitPrettyQuestReport
@synthesize iQuitGameButton;
@synthesize questReq2Box;
@synthesize questReq1Box;
@synthesize questReq3Box;
@synthesize questReq4Box;
@synthesize questReq5Box;
@synthesize questReq6Box;
@synthesize resultMark;
@synthesize req3Icon = _req3Icon;
@synthesize req4Icon = _req4Icon;
@synthesize req5Icon = _req5Icon;
@synthesize req6Icon = _req6Icon;
@synthesize req3Label = _req3Label;
@synthesize req4Label = _req4Label;
@synthesize req5Label = _req5Label;
@synthesize req6Label = _req6Label;
@synthesize reward3Container = _reward3Container;
@synthesize reward3Icon = _reward3Icon;
@synthesize reward3Quantity = _reward3Quantity;

-(IBAction)quitGameButtonOnClick{
    [_stageViewController.stage quitGame];
}

-(void)debugRankComplete:(id)sender{
    NSString *rank = nil;
    NSInteger tag = ((UIButton*)sender).tag;
    switch (tag) {
        case 1001:
            rank = @"s";
            break;
        case 1002:
            rank = @"a";
            break;
        case 1003:
            rank = @"b";
            break;
        case 1004:
            rank = @"c";
            break;
            
        default:
            break;
    }
    
    DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
    NSMutableDictionary *iDict = nil;
    if([sysProfile.dict getData:DATA_CHALLENGE_DICT_KEY]==nil){
        iDict = [[[NSMutableDictionary alloc] init] autorelease];
    }
    else{
        iDict = [sysProfile.dict getData:DATA_CHALLENGE_DICT_KEY];
    }
    int rank_comp = [[iDict objectForKey:[NSString stringWithFormat:@"%@_%@",DATA_CHALLENGE_RANK_COMPLETED,rank] defaultValue:[NSNumber numberWithInt:0]] intValue];
    rank_comp++;
    
    int quest_comp = [[iDict objectForKey:[NSString stringWithFormat:DATA_CHALLENGE_COMPLETED] defaultValue:[NSNumber numberWithInt:0]] intValue];
    quest_comp++;
    [iDict setObject:[NSNumber numberWithInt:quest_comp] forKey:DATA_CHALLENGE_COMPLETED];
    [iDict setObject:[NSNumber numberWithInt:rank_comp] forKey:[NSString stringWithFormat:@"%@_%@",DATA_CHALLENGE_RANK_COMPLETED,rank]];
    [sender setTitle:[NSString stringWithFormat:@"%@ ++(%d)",rank,rank_comp] forState:UIControlStateNormal];
    [[DCProfileManager sharedManager] saveAllProfiles];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    QuestConditionState *condition3 = nil;
    QuestConditionState *condition4 = nil;
    QuestConditionState *condition5 = nil;
    QuestConditionState *condition6 = nil;
    if([quest.currentState count] >=3){
        condition3 = [quest.currentState objectAtIndex:2];
    }
    if([quest.currentState count] >=4){
        condition4 = [quest.currentState objectAtIndex:3];
    }
    if([quest.currentState count] >=5){
        condition5 = [quest.currentState objectAtIndex:4];
    }
    if([quest.currentState count] >=6){
        condition6 = [quest.currentState objectAtIndex:5];
    }
    [self setReqContent:condition3 icon:_req3Icon label:_req3Label];
    [self setReqContent:condition4 icon:_req4Icon label:_req4Label];
    [self setReqContent:condition5 icon:_req5Icon label:_req5Label];
    [self setReqContent:condition6 icon:_req6Icon label:_req6Label];
     
    [resultMark setHidden:YES];
    [self.questReq2Box setHidden:!([quest.currentState count] >= 2)];
    [self.questReq3Box setHidden:!([quest.currentState count] >= 3)];
    [self.questReq4Box setHidden:!([quest.currentState count] >= 4)];
    [self.questReq5Box setHidden:!([quest.currentState count] >= 5)];
    [self.questReq6Box setHidden:!([quest.currentState count] >= 6)];
    
    NSString *rewardstr = quest.rewardcode;
    NSArray *rewardList = [rewardstr componentsSeparatedByString:@","];
    [self setRewardBoxInfo:2 rewardList:rewardList rewardIcon:_reward3Icon rewardLabel:_reward3Quantity container:_reward3Container];
    if(DEBUG_SKIP){
        
        DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
        NSMutableDictionary *iDict = nil;
        if([sysProfile.dict getData:DATA_CHALLENGE_DICT_KEY]==nil){
            iDict = [[[NSMutableDictionary alloc] init] autorelease];
        }
        else{
            iDict = [sysProfile.dict getData:DATA_CHALLENGE_DICT_KEY];
        }
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button setFrame:CGRectMake(screenWidth - 100, 10, 75, 25)];
        [button setTitle:@"DAY END" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(dayEndOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button setFrame:CGRectMake(screenWidth - 200, 10, 75, 25)];
        button.tag = 1001;
        int rank_comp = [[iDict objectForKey:[NSString stringWithFormat:@"%@_%@",DATA_CHALLENGE_RANK_COMPLETED,@"s"] defaultValue:[NSNumber numberWithInt:0]] intValue];
        [button setTitle:[NSString stringWithFormat:@"s ++(%d)",rank_comp] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(debugRankComplete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button setFrame:CGRectMake(screenWidth - 300, 10, 75, 25)];
        button.tag = 1002;
        rank_comp = [[iDict objectForKey:[NSString stringWithFormat:@"%@_%@",DATA_CHALLENGE_RANK_COMPLETED,@"a"] defaultValue:[NSNumber numberWithInt:0]] intValue];
        [button setTitle:[NSString stringWithFormat:@"a ++(%d)",rank_comp] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(debugRankComplete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button setFrame:CGRectMake(screenWidth - 100, 40, 75, 25)];
        button.tag = 1003;
        rank_comp = [[iDict objectForKey:[NSString stringWithFormat:@"%@_%@",DATA_CHALLENGE_RANK_COMPLETED,@"b"] defaultValue:[NSNumber numberWithInt:0]] intValue];
        [button setTitle:[NSString stringWithFormat:@"b ++(%d)",rank_comp] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(debugRankComplete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button setFrame:CGRectMake(screenWidth - 200, 40, 75, 25)];
        button.tag = 1004;
        rank_comp = [[iDict objectForKey:[NSString stringWithFormat:@"%@_%@",DATA_CHALLENGE_RANK_COMPLETED,@"c"] defaultValue:[NSNumber numberWithInt:0]] intValue];
        [button setTitle:[NSString stringWithFormat:@"c ++(%d)",rank_comp] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(debugRankComplete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
    }
    
    [self.rootViewController showBannerWithViewController:self zone:@"ingame" position:BANNER_BOTTOM animated:YES];
}

-(void)setReportTitle{
    QuestConditionState *condition1 = [quest.currentState objectAtIndex:0];
    QuestConditionState *condition2 = nil;
    QuestConditionState *condition3 = nil;
    if([quest.currentState count] >=2){
        condition2 = [quest.currentState objectAtIndex:1];
    }
    if([quest.currentState count] >=3){
        condition3 = [quest.currentState objectAtIndex:2];
    }
    
    [_questTitleLabel setText:LOCALIZATION_TEXT(@"Today's Challenge")];
    
    if(quest.marked == YES && condition1.complete == YES && (condition2 == nil || condition2.complete == YES) && (condition3 == nil || condition3.complete == YES)){
        [_questTitleLabel setText:LOCALIZATION_TEXT(@"Challenge Complete")];
        currentQuestState = QUEST_COMPLETED;
        [[SoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_QUEST_REPORT_SUCCESS];
        //[self logChallengeResult:YES];
    } else if (quest.marked){
        [_questTitleLabel setText:LOCALIZATION_TEXT(@"Challenge Failed")];
        currentQuestState  = QUEST_FAILED;
        [[SoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_QUEST_REPORT_FAIL];
        //[self logChallengeResult:NO];
    }
}

-(void)dealloc{
    iQuitGameButton = nil;
    questReq1Box = nil;
    questReq2Box = nil;
    resultMark = nil;
    [super dealloc];
}

-(IBAction)dayEndOnClick{
    [_stageViewController hideQuestReportView];
    _stageViewController.stage.stageTime = [_stageViewController.stage getDayLengthValue];
    [_stageViewController.stage stageEnd];
}

-(void)showResultAnim{
    if(currentQuestState != QUEST_COMPLETED){
        return;
    }
    
    [resultMark setHidden:YES];
    
    [AnimViewManager addAnimView:resultMark actions:
     [avDelay actionWithDuration:0.5f],
     [avHidden setHidden:NO],
     [avFade setAlpha:1],
     [avScale setScale:2.0f],
     [avScale actionWithDuration:0.5f scale:1.0f]
     ,nil];
    
    if (!_reward1Icon.hidden) {
        UIImageView *icon1Anim = [[[UIImageView alloc] initWithImage:_reward1Icon.image] autorelease];
        [icon1Anim setFrame:_reward1Icon.frame];
        [self.view addSubview:icon1Anim];
        [AnimViewManager addAnimView:icon1Anim actions:
         [avDelay actionWithDuration:0.5f],
         [avMulti actionWithDuration:1 actions:[avMove actionWithDuration:1 center:CGPointMake(icon1Anim.center.x, icon1Anim.center.y - [GameUnit pixelLengthFromUnitLength:5])], [avFade actionWithDuration:1 alpha:0], nil],
         nil];
        
        [AnimViewManager addAnimView:_reward1Icon actions:
         [avDelay actionWithDuration:0.5f],
         [avScale actionWithDuration:0.2 scale:1.5],
         [avScale actionWithDuration:0.2 scale:1],nil];
    }
    if (!_reward2Icon.hidden) {
        UIImageView *icon2Anim = [[[UIImageView alloc] initWithImage:_reward2Icon.image] autorelease];
        [icon2Anim setFrame:_reward2Icon.frame];
        [self.view addSubview:icon2Anim];
        [AnimViewManager addAnimView:icon2Anim actions:
         [avDelay actionWithDuration:0.5f],
         [avMulti actionWithDuration:1 actions:[avMove actionWithDuration:1 center:CGPointMake(icon2Anim.center.x, icon2Anim.center.y - [GameUnit pixelLengthFromUnitLength:5])], [avFade actionWithDuration:1 alpha:0], nil],
         nil];
        [AnimViewManager addAnimView:_reward2Icon actions:
         [avDelay actionWithDuration:0.5f],
         [avScale actionWithDuration:0.2 scale:1.5],
         [avScale actionWithDuration:0.2 scale:1],nil];
    }
    if (!_reward3Icon.hidden) {
        UIImageView *icon3Anim = [[[UIImageView alloc] initWithImage:_reward3Icon.image] autorelease];
        [icon3Anim setFrame:_reward3Icon.frame];
        [self.view addSubview:icon3Anim];
        [AnimViewManager addAnimView:icon3Anim actions:
         [avDelay actionWithDuration:0.5f],
         [avMulti actionWithDuration:1 actions:[avMove actionWithDuration:1 center:CGPointMake(icon3Anim.center.x, icon3Anim.center.y - [GameUnit pixelLengthFromUnitLength:5])], [avFade actionWithDuration:1 alpha:0], nil],
         nil];
        [AnimViewManager addAnimView:_reward3Icon actions:
         [avDelay actionWithDuration:0.5f],
         [avScale actionWithDuration:0.2 scale:1.5],
         [avScale actionWithDuration:0.2 scale:1],nil];
    }
}

-(void)viewWillUnload{
    [AnimViewManager stopAllActionForView:resultMark];
    [AnimViewManager stopAllActionForView:_reward1Icon];
    [AnimViewManager stopAllActionForView:_reward2Icon];
    [AnimViewManager stopAllActionForView:_reward3Icon];
}

-(void)hideQuestResultView:(id)sender{
    [super hideQuestResultView:sender];
    [self hideBanner];
}

@end
