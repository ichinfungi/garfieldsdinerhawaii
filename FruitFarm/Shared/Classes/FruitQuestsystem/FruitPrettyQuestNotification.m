//
//  FruitPrettyQuestNotification.m
//  DCGameTemplate
//
//  Created by royl on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyQuestNotification.h"
#import "GameSetting.h"
#import "cocos2d.h"
#import "Localization.h"

@implementation FruitPrettyQuestNotification

-(id)initComplete {
    self = [super init];
    if(self != nil){
        int fontSize = 18;
        if([Utilities isiPad]){
            fontSize = 28;
        }
        _bar = [[DCSprite alloc] initWithFile:@"Challeng_banner.png"];
        _info = [CCLabelTTF labelWithString:LOCALIZATION_TEXT(@"Challenge Complete") fontName:@"Arial-BoldMT" fontSize:fontSize];
        [_info setColor:ccc3(87, 66, 36)];
        [_bar setAnchorPoint:CGPointMake(0.5, 0)];
        [_bar addChild:_info z:1];
        [self addChild:_bar];
        [self layout];
        [self show];
    }
    return self;
}

-(id)initFail {
    self = [super init];
    if(self != nil){
        int fontSize = 18;
        if([Utilities isiPad]){
            fontSize = 28;
        }
        _bar = [[DCSprite alloc] initWithFile:@"Challeng_banner.png"];
        _info = [CCLabelTTF labelWithString:LOCALIZATION_TEXT(@"Challenge Failed") fontName:@"Arial-BoldMT" fontSize:fontSize];
        [_info setColor:ccc3(87, 66, 36)];
        [_bar setAnchorPoint:CGPointMake(0.5, 0)];
        [_bar addChild:_info];
        [self addChild:_bar];
        [self layout];
        [self show];
    }
    return self;
}

-(void)layout{
    float bar_offset = 16;
    if([Utilities isiPad]){
        bar_offset = 32;
    }
    [_bar setPosition:CGPointMake([GameSetting getScreenCenter].x, -_bar.contentSize.height)];
    [_info setPosition:CGPointMake(_bar.contentSize.width/2, _bar.contentSize.height-bar_offset)];
}

-(void) show {
    float bar_height = 30;
    if([Utilities isiPad]){
        bar_height = 57;
    }
    [_bar runAction:[CCSequence actions:
                     [CCMoveBy actionWithDuration:0.25 position:CGPointMake(0, bar_height)],
                     [CCDelayTime actionWithDuration:2],
                     [CCMoveBy actionWithDuration:0.25 position:CGPointMake(0, -bar_height)],
                     [CCCallFunc actionWithTarget:self selector:@selector(destroy)], nil]];
}

@end
