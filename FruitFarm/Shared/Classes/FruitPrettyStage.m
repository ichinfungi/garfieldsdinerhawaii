//
//  FruitPrettyStage.m
//  DCGameTemplate
//
//  Created by Calvin on 24/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FruitPrettyStage.h"
#import "AchievementSystem.h"
#import "CCActionEase.h"
#import "DCProfileManager.h"
#import "DCGraphicEngine.h"
#import "FruitGameSetting.h"
#import "FruitStageViewController.h"
#import "GameCenter.h"
#import "GameUnit.h"
#import "GamePointManager.h"
#import "NSDictionaryExtend.h"
#import "PrettyManager.h"
#import "FruitPrettyFacilityUser.h"
#import "PathFindingManager.h"
#import "PrettyFacilityUserGenerator.h"
#import "CCLabelTTF.h"
#import "PrettyStaff.h"
#import "PrettyStage.h"
#import "CCTouchDispatcher.h"
#import "PrettyConsumableItemController.h"
#import "PrettySoundEventManager.h"
#import "Localization.h"
#import "PrettyQuestContorller.h"
#import "FruitPrettyFacility.h"
#import "FruitStageViewController.h"
#import "FruitPrettyConsumableItemController.h"
#import "FruitMenuViewController.h"


@implementation FruitPrettyStage

@synthesize _CustomerArray;
@synthesize _CustomerGeneratorArray;
@synthesize _WaitingOutdoorArray;


#pragma mark -
#pragma mark init & release & dealloc

- (id)init {
	self = [super init];
    if (self != nil) {
        backgroundCarSwanpTimer = 0;
		_CustomerArray = [[NSMutableArray alloc] init];
		_CustomerGeneratorArray = [[NSMutableArray alloc] init];
        _WaitingOutdoorArray = [[NSMutableArray alloc] init];
        shouldDoorOpen = NO;
	}
	return self;
}

- (void)dealloc
{
	if (_CustomerArray != nil) {
		[_CustomerArray removeAllObjects];
		[_CustomerArray release];
		_CustomerArray = nil;
	}
	if (_CustomerGeneratorArray != nil) {
		[_CustomerGeneratorArray removeAllObjects];
		[_CustomerGeneratorArray release];
		_CustomerGeneratorArray = nil;
	}
    if (_WaitingOutdoorArray != nil){
        [_WaitingOutdoorArray removeAllObjects];
        [_WaitingOutdoorArray release];
        _WaitingOutdoorArray = nil;
    }
	[super dealloc];
}

- (void)cleanArrays {
	[super cleanArrays];
	if (_CustomerGeneratorArray != nil) {
		[_CustomerGeneratorArray removeAllObjects];
	}
    if (_WaitingOutdoorArray != nil){
        [_WaitingOutdoorArray removeAllObjects];
    }
    if (_CustomerArray != nil){
        [_CustomerArray removeAllObjects];
    }
}

- (void)showBackgroundCar{
    PrettyFacilityUser *facilityUser = [[[AutoClass(@"PrettyFacilityUser") alloc] init:1+arc4random()%10 position:CGPointMake(47, 0)] autorelease];
	[facilityUser.updateZSignal addListener:self withSelector:@selector(updateObjectZ:)];
	[facilityUser.updateZSignal dispatchWithSender:facilityUser];
	[facilityUser.touchBeganSignal addListener:self withSelector:@selector(facilityUserOnClick:)];
	[facilityUser.dragMoveSignal addListener:self withSelector:@selector(facilityUserOnDrag:)];
	[facilityUser.dragEndSignal addListener:self withSelector:@selector(facilityUserOnDragEnd:)];
    [facilityUser.removeSignal addListener:self withSelector:@selector(facilityUserOnRemove:)];
	[facilityUser.spriteChangeSignal addListener:self withSelector:@selector(onSpriteChange:)];
	[facilityUser.timeoutSignal addListener:self withSelector:@selector(facilityUserOnTimeout:)];
	[facilityUser.finishedAllServiceSignal addListener:self withSelector:@selector(facilityUserOnFinishedAllService:)];	
	[_StageObjectArray addObject:facilityUser];
	[self displayStageObject:facilityUser];
    [facilityUser addWaypoint:CGPointMake(-20, 320)];
    [facilityUser.reachDestSignal addListener:facilityUser withSelector:@selector(disappear)];
}

- (NSString*)getBGM
{
    if(self.day % 3 == 0){
        return @"gameplay1.mp3";
    }
    else if (self.day %3 == 1){
        return @"gameplay2.mp3";
    }
    else{
        return @"gameplay3.mp3";
    }
}

- (void)loadMap:(NSString *)pMap
{
    if (staff_map != nil) {
        [staff_map release];
    }
	staff_map = [[PathMap alloc] initWithFile:@"Staff_map.map"];
    [super loadMap:pMap];
    
}

- (void)drawMapPath
{
    if (staff_map != nil) {
		for (int y=0;y< [staff_map getHeight];y++) {
			for (int x=0; x<[staff_map getWidth];x++) {			
				CCLabelTTF *pLabel = [CCLabelTTF labelWithString:[staff_map pathTypeDesc:[staff_map getPathTypeAtX:x y:y]] fontName:@"arial" fontSize:16];
				pLabel.position = [GameUnit pixelFromUnit:CGPointMake(x, y)];
				[pLabel setColor:ccRED];
				[[DCGraphicEngine layer] addChild:pLabel z:10000000];
			}
		}
	}
    [super drawMapPath];
}

- (void)facilityUserWalkToNextFacilityQueue:(PrettyFacilityUser*)facilityUser
{
    if([facilityUser getNextFacilityDNAID]==2){
        [facilityUser hideBubble];
    }
    	[self facilityUserWalkToFacilityQueue:facilityUser facility:[self findAvailableFacilityByDNAID:facilityUser.getNextFacilityDNAID]];
}

- (void)facilityUserWalkToNextFacility:(PrettyFacilityUser *)facilityUser
{
    if([facilityUser getNextFacilityDNAID]==2){
        [facilityUser hideBubble];
    }
    if([facilityUser getNextFacilityDNAID]==1){
        ((FruitPrettyFacility*)[self findFacilityByDNAID:9]).waitingUser--;
    //   [_WaitingOutdoorArray removeObject:facilityUser];
    }
    
    [self facilityUserWalkToFacility:facilityUser facility:[self findAvailableFacilityByDNAID:facilityUser.getNextFacilityDNAID]];
}

- (void)facilityUserOnDrag:(PrettyFacilityUser*)facilityUser
{
    //avoid User can drag when it waiting for cashier and wildcard is on
    if([facilityUser getNextFacilityDNAID] == 2){
        [facilityUser dragEnd];
    }
    
    [super facilityUserOnDrag:facilityUser];
}


- (void)summonStaffToFacility:(PrettyStaff*)staff facility:(PrettyFacility*)facility
{		
	//override to make staff use other map
	CGPoint destUnit = [GameUnit unitFromPixel:[facility getStaffPos]];
	CGPoint startUnit = [GameUnit unitFromPixel:staff.sprite.position];
	
	NSArray *waypointUnitList = [[PathFindingManager sharedManager] findPathForMap:staff_map startPoint:startUnit endPoint:destUnit];
	
	[staff addWaypointArray:[GameUnit pixelArrayFromUnitArray:waypointUnitList]];
	
	[staff gotoFacility:facility];	
	
	if (destUnit.x == startUnit.x && destUnit.y == startUnit.y) {
        // already arrive;
        [self staffReachedDestination:staff];
    }
    else {
        // wait for arriving signal
        [staff.reachDestSignal addListener:self withSelector:@selector(staffReachedDestination:)];
    }
}

- (void)facilityUserExit:(PrettyFacilityUser*)facilityUser
{
	if (facilityUser.facility != nil) {
		[facilityUser.facility facilityUserLeft:facilityUser];
        if (facilityUser.facility.facilityUserQueueAble) {
			[self facilityQueueShiftNext:facilityUser.facility];
		}
	}
	
	if([facilityUser isDragging]){
        [facilityUser restorePositionBeforeDraggingImmediate];
        [facilityUser dragEnd];
    }
    
    CGPoint EndPoint = CGPointMake(47, 7);
    
	NSArray *waypointUnitList = [[PathFindingManager sharedManager] findPathForMap:map startPoint:[GameUnit unitFromPixel:facilityUser.sprite.position] endPoint:EndPoint];
    
    [facilityUser forceStop];
	
	[facilityUser addWaypointArray:[GameUnit pixelArrayFromUnitArray:waypointUnitList]];
    
    [facilityUser addWaypoint:[GameUnit pixelFromUnit:CGPointMake(49, 7)]];
	
    //[facilityUser.sprite setTouchPriorityOffset:-5];
    [facilityUser.sprite setUserInteractionEnabled:NO];
    
	[facilityUser.reachDestSignal addListener:facilityUser withSelector:@selector(disappear)];
}

- (void)facilityUserOnFinishedAllService:(PrettyFacilityUser*)facilityUser
{
	float factor = 1;
	if (facilityUser.perfect) {
		self.todayPerfectFacilityUser++;
		//factor = 1 + PERFECT_FACILITY_USER_BONUS_PERCENTAGE / 100.0f;
	}
	else {
		self.todayNormalFacilityUser++;
	}
	
    [self addMoney:facilityUser.profit*factor*facilityUser.moneyFactor];
	self.todayMoney += facilityUser.profit*factor*facilityUser.moneyFactor;
	
	[self addScore:facilityUser.score*factor*factor*facilityUser.scoreFactor];
	self.todayScore += facilityUser.score*factor*facilityUser.scoreFactor;
	
	[self setExp:self.exp+facilityUser.finishedExp];
	self.todayExp += facilityUser.finishedExp;
    
    [facilityUser displayBubble:[[[PrettyManager sharedManager] getDNADict:FACILITY_USER_DNA_FILE] getData:@"Bubbles/picDict/finishedAll"] style:GAMEOBJECT_BUBBLESTYLE_LOOPROTATE];
    
}

- (void)facilityUserOnTimeout:(PrettyFacilityUser*)facilityUser
{
    [super facilityUserOnTimeout:facilityUser];
}

// generate background cars
- (void)updateGenerators
{
    /*if((backgroundCarSwanpTimer < stageTime) && (self.stageState == STAGE_STARTED)){
        [self showBackgroundCar];
        backgroundCarSwanpTimer = stageTime + BG_CAR_BASE_TIME + arc4random()%BG_CAR_OFFSET_TIME;
    }*/
    
	PrettyFacilityUser *facilityUser;
	PrettyFacility *facility = nil;
	NSMutableArray* tArray = [_FacilityUserGeneratorArray copy];
	for (PrettyFacilityUserGenerator *generator in tArray) {
        if (stageState == STAGE_ENDING && !generator.ignoreDayEnd) {
			continue;
		}
        //if([_WaitingOutdoorArray count] >= 2){
        //    [generator resetSpawnTimer:stageTime + 15];
        //}
        //if(((FruitPrettyFacility*)[self findFacilityByID:25]).waitingUser > 2){
        //   [generator resetSpawnTimer:stageTime + 10];
        //}
        
		if ([generator canGenerateFacilityUser:stageTime CurrentScore:_score CurrentUserCount:(int)[_FacilityUserArray count] isBlocked:self.isEntryBlocked]) {
            // generate facilityUser directly at facility
            int User = 0;
            if(self.isVIPDay){
                User = [generator generateVIPFacilityUser:stageTime];
            }
            else{
                User = [generator generateFacilityUser:stageTime];
            }
			if (generator.enterFacilityID || generator.enterFacilityDNAID) {
				if (generator.enterFacilityID > 0) {
					facility = [self findFacilityByID:generator.enterFacilityID];
				}
				else if (generator.enterFacilityDNAID > 0) {
					facility = [self findFacilityByDNAID:generator.enterFacilityDNAID];
				}
				if (facility != nil && [facility getMaxUserCount] > [facility.facilityUserArray count]) {
					facilityUser = [self addFacilityUser:User position:CGPointMake(0, 0)];
                    if( facilityUser == nil){
                        NSLog(@"[Generate Facility User Error]:FacilityUserDNAID = %d",User);
                        break;
                    }
					[self facilityUserEnterFacility:facilityUser facility:facility];
				}
			}
            // generate facilityUser at coordinate
			else {
				facilityUser = [self addFacilityUser:User position:generator.enterCoordinate];
                if( facilityUser == nil){
                    NSLog(@"[Generate Facility User Error]:FacilityUserDNAID = %d",User);
                    break;
                }
				[facilityUser.sprite setPosition:[GameUnit pixelFromUnit:generator.enterCoordinate]];
                NSLog(@"waitingUser:%d",((FruitPrettyFacility*)[self findFacilityByDNAID:9]).waitingUser);
                if(((FruitPrettyFacility*)[self findFacilityByDNAID:9]).waitingUser >= 2){
                    [self facilityUserExit:facilityUser]; 
                    [generator resetSpawnTimer:stageTime + 10.0];
                }
                else{
                    [self facilityUserPrepareForNextFacility:facilityUser];
                }

			}
            
            if(_isWildCardActive){
                ((FruitPrettyFacilityUser*)facilityUser).isWildCard = YES;
            }
        }
	}
    [tArray release];
    
} 

// override to set map button function
- (void)stagePrepare
{	
	if (self.stageViewController != nil
        && self.day != 0 && self.day%[AutoClass(@"GameSetting") getStagePrepareTakeOverInterval] == 0
        )
	{
		[[MunerisAppEvents report:MUNERIS_ZONE_PREPARE withViewController:[[UIApplication sharedApplication] keyWindow].rootViewController] execute];
	}
	
	BOOL isLevelUp = [self checkLevelUp];
	
	if (isLevelUp) {
		[self stageLevelUp];
	}
	
	[self saveStageGameDataToProfile];
	
	[[SoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_STAGE_PREPARE data:self];
	
	[self stageResetDay];
	stageState = STAGE_PREPARE;
    [((FruitStageViewController*)self.stageViewController) setCatagoryButtonsEnable:YES];
	
	
	if ([self hasDialogue:DIALOGUE_TYPE_PREPARE]) {
		[self showDialogue:DIALOGUE_TYPE_PREPARE selector:@selector(stagePrepare)];
		return;
	}
	else {
		[self hideDialogue];
	}
	
	PrettyFacility *facility;
	for (int i=0;i<[_FacilityArray count];i++) {
		facility = (PrettyFacility*)[_FacilityArray objectAtIndex:i];
        
        // restore music player
        if ([facility.specialFacilityType isEqualToString:@"MusicPlayer"]) {
            NSDictionary *dnaDict = [[(PrettyManager*)[PrettyManagerClass sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", facility.dnaID]];
            facility.maxUserCount = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/maxUserCount",facility.level]]intValue];
            self.musicPlayerUsedCount = 0;
            UsedMusicPlayer = NO;
            if([facility.sprite getChildByTag:1] != nil){
                CCLabelTTF *MusicPlayerCount = (CCLabelTTF*)[facility.sprite getChildByTag:1];
                [MusicPlayerCount setString:[NSString stringWithFormat:@"%.2d",facility.maxUserCount]];
            }
        }
        
        // restore blocker
        if ([facility.specialFacilityType isEqualToString:@"Blocker"]){
            NSDictionary *dnaDict = [[(PrettyManager*)[PrettyManagerClass sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", facility.dnaID]];
            facility.maxUserCount = [[dnaDict getData:[NSString stringWithFormat:@"level/%d/maxUserCount",facility.level]]intValue];
            self.blockerUsedCount = 0;
            self.isEntryBlocked = NO;
            [self removeBlockerAction:facility];
            if([facility.sprite getChildByTag:1] != nil){
                CCLabelTTF *blockerCount = (CCLabelTTF*)[facility.sprite getChildByTag:1];
                [blockerCount setString:[NSString stringWithFormat:@"%.2d",facility.maxUserCount]];
            }
        }
        
        if(facility.dnaID == 9){
            ((FruitPrettyFacility*)facility).waitingUser = 0;
        }
	}
	
	PrettyStaff *staff;
	for (int i=0;i<[_StaffArray count];i++) {
		staff = (PrettyStaff*)[_StaffArray objectAtIndex:i];
		if (staff != nil) {
			[staff resetPosition];
		}
	}
    
    //restore door position
    PrettyObstacle *obstacle;
    for (int i = 0; i < [_ObstacleArray count]; i++){
        obstacle = [_ObstacleArray objectAtIndex:i];
        if(obstacle.dnaID == 5){
            if([Utilities isiPad]){
                obstacle.sprite.position = [GameUnit pixelFromUnit:CGPointMake(31, 7.1)];
            }
            else{
                obstacle.sprite.position = [GameUnit pixelFromUnit:CGPointMake(31, 6)];
            }
        }
    }
	
	//EDIT: Request that staff be the first category to show
    //[self showAllFacilityUpgradeBubble];
	[self showAllStaffUpgradeBubble];
	
	if (self.stageViewController != nil) {
		[self.stageViewController setPauseButtonEnable:NO];
		[self.stageViewController showPrepareView];
		[self.stageViewController setPrepareViewButton:STAGEVIEW_BTN_OK target:self selector:@selector(stagePrepareEnd)];
	}
    
    if([self getIsQuestSystemActivated]){
        [self checkChallengeAchievements];
        [self.stageViewController.iQuestPopUpButton setHidden:YES];
    }
    
	if (self.stageViewController != nil) {
		[(FruitStageViewController*)self.stageViewController setPrepareViewButton:STAGEVIEW_BTN_MAP target:self selector:@selector(quitGame)];
	}
}

-(void)hideAllUpgradeBubble{
    for (int i = 0 ; i < [_StaffArray count]; i++) {
        PrettyStaff *tStaff = [_StaffArray objectAtIndex:i];
        [tStaff cancelUpgrade];
    }
    for (int i = 0 ; i < [_FacilityArray count]; i++) {
        PrettyFacility *tFacility = [_FacilityArray objectAtIndex:i];
        [tFacility cancelFacilityUpgrade];
    }
    for (int i = 0 ; i < [_ObstacleArray count]; i++) {
        PrettyObstacle *tObstacle = [_ObstacleArray objectAtIndex:i];
        [tObstacle cancelFacilityUpgrade];
    }
}

-(void)showAllFacilityUpgradeBubble{
    [self hideAllUpgradeBubble];
    PrettyObstacle *obstacle;
	for (int i=0;i<[_ObstacleArray count];i++) {
		obstacle = (PrettyObstacle*)[_ObstacleArray objectAtIndex:i];
		[obstacle.sprite setUserInteractionEnabled:YES];
		if (obstacle != nil && _level >= obstacle.upgradeLevelPrerequisite) {
			[obstacle showFacilityUpgrade];
		}
	}
	
	PrettyFacility *facility;
	for (int i=0;i<[_FacilityArray count];i++) {
		facility = (PrettyFacility*)[_FacilityArray objectAtIndex:i];
		if (facility != nil && facility.upgradeMoney >=0 && facility.upgradeGamePoint >=0) {
			[facility showFacilityUpgrade];
		}
	}
}

-(void)showAllStaffUpgradeBubble{
    [self hideAllUpgradeBubble];
    PrettyStaff *staff;
	for (int i=0;i<[_StaffArray count];i++) {
		staff = (PrettyStaff*)[_StaffArray objectAtIndex:i];
		[staff.sprite setUserInteractionEnabled:YES];
		if (staff != nil) {
			[staff resetPosition];
            if(staff.upgradeMoney >= 0 && staff.upgradeGamePoint >= 0){
                [staff showUpgrade];
            }
		}
	}
}

-(void)stagePrepareEnd{
	[[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
    [super stagePrepareEnd];
}

- (PrettyFacility*)facilityUpgrade:(PrettyFacility*)facility
{
    PrettyFacility *newFacility = [super facilityUpgrade:facility];
    //Achievement : Upgrade all equipment to max. level
    if([self._FacilityArray count]==25){
        BOOL achieved = YES;
        for (PrettyFacility *object in self._FacilityArray){
            NSLog(@"facility %d level count:%lu currentLv:%d",object.ID, (unsigned long)[[[[PrettyManager sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d/level",object.dnaID]]count],object.level);
            if([[[[PrettyManager sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d/level",object.dnaID]]count] > object.level){
                achieved = NO;
            }
        }
        if(achieved){
            [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_maxupgrade percent:100];
        }
    }
    
    return newFacility;
}

- (int)setExp:(int)pExp
{
	[super setExp:pExp];
	if (self.stageViewController != nil) {
		[(FruitStageViewController*)self.stageViewController setExpPercent:_exp / [self getLevelUpExp] animated:((stageState == STAGE_STARTED || stageState == STAGE_ENDING) && pExp > 0)];		
	}
	return _exp;
}

- (BOOL)facilityUserTrytoWalkToNextFacility:(PrettyFacilityUser*)facilityUser
{
	PrettyFacility* facility = [self findAvailableFacilityByDNAID:[facilityUser getNextFacilityDNAID]];
	if (facility != nil) {
        [facilityUser.facility facilityUserLeft:facilityUser];
		
		// if someone is queueing
		if (facility.facilityUserQueueAble && [facility.facilityUserQueueArray count]) {
			[self facilityUserWalkToNextFacilityQueue:facilityUser];
			return YES;
		}
		// if facility is empty, user walk to facility
		else if ([facility canFacilityUserEnter:facilityUser]) {
			[self facilityUserWalkToNextFacility:facilityUser];
			return YES;
		}
		// if queue
		else if (facility.facilityUserQueueAble) {
			[self facilityUserWalkToNextFacilityQueue:facilityUser];
			return YES;
		}	
	}
	return NO;
}

- (void)handleUpdateFacilityUser:(PrettyFacilityUser *)facilityUser {
    [super handleUpdateFacilityUser:facilityUser];
    //User will not angry in day 1
    if(self.day == 1){
        facilityUser.stateTimer = fmax(facilityUser.stateTimer, stageTime);
    }
    //PrettyObstacle *door = [self findObstacleByID:27];
    
}

- (void)handleUpdateStaff:(PrettyStaff *)staff {
    [super handleUpdateStaff:staff];
    if(staff.dnaID == 6 && staff.staffState == STAFF_AVAILABLE){
        PrettyFacility* tFacility = [self findFacilityByDNAID:2];
        if(tFacility != nil){
            tFacility.staff = staff;
            if([tFacility.facilityUserArray count] > 0){
                [self facilityTryToStartService:tFacility];
            }
        }
    }
}

- (void)handleUpdateObstacle:(PrettyObstacle*)obstacle {
    if(obstacle.dnaID == 5){
        shouldDoorOpen = NO;
        int doorMove = [GameUnit pixelLengthFromUnitLength:7.5];
        float duration = 0.1;
        //float moveOffset = 0.15;
        float moveOffset = 0.3;
        CGPoint doorOrginalPos = [GameUnit pixelFromUnit:CGPointMake(31, 6)];
        if([Utilities isiPad]){
            duration = 0.05;
            //moveOffset = 0.5;
            moveOffset = 0.6;
            doorOrginalPos = [GameUnit pixelFromUnit:CGPointMake(31, 7.5)];
        }
        for(int i= 0 ; i < [_FacilityUserArray count]; i++){
            PrettyFacilityUser *facilityUser = [_FacilityUserArray objectAtIndex:i];
			if((fabs(facilityUser.sprite.position.x - doorOrginalPos.x) <= [GameUnit pixelLengthFromUnitLength:7]) && (fabs(facilityUser.sprite.position.y - doorOrginalPos.y) <= [GameUnit pixelLengthFromUnitLength:7])){
				shouldDoorOpen = YES;
            }
        }
        
        PrettyObstacle *Door = [self findObstacleByID:27];
        if(shouldDoorOpen){
            if(Door.sprite.position.x - [GameUnit pixelLengthFromUnitLength:moveOffset] >= doorOrginalPos.x - doorMove){
                Door.sprite.position = CGPointMake(Door.sprite.position.x - [GameUnit pixelLengthFromUnitLength:moveOffset], Door.sprite.position.y);
            }
            if(Door.sprite.position.x > doorOrginalPos.x - doorMove){
                [Door.sprite stopAllActions];
                [Door.sprite runAction:[CCMoveTo actionWithDuration:duration position:CGPointMake(doorOrginalPos.x - doorMove, Door.sprite.position.y)]];                               
            }
        }
        else if(!shouldDoorOpen){
            if(Door.sprite.position.x + [GameUnit pixelLengthFromUnitLength:moveOffset] <= doorOrginalPos.x){
                Door.sprite.position = CGPointMake(Door.sprite.position.x + [GameUnit pixelLengthFromUnitLength:moveOffset], Door.sprite.position.y);
            }
            if(Door.sprite.position.x <= doorOrginalPos.x){
                [Door.sprite stopAllActions];
                [Door.sprite runAction:[CCMoveTo actionWithDuration:duration position:CGPointMake(doorOrginalPos.x, Door.sprite.position.y)]]; 
            }
			
        }
    }
}


/*- (void)facilityUserOnRemove:(PrettyFacilityUser *)facilityUser{
    [_WaitingOutdoorArray removeObject:facilityUser];
    [super facilityUserOnRemove:facilityUser];
}*/

//overrided for Achievement checking
- (PrettyFacilityUser*)addFacilityUser:(int)pDNAID position:(CGPoint)pPosition 
{
	PrettyFacilityUser *facilityUser = [[[AutoClass(@"PrettyFacilityUser") alloc] init:pDNAID position:pPosition] autorelease];
	[facilityUser.updateZSignal addListener:self withSelector:@selector(updateObjectZ:)];
	[facilityUser.updateZSignal dispatchWithSender:facilityUser];
	[facilityUser.touchBeganSignal addListener:self withSelector:@selector(facilityUserOnClick:)];
	[facilityUser.dragMoveSignal addListener:self withSelector:@selector(facilityUserOnDrag:)];
	[facilityUser.dragEndSignal addListener:self withSelector:@selector(facilityUserOnDragEnd:)];
    [facilityUser.removeSignal addListener:self withSelector:@selector(facilityUserOnRemove:)];
	[facilityUser.spriteChangeSignal addListener:self withSelector:@selector(onSpriteChange:)];
	[facilityUser.timeoutSignal addListener:self withSelector:@selector(facilityUserOnTimeout:)];
	[facilityUser.finishedAllServiceSignal addListener:self withSelector:@selector(facilityUserOnFinishedAllService:)];	
	[_StageObjectArray addObject:facilityUser];
	[_FacilityUserArray addObject:facilityUser];
	[self displayStageObject:facilityUser];
    
    if(!self.isVIPDay){
        if(facilityUser.isVIP){
            [self displayVIP:facilityUser];
            [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_spvisit percent:100];
        }
    }
    
    facilityUser.gameSpeed = self.gameSpeed;
    
    // tips on for stage 1 first user
	if (self.dnaID == 1 && self.day == 1 && self.stageTime < 3.0) {
		[facilityUser tipOn:YES];
		[facilityUser.updateTipSignal addListener:self withSelector:@selector(facilityUserOnUpdateTip:)];
	}
    
    [facilityUser.sprite setUserInteractionEnabled:NO];
    [facilityUser setFacilityCycle:[self generateFacilityCycle2]];
	return facilityUser;
}

-(void)displayVIP:(PrettyFacilityUser *)facilityUser{
    for(CCNode *child in [DCGraphicEngine layer].children){
        [child pauseSchedulerAndActions];
    }
    if ([[DCGraphicEngine layer] getChildByTag:101] != nil){
        [[[DCGraphicEngine layer] getChildByTag:101] stopAllActions];
        [[DCGraphicEngine layer] removeChildByTag:101 cleanup:YES];
    }
    
    DCSprite *VIPSprite = [DCSprite spriteWithFile:facilityUser.VIP_pic];
    [VIPSprite setPosition:CGPointMake(screenWidth, VIPSprite.textureRect.size.height/2)];
    [[DCGraphicEngine layer] addChild:VIPSprite z:9999999 tag:101];
    [VIPSprite runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.1 position:CGPointMake(screenWidth-VIPSprite.textureRect.size.width/2, VIPSprite.textureRect.size.height/2)],[CCDelayTime actionWithDuration:2.0],[CCFadeOut actionWithDuration:0.1],[CCCallFuncN actionWithTarget:self selector:@selector(removeVIP)],nil]];
    if(facilityUser.dnaID == 101){
		[[OALSimpleAudio sharedInstance] playEffect:@"PetSalon_Obama_v1.wav"];
    }
    else{
		[[OALSimpleAudio sharedInstance] playEffect:@"PetSalon_VIP_generic.mp3"];
    }
	[[OALSimpleAudio sharedInstance] setBgVolume:0];
    VIPAnimationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 480, 320)];
    [VIPAnimationView setUserInteractionEnabled:YES];
    [VIPAnimationView setBackgroundColor:[UIColor clearColor]];
    [self.stageViewController.view addSubview:VIPAnimationView];
    _isPlayingAnimation = YES;
}

-(void)removeVIP{
    [super removeVIP];
    for(CCNode *child in [DCGraphicEngine layer].children){
        [child resumeSchedulerAndActions];
    }
    if(VIPAnimationView != nil){
        [VIPAnimationView removeFromSuperview];
        [VIPAnimationView release];
        VIPAnimationView = nil;
    }
    _isPlayingAnimation = NO;
	
	[[OALSimpleAudio sharedInstance] setBgVolume:BGMVolume];
    
}

- (void)facilityUserArrivedFacility:(PrettyFacilityUser*)facilityUser
{
    [facilityUser.sprite setUserInteractionEnabled:YES];
    [super facilityUserArrivedFacility:facilityUser];
    /*if(facilityUser.facility.dnaID == 9){
        [_WaitingOutdoorArray addObject:facilityUser];
    }*/
}

// music player function
- (void)facilityOnClick:(PrettyFacility*)facility 
{
	[facility.sprite runAction:[CCSequence actions:[CCScaleTo actionWithDuration:0.1 scale:1.2],[CCScaleTo actionWithDuration:0.1 scale:1.0], nil]];
    if ([facility.specialFacilityType isEqualToString:@"MusicPlayer"] && facility.facilityState == FACILITY_AVAILABLE && stageState != STAGE_PREPARE){
        if(facility.maxUserCount > 0){
            [[SoundEventManager sharedManager] handleSoundEvent:PRETTY_SOUND_EVENT_MUSIC_PLAYER_START];            UsedMusicPlayer = YES;
            if([facility.sprite getChildByTag:1] != nil){
                CCLabelTTF *MusicPlayerCount = (CCLabelTTF*)[facility.sprite getChildByTag:1];
                [MusicPlayerCount setString:[NSString stringWithFormat:@"%.2d",facility.maxUserCount-1]];
            }
        }
    }
    
    if ([facility.specialFacilityType isEqualToString:@"Blocker"] && stageState != STAGE_PREPARE){
		[[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
        if(!self.isEntryBlocked && facility.maxUserCount > 0){
            if([facility.sprite getChildByTag:1] != nil ){
                CCLabelTTF *blockerCount = (CCLabelTTF*)[facility.sprite getChildByTag:1];
				[blockerCount setString:[NSString stringWithFormat:@"%.2d",facility.maxUserCount-1]];
            }
        }
    }
	
	[super facilityOnClick:facility];
    
    if (facility.facilityState == FACILITY_SHOWING_UPGRADE) {
        [self.stageViewController setPurchaseViewTitle:LOCALIZATION_TEXT(@"Upgrade") imageName:facility.upgradePic name:[NSString stringWithFormat:@"%@ Lv %d → Lv %d", facility.name, facility.level, facility.level+1] desc:facility.upgradeDesc money:facility.upgradeMoney gamePoint:facility.upgradeGamePoint];
    }
}

-(void)facilityUserOnClick:(PrettyFacilityUser *)facilityUser{
	[super facilityUserOnClick:facilityUser];
	[facilityUser.sprite runAction:[CCSequence actions:[CCScaleTo actionWithDuration:0.1 scale:1.2],[CCScaleTo actionWithDuration:0.1 scale:1.0], nil]];
}

-(void)staffOnClick:(PrettyStaff *)staff{
	[staff.sprite runAction:[CCSequence actions:[CCScaleTo actionWithDuration:0.1 scale:1.2],[CCScaleTo actionWithDuration:0.1 scale:1.0], nil]];
    [super staffOnClick:staff];
    if(staff.staffState == STAFF_SHOWING_UPGRADE){
        if(staff.level == 0){
            [self.stageViewController setPurchaseViewTitle:LOCALIZATION_TEXT(@"Hire") 
                                                 imageName:staff.upgradePic
                                                      name:[NSString stringWithFormat:@"%@ Lv %d", staff.name, 1] 
                                                      desc:staff.upgradeDesc 
                                                     money:staff.upgradeMoney 
                                                 gamePoint:staff.upgradeGamePoint];
        }
        else{
            [self.stageViewController setPurchaseViewTitle:LOCALIZATION_TEXT(@"Upgrade") 
                                        imageName:staff.upgradePic
                                             name:[NSString stringWithFormat:@"%@ Lv %d → Lv %d", staff.name, staff.level, staff.level+1] 
                                             desc:staff.upgradeDesc 
                                            money:staff.upgradeMoney 
                                        gamePoint:staff.upgradeGamePoint];
        }
    }
}

-(void)obstacleOnClick:(PrettyObstacle *)obstacle{
    [super obstacleOnClick:obstacle];
    NSDictionary *dnaDict = [[(PrettyManager*)[PrettyManagerClass sharedManager] getDNADict:FACILITY_DNA_FILE] getData:[NSString stringWithFormat:@"DNADict/%d", obstacle.upgradeToFacilityDNAID]];
    [self.stageViewController setPurchaseViewTitle:LOCALIZATION_TEXT(@"Purchase") imageName:[dnaDict getData:[NSString stringWithFormat:@"level/1/upgradePic"]] name:[NSString stringWithFormat:@"%@ Lv 1", [dnaDict objectForKey:@"name"]] desc:[dnaDict getData:@"level/1/upgrade/desc"] money:[[dnaDict getData:@"level/1/upgrade/money"] intValue] gamePoint:[[dnaDict getData:@"level/1/upgrade/gamePoint"] intValue]];
}

- (int)addMoney:(int)pMoney
{
	if (self.stageViewController != nil) {
		[(FruitStageViewController*)self.stageViewController showChangeMoney:pMoney>=0?[NSString stringWithFormat:@"+%d",pMoney]:[NSString stringWithFormat:@"%d",pMoney] style:pMoney>=0?CHANGE_STYLE_UP:CHANGE_STYLE_DOWN];
	}
	return [super addMoney:pMoney];
}

- (int)addMoneyWithoutAnimate:(int)pMoney
{
    return [super addMoney:pMoney];
}

- (int)addBonusMoney:(int)pMoney
{
    return 0;
}

- (float)addScore:(float)pScore
{
	if (self.stageViewController != nil) {
		[(FruitStageViewController*)self.stageViewController showChangeScore:pScore>=0?[NSString stringWithFormat:@"+%.1f",pScore]:[NSString stringWithFormat:@"%.1f",pScore] style:pScore>=0?CHANGE_STYLE_UP:CHANGE_STYLE_DOWN];
	}
    if (self.score >= 100){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_happiness100 percent:100];
    }
    if (self.score >= 500){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_happiness500 percent:100];
    }
    if (self.score >= 999){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_happiness999 percent:100];
    }
	return [super addScore:pScore];
}

- (float)addBonusScore:(float)pScore
{
    return 0;
}

#pragma mark -
#pragma mark Quest System

-(void)parseReward:(NSString *)command quantity:(NSNumber*)qty{
    if([command isEqualToString:@"staffboost"]){
        [(PrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager] addItemCountForKey:@"StaffBoostCountKey" count:[qty intValue]];
        [((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iStaffBoostBtn updateCountLabel];
    }
    else if([command isEqualToString:@"wildcard"]){
        [(PrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager] addItemCountForKey:@"WildCardFacilityCountKey" count:[qty intValue]];
        [((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iWildCardBtn updateCountLabel];
    }
    else if([command isEqualToString:@"spawn"]){
        [(PrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager] addItemCountForKey: @"SpawnBoostCountKey" count:[qty intValue]];
        [((FruitPrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager]).iSpawnBoostBtn updateCountLabel];
    }
    else{
        [super parseReward:command quantity:qty];
    }
}

#pragma mark -
#pragma mark LeaderBoard

- (void)displayResult
{
    [super displayResult];
	
	BOOL noMoreLevel = (([self getLevelUpMoney]) == 0 && ([self getLevelUpScore] == 0));
    [(FruitStageViewController*)self.stageViewController setReportViewTodayMoney:self.todayMoney
                                                                      todayScore:self.todayScore
                                                                  happyCustomers:(self.todayPerfectFacilityUser + self.todayNormalFacilityUser) 
                                                                    sadCustomers:self.todayWastedFacilityUser
                                                                       nextMoney:noMoreLevel?-1:[self getLevelUpMoney]
                                                                       nextScore:noMoreLevel?-1:[self getLevelUpScore]];
	DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
	if (sysProfile != nil) {

		NSMutableDictionary* statsDict = [sysProfile.dict getData:@"stats"];
		if (statsDict == nil) {
			statsDict = [[[NSMutableDictionary alloc] init] autorelease];
		}
		
		// total score leaderboard
		float total_score = 0;
		if ([statsDict getData:@"total_score"] != nil) {
			total_score = [[statsDict getData:@"total_score"] floatValue];
		}
		if (self.score > total_score) {
			[statsDict setObject:[NSNumber numberWithFloat:self.score] forKey:@"total_score"];
			[[GameCenter sharedManager] reportScore:((int64_t)self.score) forCategory:lb_happiness];
		}
		
		// money earned leader board
		int money_earned = 0;
		if ([statsDict getData:@"money_earned"] != nil) {
			money_earned = [[statsDict getData:@"money_earned"] intValue];
		}
		money_earned += self.todayMoney;		
		[statsDict setObject:[NSNumber numberWithFloat:money_earned] forKey:@"money_earned"];
		[[GameCenter sharedManager] reportScore:money_earned forCategory:lb_overallmoney];
		
		[sysProfile.dict setObject:statsDict forKey:@"stats"];
		[[DCProfileManager sharedManager] saveAllProfiles];
		
	}
}

- (void)stageLevelUp 
{   
    [super stageLevelUp];
    
    // Flurry
	NSDictionary* params = @{@"level": [NSNumber numberWithInt: _level]};
	[[[MunerisAppEvents report:@"stage_level_up" withViewController:[[UIApplication sharedApplication] keyWindow].rootViewController] setParametersWithDictionary:params] execute];

	UIViewController *viewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
	if(self.level >= 2) {[[MunerisAppEvents report:@"ppa_reach_lv2" withViewController:viewController] execute];}
    if(self.level >= 3) {[[MunerisAppEvents report:@"ppa_reach_lv3" withViewController:viewController] execute];}
    if(self.level >= 4) {[[MunerisAppEvents report:@"ppa_reach_lv4" withViewController:viewController] execute];}
	if(self.level >= 5) {[[MunerisAppEvents report:@"ppa_reach_lv5" withViewController:viewController] execute];}
	if(self.level >= 6) {[[MunerisAppEvents report:@"ppa_reach_lv6" withViewController:viewController] execute];}
	if(self.level >= 7) {[[MunerisAppEvents report:@"ppa_reach_lv7" withViewController:viewController] execute];}
	if(self.level >= 8) {[[MunerisAppEvents report:@"ppa_reach_lv8" withViewController:viewController] execute];}
	if(self.level >= 9) {[[MunerisAppEvents report:@"ppa_reach_lv9" withViewController:viewController] execute];}
	if(self.level >= 10) {[[MunerisAppEvents report:@"ppa_reach_lv10" withViewController:viewController] execute];}
	if(self.level >= 11) {[[MunerisAppEvents report:@"ppa_reach_lv11" withViewController:viewController] execute];}
	if(self.level >= 12) {[[MunerisAppEvents report:@"ppa_reach_lv12" withViewController:viewController] execute];}
	if(self.level >= 13) {[[MunerisAppEvents report:@"ppa_reach_lv13" withViewController:viewController] execute];}
	if(self.level >= 14) {[[MunerisAppEvents report:@"ppa_reach_lv14" withViewController:viewController] execute];}
    
    if([self checkLevelUp]){
        [self stageLevelUp];
    }
       
}


- (void)stageNextDay
{
	[super stageNextDay];	
    
	DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
	if (sysProfile != nil) {
		NSMutableDictionary* statsDict = [sysProfile.dict getData:@"stats"];
		if (statsDict == nil) {
			statsDict = [[[NSMutableDictionary alloc] init] autorelease];
		}
		
		// day leaderboard
		//int tDay = 0;
		BOOL update = NO;
        
        int shopdays = 0;
        if ([statsDict getData:@"total_shopdays"] != nil){
            shopdays = [[statsDict getData:@"total_shopdays"]intValue];
        }
        if(self.day > shopdays){
            [statsDict setObject:[NSNumber numberWithInt:self.day] forKey:@"total_shopdays"];
            [[GameCenter sharedManager] reportScore:self.day forCategory:lb_shopdays];
            update = YES;
        }
		

		
		if (update) {
			[sysProfile.dict setObject:statsDict forKey:@"stats"];
			[[DCProfileManager sharedManager] saveAllProfiles];
		}
	}

}

#pragma mark -
#pragma mark Achievements

- (void)purchaseOKCallBack
{
	if ([self.selectedObject isKindOfClass:([PrettyObstacle class])]) { 
        DCProfile *curProfile = [[DCProfileManager sharedManager] getCurrentProfile];
        if (curProfile.dict != nil){
            int totalEquipmentPurchased = 0;
            
            if([curProfile.dict getData:TotalPurchaseEquipmentKey] != nil){
                totalEquipmentPurchased = [[curProfile.dict getData:TotalPurchaseEquipmentKey]intValue];
            }
            else{
                totalEquipmentPurchased = 0;
            }
            
            totalEquipmentPurchased++;
            
            [curProfile.dict setObject:[NSNumber numberWithInt:totalEquipmentPurchased] forKey:TotalPurchaseEquipmentKey];
            [[DCProfileManager sharedManager] saveAllProfiles];
        }
    }    
    
    [super purchaseOKCallBack];
	if ([self._FacilityArray count] == 25) {			
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_allequipment percent:100];
	}
}

- (void)facilityOnFinishedServingFacilityUser:(PrettyFacility*)facility
{
	PrettyFacilityUser* facilityUser;
	
	if (facility.dnaID==2) {
		DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
		if (sysProfile == nil) {
			NSLog(@"impossible sys profile = nil");
			[super facilityOnFinishedServingFacilityUser:facility];
			return;			
		}
		NSMutableDictionary* statsDict = [sysProfile.dict getData:@"stats"];
		if (statsDict == nil) {
			statsDict = [[[NSMutableDictionary alloc] init] autorelease];
		}
	}
    if ([facility.specialFacilityType isEqualToString:@"MusicPlayer"]) {
		NSLog(@"remove all music notes");
        NSMutableArray *tArray = [_musicNoteArray copy];
		for (GameObject *musicNote in tArray) {
			[[DCGraphicEngine sharedManager].layer removeChild:musicNote.sprite cleanup:YES];
		}
        [[SoundEventManager sharedManager]handleSoundEvent:PRETTY_SOUND_EVENT_RESUME_BGM_ON_MUSIC_PLAYER_FINISH data:self];
		[_musicNoteArray removeAllObjects];
        [tArray release];
	}
    if([facility.specialFacilityType isEqualToString:@"Blocker"]){
        self.isEntryBlocked = NO;
        [self removeBlockerAction:facility];
    }
    if ([self hasDialogue:[NSString stringWithFormat:@"%@%d", DIALOGUE_TYPE_SERVED_FACILITY, facility.dnaID]]) {
		[self showDialogue:[NSString stringWithFormat:@"%@%d", DIALOGUE_TYPE_SERVED_FACILITY, facility.dnaID] selector:@selector(hideDialogue)];
	}
    BOOL hasQueueingFacilityUser = facility.facilityUserQueueAble && [facility.facilityUserQueueArray count];
	
    //make score added after User finished all service
    NSMutableArray* userArray = [facility.facilityUserArray copy];
    for (int i=0;i<[userArray count];i++) {
		facilityUser = (PrettyFacilityUser*)[userArray objectAtIndex:i];
		if (facilityUser.facilityUserState == FACILITY_USER_USING_SERVICE) {
			
			facilityUser.profit += [self getSpeedUpProfit:facility.serviceProfit*_itemMoneyFactor];
            
            facilityUser.score += [self getSpeedUpScore:facility.serviceScore];
			
			[facilityUser finishedServiceAtFacility:facility];
            if(_isSpawnBoostActive && facility.serviceProfit > 0){
                [(FruitPrettyFacilityUser*)facilityUser showMoneyItemEffect];
            }
			
			[self setExp:self.exp+facility.serviceFinishedExp];
			self.todayExp += facility.serviceFinishedExp;
			
			[self facilityUserPrepareForNextFacility:facilityUser];
			
            if(facility.dnaID == 2){
                [facility showRewardLabelWithMoney:facilityUser.profit*facilityUser.moneyFactor score:facilityUser.score*facilityUser.scoreFactor exp:0];
            }
			
		}
	}
	if (hasQueueingFacilityUser) {
		[self facilityOnClick:facility];
	}
    [mQuestController updateCurrentQuests];
    [userArray release];
}

- (int)setDay:(int)pDay
{
    if (pDay >= 50){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_50days percent:100];
    }
    if (pDay >= 100){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_100days percent:100];
    }
    if (pDay >= 200){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_200days percent:100];
    }
		
	return [super setDay:pDay];
}

- (int)setLevel:(int)pLevel
{
    if(pLevel >= 5){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_shoplv5 percent:100];
    }
    if(pLevel >= 10){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_shoplv10 percent:100];
    }
        
	return [super setLevel:pLevel];
}

- (void)removeBlockerAction:(PrettyFacility *)facility{
    [facility setSpriteWithFile:@"dv_09_lv1.png"];
}

- (void)startBlockerAction:(PrettyFacility *)facility{
    [facility setSpriteWithFile:@"dv_09_lv2.png"];
}

- (PrettyFacility*)addFacility:(int)pDNAID ID:(int)pID level:(int)pLevel position:(CGPoint)pPosition{
    PrettyFacility* facility = [super addFacility:pDNAID ID:pID level:pLevel position:pPosition];
    if([facility.specialFacilityType isEqualToString:@"Blocker"]){
        [DCGraphicEngine sharedManager];
        int nZPos = [DCGraphicEngine screenSize].height*10.0f - facility.sprite.position.y*10.0f;
        nZPos += 40;
        [facility.sprite.parent reorderChild:facility.sprite z:nZPos];
    }
    return facility;
}

- (void)loadStageFromProfile
{
    [super loadStageFromProfile];
    for (PrettyFacility *facility in _FacilityArray) {
        if([facility.specialFacilityType isEqualToString:@"MusicPlayer"]){
            if([facility.sprite getChildByTag:1] != nil){
                CCLabelTTF *MusicPlayerCount = (CCLabelTTF*)[facility.sprite getChildByTag:1];
                [MusicPlayerCount setString:[NSString stringWithFormat:@"%.2d",facility.maxUserCount]];
            }
        }
        if([facility.specialFacilityType isEqualToString:@"Blocker"]){
            
        }if([facility.sprite getChildByTag:1] != nil){
            CCLabelTTF *BlockerCount = (CCLabelTTF*)[facility.sprite getChildByTag:1];
            [BlockerCount setString:[NSString stringWithFormat:@"%.2d",facility.maxUserCount]];
        }
    }
    
    //restore door
    PrettyObstacle *obstacle;
    for (int i = 0; i < [_ObstacleArray count]; i++){
        obstacle = [_ObstacleArray objectAtIndex:i];
        if(obstacle.dnaID == 5){
            if([Utilities isiPad]){
                obstacle.sprite.position = [GameUnit pixelFromUnit:CGPointMake(31, 7.1)];
            }
            else{
                obstacle.sprite.position = [GameUnit pixelFromUnit:CGPointMake(31, 6)];
            }
        }
    }
}

- (void)stageStart
{
    NSSortDescriptor *Sorter = [[[NSSortDescriptor alloc] initWithKey:@"ID" ascending:YES] autorelease];
    NSArray *descriptorArray = [NSArray arrayWithObjects:Sorter, nil];
    [_FacilityArray sortUsingDescriptors:descriptorArray];
    [super stageStart];
    if(self.day == 1 && self.stageTime < 0.1){
        //[self setMoney:self.money + 400];
    }
    
    DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
    NSMutableDictionary *VIPProfile = [profile.dict getData:@"vipData"];
    if(VIPProfile == nil){
        VIPProfile = [NSMutableDictionary dictionary];
    }
    [VIPProfile setObject:[NSNumber numberWithInt:0] forKey:VIP_SHOWN_KEY];
    [profile.dict setObject:VIPProfile forKey:@"vipData"];
    [[DCProfileManager sharedManager] saveAllProfiles];
    [((FruitStageViewController*)self.stageViewController) setCatagoryButtonsEnable:NO];
	for(int i = 0 ; i < [_StaffArray count]; i++){
		PrettyStaff *staff = [_StaffArray objectAtIndex:i];
		[staff.sprite setUserInteractionEnabled:NO];
	}
}

- (void)stageEnd
{
    if(self.todayWastedFacilityUser == 0){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_perfectday percent:100];
    }
    
    [super stageEnd];
	[[OALSimpleAudio sharedInstance] setBgPaused:YES];
    
    [self setGameSpeed:1.0];
    
    DCProfile *curProfile = [[DCProfileManager sharedManager] getCurrentProfile];
    int purchasedEquipment = -1;
    if(curProfile != nil){
        purchasedEquipment = [[curProfile.dict getData:TotalPurchaseEquipmentKey]intValue];
    }
    
    int StaffCount = 0;
    PrettyStaff *tStaff;
    for (int i=0;i<[_StaffArray count];i++) {
		tStaff = [_StaffArray objectAtIndex:i];
        if (tStaff != nil){
            if(tStaff.level >= 1){
                StaffCount++;
            }
        }
    }
    
    if(self.todayMoney >= 5000){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_dayearn5000 percent:100];
    }
    if(self.todayMoney >= 10000){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_dayearn10000 percent:100];
    }
    if(self.todayMoney >=15000){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_dayearn15000 percent:100];
    }
    if(self.todayMoney == 0){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_dayearn0 percent:100];
    }
    if(self.todayScore >= 10.0f){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_dayhappiness10 percent:100];
    }
    if(self.todayScore >= 15.0f){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_dayhappiness15 percent:100];
    }
    if(self.todayScore <= -10.0f){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_daysad10 percent:100];
    }
    if(self.todayScore <= -15.0f){
        [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_daysad15 percent:100];
    }
    
    //pay per action : finish day
	UIViewController *viewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    if(self.day >= 1){
		[[MunerisAppEvents report:@"ppa_finish_day1" withViewController:viewController] execute];
    }
    if(self.day >= 2){
        [[MunerisAppEvents report:@"ppa_finish_day2" withViewController:viewController] execute];
    }
    if(self.day >= 3){
        [[MunerisAppEvents report:@"ppa_finish_day3" withViewController:viewController] execute];
    }
    if(self.day >= 4){
        [[MunerisAppEvents report:@"ppa_finish_day4" withViewController:viewController] execute];
    }
    if(self.day >= 5){
        [[MunerisAppEvents report:@"ppa_finish_day5" withViewController:viewController] execute];
    }
    
    
    
    if(curProfile != nil){
        int total_customer_count = 0;
        int total_money_earned = 0;
        if([curProfile.dict getData:TotalCustomersKey]!= nil){
            total_customer_count = [[curProfile.dict getData:TotalCustomersKey] intValue];
        }
        else{
            total_customer_count = 0;
        }
        
        if([curProfile.dict getData:TotalMoneyKey]!=nil){
            total_money_earned = [[curProfile.dict getData:TotalMoneyKey] intValue];
        }
        else{
            total_money_earned = 0;
        }
        
        total_money_earned += self.todayMoney;
        total_customer_count += self.todayNormalFacilityUser + self.todayPerfectFacilityUser;
        
        // pay per action : Finish the service for 5 customers
        if(total_customer_count >= 5){
			[[MunerisAppEvents report:@"ppa_customer_5" withViewController:viewController] execute];
        }
        
        [curProfile.dict setObject:[NSNumber numberWithInt:total_money_earned] forKey:TotalMoneyKey];
        [curProfile.dict setObject:[NSNumber numberWithInt:total_customer_count] forKey:TotalCustomersKey];
		[[DCProfileManager sharedManager] saveAllProfiles];
        
        NSLog(@"total customer: %d",total_customer_count);
        NSLog(@"total money: %d",total_money_earned);
        
        if(total_customer_count >= 2500){
            [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_customers2500 percent:100];
        }
        if(total_customer_count >= 5000){
            [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_customers5000 percent:100];
        }
        if(total_customer_count >= 10000){
            [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_customers10000 percent:100];
        }
        
    }
}

- (PrettyStaff*)staffUpgrade:(PrettyStaff*)staff
{
	PrettyStaff* newStaff = [super staffUpgrade:staff];
	
	float staffCount = 0;
	float staffLv5Count = 0;
	
	PrettyStaff *tStaff;
	for (int i=0;i<[_StaffArray count];i++) {
		tStaff = [_StaffArray objectAtIndex:i];
		if (tStaff != nil && tStaff.level >=1) {
			staffCount++;
		}
		if (tStaff != nil && tStaff.level >=5) {
			staffLv5Count++;
		}
	}
    
    //if(newStaff.dnaID == 7 && newStaff.level >= 1){
    //    [[AchievementSystem sharedManager] updateAchievementWithID:car_ach_mvp percent:100];
    //}
	
	DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
	if (sysProfile != nil) {
		NSMutableDictionary* statsDict = [sysProfile.dict getData:@"stats"];
		if (statsDict == nil) {
			statsDict = [[[NSMutableDictionary alloc] init] autorelease];
		}
		float ppt_ach_staff_count;
		if ([statsDict getData:car_ach_allstaff] != nil) {
			ppt_ach_staff_count = [[statsDict getData:car_ach_allstaff] floatValue];
		}
		else {
			ppt_ach_staff_count = 0;
		}
		if (staffCount/[[[[PrettyManager sharedManager] getDNADict:STAFF_DNA_FILE] getData:@"DNADict"] count]*100 > ppt_ach_staff_count) {
			ppt_ach_staff_count = staffCount/[[[[PrettyManager sharedManager] getDNADict:STAFF_DNA_FILE] getData:@"DNADict"] count]*100;
			[statsDict setObject:[NSNumber numberWithFloat:ppt_ach_staff_count] forKey:car_ach_allstaff];
			[[AchievementSystem sharedManager] updateAchievementWithID:car_ach_allstaff percent:ppt_ach_staff_count];
		}
		[sysProfile.dict setObject:statsDict forKey:@"stats"];
		[[DCProfileManager sharedManager] saveAllProfiles];
		
	}

	return newStaff;
}


- (void)reloadInterface
{
	[super reloadInterface];
	if (stageState == STAGE_PREPARE) {
		if (self.stageViewController != nil) {
			[(FruitStageViewController*)self.stageViewController setPrepareViewButton:STAGEVIEW_BTN_MAP target:self selector:@selector(quitGame)];
		}
	}
}

- (void)quitGame {
	[[OALSimpleAudio sharedInstance] playEffect:@"click.wav"];
    
    if(_isUsingConsumableItem){
        [(PrettyConsumableItemController*)[PrettyConsumableItemControllerClass sharedManager] stopAndReomveAllEvent];
        [PrettyConsumableItemController releaseManager];
    }
    if(mQuestController != nil){
        [mQuestController release];
        mQuestController = nil;
    }
	[self removeAllSignal];
	[self saveStageAllDataToProfile];
    
    
	if (self.stageViewController != nil) {
		[self.stageViewController dismissAndBackToProgramState:PROGRAM_MAINMENU_STATE];
	}
    
    DCSysProfile *sysProfile = [[DCProfileManager sharedManager] getSystemProfile];
    [sysProfile.dict setObject:[NSNumber numberWithBool:YES] forKey:SHOULD_SHOW_TAKEOVER_KEY];
    [[DCProfileManager sharedManager] saveAllProfiles];
}

- (void)updateStageProfile:(NSMutableDictionary*)pStageDict fromVersion:(NSString*)pOldVersion toVersion:(NSString*)pNewVersion
{
}

- (int)getIDFromPositionForVersion_1_1_0:(CGPoint)pPosition atStage:(int)pStage
{
		return 0;
}

#pragma mark -
#pragma mark Game Speed Up

- (float)getSpeedUpProfit:(float)originalProfit {
	return originalProfit * [self getSpeedUpBonusRatio];
}

- (float)getSpeedUpScore:(float)originalScore {
    return originalScore;
	//return originalScore * [self getSpeedUpBonusRatio];
}

#pragma mark -
#pragma mark Trial Staff
- (void)reloadTrialStaff:(PrettyStaff *)staff{
    [super reloadTrialStaff:staff];
    [staff.sprite setOpacity:125];
}

/** Jonathan: Never snap to current facility **/
- (PrettyFacility*)checkSnappingFacility:(PrettyFacilityUser*)facilityUser 
{
    PrettyFacility* closestFacility = nil;
	for (PrettyFacility *facility in _FacilityArray) {
		if (facility.sprite != nil && facility != facilityUser.facility) {
			if (CGPointDistance(CGPointAdd(facility.sprite.position, [GameUnit relativePixelFromUnit:facility.snapPosOffset]), facilityUser.sprite.position) <= facility.snapDistance) {
				if (facilityUser.facility == facility || ([facility canFacilityUserEnter:facilityUser] && [facilityUser canEnterFacility:facility])) {
                    // replace closest facility
					if (closestFacility == nil || CGPointDistance(CGPointAdd(facility.sprite.position, [GameUnit relativePixelFromUnit:facility.snapPosOffset]), facilityUser.sprite.position) < CGPointDistance(closestFacility.sprite.position, facilityUser.sprite.position) ) {
						closestFacility = facility;
					}
				}
			}
		}
	}
	return closestFacility;
}

@end
