//
//  FruitMenuViewController.h
//  DCGameTemplate
//
//  Created by Dream Cortex on 08/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PrettyMenuViewController.h"

#define SHOULD_SHOW_TAKEOVER_KEY @"SHOULD_SHOW_TAKEOVER_KEY"

@interface FruitMenuViewController : PrettyMenuViewController {
	//BOOL bBgmEnable, bSfxEnable;
    int curHelpPage;

    UILabel *iEnterNameTitleLabel;
    UIButton *iNewGameButton;
    UIView *iHelpView;
    UIButton *iHelpCloseButton;
    UIButton *iHelpLeftButton;
    UIButton *iHelpRightBUtton;
    UIButton *iHelpButton;
    UIImageView *iHelpPage;
    UILabel *iHelpDesc1;
    UILabel *iHelpDesc2;
    UILabel *iSettingTitle;
    UIView *iUpdatePopUpView;
    UIImageView *iUpdatePopUpImage;
    UILabel *iUpdatePopUpDesc;
    UILabel *iUpdatePopUpDesc2;
    UIButton *iUpdatePopUpCloseButton;
    NSMutableArray *iAnimationViewArray;
    UIImageView *iMenuHousePic;
    UIButton *iCardCollectionButton;
	UILabel *iDebugLabel;
    BOOL showingHelp;
    
    UIView *iMenuButtonView;
}
@property (nonatomic, retain) IBOutlet UILabel *iSettingTitle;
@property (nonatomic, retain) IBOutlet UIView *iAnimationContainer;
@property (nonatomic, retain) IBOutlet UILabel *iEnterNameTitleLabel;
@property (nonatomic, retain) IBOutlet UIView *iHelpView;
@property (nonatomic, retain) IBOutlet UIButton *iHelpCloseButton;
@property (nonatomic, retain) IBOutlet UIButton *iHelpLeftButton;
@property (nonatomic, retain) IBOutlet UIButton *iHelpRightButton;
@property (nonatomic, retain) IBOutlet UIImageView *iHelpPage;
@property (nonatomic, retain) IBOutlet UILabel *iHelpDesc1;
@property (nonatomic, retain) IBOutlet UILabel *iHelpDesc2;
@property (nonatomic, retain) IBOutlet UIView *iUpdatePopUpView;
@property (nonatomic, retain) IBOutlet UIImageView *iUpdatePopUpImage;
@property (nonatomic, retain) IBOutlet UILabel *iUpdatePopUpDesc;
@property (nonatomic, retain) IBOutlet UILabel *iUpdatePopUpDesc2;
@property (nonatomic, retain) IBOutlet UIButton *iUpdatePopUpCloseButton;
@property (nonatomic, retain) IBOutlet UIButton *iNewGameButton;
@property (nonatomic, retain) IBOutlet UIImageView *iMenuHousePic;
@property (nonatomic, retain) IBOutlet UIView *iMenuButtonView;
@property (nonatomic, retain) IBOutlet UIButton *iCardCollectionButton;
@property (nonatomic, retain) IBOutlet UILabel *iDebugLabel;

- (IBAction)HelpCloseOnClick:(id)sender;
- (IBAction)HelpLeftOnClick:(id)sender;
- (IBAction)HelpRightOnClikc:(id)sender;
- (IBAction)HelpButtonOnClick:(id)sender;
- (void)updateHelpPage;
- (void)showHelpView;
- (void)hideHelpView;
- (void)releaseHelpView;

-(void)showUpdatePopUpView;
-(void)hideUpdatePopUpView;
-(IBAction)UpdatePopUpCloseButtonOnClick:(id)sender;
-(void)addMenuAnimationViewWithType:(int)type;
-(void)setAnimationPosition:(UIImageView*)sender;
-(void)updateRewardsIcon;

-(UIImage*)getImageFromSpriteSheet:(NSString*)spriteSheetFile spriteFile:(NSString*)spriteFileName frameName:(NSString*)frameName flipX:(BOOL)flipX;

@end
