//
//  FruitDailyRewardsViewController.h
//  DCGameTemplate
//
//  Created by royl on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DailyRewardsViewController.h"

@interface FruitDailyRewardsViewController : DailyRewardsViewController {
    
}
@property (nonatomic, retain) IBOutlet UIButton *iCollectButton;
@property (nonatomic, retain) IBOutlet UIButton *iCollectButton2;

@property (nonatomic, retain) IBOutlet UIImageView *day1RewardBG;
@property (nonatomic, retain) IBOutlet UIImageView *day1RewardIcon;
@property (nonatomic, retain) IBOutlet UILabel *day1RewardLabel;
@property (nonatomic, retain) IBOutlet UIImageView *day2RewardBG;
@property (nonatomic, retain) IBOutlet UIImageView *day2RewardIcon;
@property (nonatomic, retain) IBOutlet UILabel *day2RewardLabel;
@property (nonatomic, retain) IBOutlet UIImageView *day3RewardBG;
@property (nonatomic, retain) IBOutlet UIImageView *day3RewardIcon;
@property (nonatomic, retain) IBOutlet UILabel *day3RewardLabel;
@property (nonatomic, retain) IBOutlet UIImageView *day4RewardBG;
@property (nonatomic, retain) IBOutlet UIImageView *day4RewardIcon;
@property (nonatomic, retain) IBOutlet UILabel *day4RewardLabel;
@property (nonatomic, retain) IBOutlet UIImageView *day5RewardBG;
@property (nonatomic, retain) IBOutlet UIImageView *day5RewardIcon;
@property (nonatomic, retain) IBOutlet UILabel *day5RewardLabel;


-(void)updateRewardViewWithDay:(int)day icon:(UIImageView*)rewardIcon bg:(UIImageView*)bg label:(UILabel*)label rewardType:(DAILY_REWARDS)reward today:(int)today;
@end
