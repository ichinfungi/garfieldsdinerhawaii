//
//  FruitDailyRewardsManager.m
//  DCGameTemplate
//
//  Created by royl on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitDailyRewardsManager.h"

@implementation FruitDailyRewardsManager

- (NSTimeInterval)getDailyRewardsInterval {
	// In second, 86400 = 1 day
    // In second, 82800 = 23 hours
    if(DEBUG_SKIP){
        return 2.0f;
    }
    else{
        return 82800.0f;
    }
}

- (DAILY_REWARDS)dailyRewardsForDay:(int)day {
	DAILY_REWARDS reward;
	reward.type = DAILY_REWARDS_MONEY;
	reward.value = 0;
	
	switch (day%[self getDailyRewardsCycleDays]) {
		case 0:
			reward.type = DAILY_REWARDS_POINT;
			// From 10pp to 20pp, increase 2pp per cycle 
			reward.value = MIN(10, 5 + ((day-1)/[self getDailyRewardsCycleDays])*2);
			break;
			
		case 1:
			reward.value = 1000;
			break;
			
		case 2:
			reward.type = DAILY_REWARDS_POINT;
			// From 10pp to 20pp, increase 2pp per cycle 
			reward.value = 5;
			break;
			
		case 3:
			reward.value = 2000;
			break;
			
		case 4:
			reward.value = 3000;
			break;
	}
	
	return reward;
}

@end
