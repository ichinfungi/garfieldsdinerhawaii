//
//  FruitDailyRewardsViewController.m
//  DCGameTemplate
//
//  Created by royl on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitDailyRewardsViewController.h"
#import "FruitDailyRewardsManager.h"
#import "DailyRewardsManager.h"
#import "DCProfileManager.h"
#import "Localization.h"
#import "Utilities.h"
#import "GameSetting.h"

@implementation FruitDailyRewardsViewController
@synthesize iCollectButton;
@synthesize iCollectButton2;
@synthesize day1RewardBG, day1RewardIcon, day1RewardLabel;
@synthesize day2RewardBG, day2RewardIcon, day2RewardLabel;
@synthesize day3RewardBG, day3RewardIcon, day3RewardLabel;
@synthesize day4RewardBG, day4RewardIcon, day4RewardLabel;
@synthesize day5RewardBG, day5RewardIcon, day5RewardLabel;

/* REMOVE INGAME BANNER */
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if (self.stage!=nil) {
		[self hideBanner];
	}
}

-(void)handleRewardReady{
    [super handleRewardReady];
    [iCollectButton setEnabled:YES];
    [iCollectButton2 setEnabled:YES];
}

-(void)handleRewardNotReady:(NSTimeInterval)nextRewardTime{
    [super handleRewardNotReady:nextRewardTime];
    [iCollectButton setEnabled:NO];
    [iCollectButton2 setEnabled:NO];
    
    [self.iNextRewardTimeLabel setText:TIME_TO_STRING([[DailyRewardsManager sharedManager] getDailyRewardsInterval] - nextRewardTime)];
}

-(void)updateRewardInterface {
    [super updateRewardInterface];
    int today = [[DailyRewardsManager sharedManager] getLastRewardDay] + 1;
    int cycle = (today - 1) / [[DailyRewardsManager sharedManager] getDailyRewardsCycleDays];
    int dayPerCycle = [[DailyRewardsManager sharedManager] getDailyRewardsCycleDays];
    
    [self updateRewardViewWithDay:1 icon:day1RewardIcon bg:day1RewardBG label:day1RewardLabel rewardType:[[DailyRewardsManager sharedManager] dailyRewardsForDay:cycle * dayPerCycle + 1] today:today];
    [self updateRewardViewWithDay:2 icon:day2RewardIcon bg:day2RewardBG label:day2RewardLabel rewardType:[[DailyRewardsManager sharedManager] dailyRewardsForDay:cycle * dayPerCycle + 2] today:today];
    [self updateRewardViewWithDay:3 icon:day3RewardIcon bg:day3RewardBG label:day3RewardLabel rewardType:[[DailyRewardsManager sharedManager] dailyRewardsForDay:cycle * dayPerCycle + 3] today:today];
    [self updateRewardViewWithDay:4 icon:day4RewardIcon bg:day4RewardBG label:day4RewardLabel rewardType:[[DailyRewardsManager sharedManager] dailyRewardsForDay:cycle * dayPerCycle + 4] today:today];
    [self updateRewardViewWithDay:5 icon:day5RewardIcon bg:day5RewardBG label:day5RewardLabel rewardType:[[DailyRewardsManager sharedManager] dailyRewardsForDay:cycle * dayPerCycle + 5] today:today];
}

-(void)updateRewardViewWithDay:(int)day icon:(UIImageView*)rewardIcon bg:(UIImageView*)bg label:(UILabel*)label rewardType:(DAILY_REWARDS)reward today:(int)today{
    
    int dayPerCycle = [[DailyRewardsManager sharedManager] getDailyRewardsCycleDays];
    int cycle = (today - 1) / [[DailyRewardsManager sharedManager] getDailyRewardsCycleDays];
    int currentCycleDay = today - (cycle * dayPerCycle);
    NSString* bgName = @"empty.png";
    
    if(reward.type == DAILY_REWARDS_MONEY){
        [rewardIcon setImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(( day <= currentCycleDay)?@"gd_bonus1.png":@"gd_bonus1_off.png")]];
        if( day < currentCycleDay){
            bgName = @"gd_bonus1_base3.png";
        }
        else if(day == currentCycleDay){
            bgName = @"gd_bonus1_base1.png";
        }
        else {
            bgName = @"gd_bonus1_base2.png";
        }
    }
    else{
        [rewardIcon setImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName((day <= currentCycleDay)?@"gd_bonus3.png":@"gd_bonus3_off.png")]];
        if(day < currentCycleDay){
            bgName = @"gd_bonus3_base3.png";
        }
        else if(day == currentCycleDay){
            bgName = @"gd_bonus3_base1.png";
        }
        else {
            bgName = @"gd_bonus3_base2.png";
        }
    }
    
    [bg setImage:[UIImage imageNamed:appendUniversalDeviceSuffixToFileName(bgName)]];
    [label setText:[NSString stringWithFormat:@"x%d",reward.value]];
}

-(IBAction)collectOnClick:(id)sender{
    DCProfile *profile = [[DCProfileManager sharedManager] getCurrentProfile];
    if(profile == nil){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:LOCALIZATION_TEXT(@"DAILY_REWARD_CREATE_PROFILE_WARNING") delegate:nil cancelButtonTitle:LOCALIZATION_TEXT(@"OK") otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    }
    else{
        [super collectOnClick:sender];
    }
}


@end
