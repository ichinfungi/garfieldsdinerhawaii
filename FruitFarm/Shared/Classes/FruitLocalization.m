//
//  FruitLocalization.m
//  DCGameTemplate
//
//  Created by royl on 4/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FruitLocalization.h"
#import "JRSwizzle.h"

@implementation Localization (Fruit)

+ (void)load
{
	// method swizzling
	[self jr_swizzleMethod:@selector(changeToCustomizeFont:) withMethod:@selector(changeToCustomizeFont_f:) error:nil];
}

- (void)changeToCustomizeFont_f:(UILabel *)label{
    NSString* userLangPref = LOCALIZATION_TEXT(@"currentLocalization");
    NSString *reqSysVer = @"3.2";
	NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
	BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
    if([userLangPref isEqualToString:@"en"] && osVersionSupported){
        [label setFont:[UIFont fontWithName:@"Garfield" size:label.font.pointSize]];
    }
}

@end
