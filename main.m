//
//  main.m
//  SpriteSheetTest
//
//  Created by Dream Cortex on 16/02/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    NSString* userLangPref = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserLangPreference"];
	if (userLangPref != nil) {
		if (![userLangPref isEqualToString:@"sys_default"]) {
			NSLog(@"User Language Preference:%@", userLangPref);
			NSMutableArray *lang_edit = [NSMutableArray arrayWithCapacity:2];
			[lang_edit addObject:userLangPref];
			[[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithArray:lang_edit] forKey:@"AppleLanguages"];
		} else {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
			//[[NSUserDefaults standardUserDefaults] setObject:[NSLocale preferredLanguages] forKey:@"AppleLanguages"];
            
            NSLog(@"Restore languages to:%@", [NSLocale preferredLanguages]);
		}
	}
    
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
