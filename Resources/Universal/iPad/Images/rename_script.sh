#!/bin/bash

for FILE in ./$1/*.jpg ;do NEWFILE=`echo $FILE | sed 's/.jpg/_iPad.jpg/g'` ; mv "$FILE" $NEWFILE ; done
for FILE in ./$1/*.png ;do NEWFILE=`echo $FILE | sed 's/.png/_iPad.png/g'` ; mv "$FILE" $NEWFILE ; done
for FILE in ./$1/*.plist ;do NEWFILE=`echo $FILE | sed 's/.plist/_iPad.plist/g'` ; mv "$FILE" $NEWFILE ; done
